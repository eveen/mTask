#!/bin/sh
BASE=ftp://ftp.cs.ru.nl/pub/Clean/builds
wget $BASE/windows-x64/clean-bundle-complete-windows-x64-latest.zip
unzip *.zip
patch clean-bundle-complete/Libraries/StdEnv/StdGeneric.icl < patches/StdGeneric.icl.patch
patch clean-bundle-complete/Libraries/StdEnv/StdGeneric.dcl < patches/StdGeneric.dcl.patch
sed -i 's/CleanCompiler64.exe : -h [0-9]\+M/CleanCompiler64.exe : -h 500M/' \
	clean-bundle-complete/Config/IDEEnvs
rm *.zip
