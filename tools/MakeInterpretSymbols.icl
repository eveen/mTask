module MakeInterpretSymbols

import StdString, StdEnum, StdDebug

import Text
import Data.List
import Data.Func
import Data.GenCons
import Control.GenBimap

import mTask.Interpret
import mTask.Interpret.DSL
import mTask.Interpret.Message

derive class gCons MTMessageFro, MTMessageTo, Maybe, BCPeripheral, Pin, MTTTaskData, MTException

(<+) infixr 5 :: a b -> String | toString a & toString b
(<+) a b = toString a +++ toString b

toDefine :: Int a -> String | consName {|*|} a
toDefine i b =
	"#define " <+ toUpperCase (consName{|*|} b) <+ " " <+ i <+ "\n"

toDispatch :: a -> String | consName {|*|} a
toDispatch a = "&&" +++ toLowerCase (consName{|*|} a)

toDispatchTable :: String [a] -> String | consName{|*|} a
toDispatchTable name items = concat
	[ "#define ", name, "_DISPATCH_TABLE\\\n\t{ ",
		join "\\\n\t, " (map toDispatch items), "\\\n\t}\n"
	, concat (zipWith toDefine [0..] items)
	]

Start = trace_n (toString (255-(length instructions)) +++ " instructions left") $
	trace_n (toString (255-(length tasktypes)) +++ " tasktypes left") $
	[ "#ifndef SYMBOLS_H\n"
	, "#define SYMBOLS_H\n"
	, "\n"
	, "#define MT_NULL ", toString MT_NULL, "\n"
	, "#define MT_REMOVE ", toString MT_REMOVE, "\n"
	, "#define MT_STABLE ", toString MT_STABLE, "\n"
	, "#define MT_UNSTABLE ", toString MT_UNSTABLE, "\n"
	, "#define MT_NOVALUE ", toString MT_NOVALUE, "\n"
	, "\n//Instructions\n"
	, toDispatchTable "I" instructions
	, toDispatchTable "T" tasktypes
	, toDispatchTable "MT" messageTos
	, toDispatchTable "MF" messageFros
	, toDispatchTable "PH" peripherals
	, toDispatchTable "DHTTYPE" dhttypes
	, toDispatchTable "BUTTONSTATUS" buttonstates
	, toDispatchTable "EXCEPTIONS" exceptions
	, "#endif"
	]

instructions :: [BCInstr]
instructions = conses{|*|}

tasktypes :: [BCTaskType]
tasktypes = conses{|*|}

messageTos :: [MTMessageTo]
messageTos = conses{|*|}

messageFros :: [MTMessageFro]
messageFros = conses{|*|}

peripherals :: [BCPeripheral]
peripherals = conses{|*|}

dhttypes :: [DHTtype]
dhttypes = conses{|*|}

buttonstates :: [ButtonStatus]
buttonstates = conses{|*|}

exceptions :: [MTException]
exceptions = conses{|*|}
