module blink

import StdEnv, iTasks

import mTask.Interpret
import mTask.Interpret.Device.TCP

Start w = doTasks main w

main :: Task Bool
main = enterDevice
	>>= \spec->withDevice spec
		\dev->liftmTask blink dev -|| viewDevice dev

blink :: Main (MTask v Bool) | mtask v
blink
	= fun \blink=(\x->
		     delay (lit 500)
		>>|. writeD d3 x
		>>=. blink o Not)
	In {main=blink (lit True)}
