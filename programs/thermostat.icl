module thermostat

import StdEnv

import iTasks

import mTask.Interpret
import mTask.Show

DHTPIN  :== D2
FANPIN  :== lit D3
HEATPIN :== lit D4

Start w = doTasks main w

main = enterDevice
	>>= \spec->withDevice spec \dev->
	   withShared 99.9 \targets->
	   withShared 42.0 \temp->
			    (updateSharedInformation [] targets <<@ Title "Goal")
			||- (viewSharedInformation [] temp <<@ Title "Current")
			||- liftmTask (mTask targets temp) dev

mTask :: (Shared sds1 Real) (Shared sds2 Real) -> Main (MTask v Real) | mtask, liftsds, dht v & RWShared sds1 & RWShared sds2
mTask goalShare tempShare =
	DHT DHTPIN DHT22 \dht->
	liftsds \goal = goalShare In
	liftsds \temp = tempShare In
	fun \tread=(
		\ot->    temperature dht
			>>*. [IfValue ((!=.)ot) (setSds temp)]
			>>=. tread
	) In
	fun \heater=(
		\on->getSds temp .&&. getSds goal
			>>*. [ IfValue (tupopen \(temp, goal)->temp >. goal &. on)
						\_->writeD HEATPIN false
			     , IfValue (tupopen \(temp, goal)->temp <. goal &. Not on)
						\_->writeD HEATPIN true]
			>>=. heater
	) In
	{main
		=    tread (lit 0.0)
		.||. heater (lit False)
//	DHT DHTPIN DHT22 \dht->
//	liftsds \goal = goalShare In
//	liftsds \temp = tempShare In
//	fun \tread=(
//		\ot->    temperature dht
//			>>*. [IfValue ((!=.)ot) (setSds temp)]
//			>>=. tread
//	) In
//	fun \heater=(
//		\on->getSds temp .&&. getSds goal
//			>>*. [IfValue (tupopen \(temp, goal)->temp >. goal &. on)
//						\_->writeD FANPIN false
//			    ,IfValue (tupopen \(temp, goal)->temp <. goal &. Not on)
//						\_->writeD FANPIN true]
//			>>=. heater
//	) In
//	{main= writeD FANPIN false >>|. rtrn (lit 42)
//		=    tread (lit 0)
//		.||. heater (lit False)
	}
