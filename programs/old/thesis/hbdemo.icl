module hbdemo

import iTasks
import mTask
import Devices.mTaskDevice

from Data.Func import $

Start world = startEngine blink world

blink :: Task ()
blink =       addDevice
	>>-       connectDevice
	>>- \stm->sendTaskToDevice "hb" hbt (stm, OnInterval 1000)
	>>- \(st, [t])->forever (
		viewSharedInformation "Heartbeat" [] (shareShare stm t)
	) >>* [OnAction (Action "Shutdown") $ always
		$ deleteDevice stm >>| shutDown 0
	]
where
	hbt :: Main (ByteCode () Stmt)
	hbt = sds \h=(HB 0 True) In {main = h =. getHb :. pub h :. noOp}
