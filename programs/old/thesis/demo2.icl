module demo2

import iTasks
import Interpret

from Data.Func import $

//Start world = startEngine demo world
Start world = startEngine demo world


lcdprint :: Task ()
lcdprint =           (addDevice <<@ Title "Please enter the arduino settings")
	>>-              connectDevice
	>>- \ard->       sendTaskToDevice "lcdprint" count (ard, OnInterval 500)
	>>- \(_, [p])->  chooseAction [(Action "Continue", ())]
	>>- \_->        (viewSharedInformation "" [] (shareShare ard p)
			-&&- updateSharedInformation "" [] (shareShare ard p))
	>>* [OnAction (Action "Shutdown") $ always
		$   deleteDevice ard
		>>| shutDown 0
		]

lcdp :: Main (ByteCode () Stmt)
lcdp = LCD 16 2 [] \lcd->sds \x=0 In {main =
	setCursor lcd (lit 0) (lit 0) :. print lcd x :. noOp}

demo :: Task ()
demo =               (addDevice <<@ Title "Please enter the arduino settings")
	>>-              connectDevice
	>>- \ard->       (addDevice <<@ Title "Please enter the stm settings")
	>>-              connectDevice
	>>- \stm->       chooseAction [(Action "Continue", ())]
	>>= \_->         sendTaskToDevice "hbmitor" hbtask (stm, OnInterval 5000)
	>>- \(_, [hbv])->sendTaskToDevice "hblcd" count (ard, OnInterval 5000)
	>>- \(_, [hbm])->chooseAction [(Action "Continue", ())]
	>>- \_->syncShares (shareShare stm hbv) (shareShare ard hbm)
	>>* [OnAction (Action "Shutdown") $ always
		$   deleteDevice ard
		>>| deleteDevice stm
		>>| shutDown 0
		]

syncShares :: (Shared BCValue) (Shared BCValue) -> Task ()
syncShares src tgt = forever $ get src
	>>- \b->wait "Wait for change" ((<>)b) src
	>>= \nb->set (BCValue $ hbInt $ castHB nb) tgt
	@! ()


hbInt :: Heartbeat -> Int
hbInt (HB i _) = i

castHB :: BCValue -> Heartbeat
castHB (BCValue b) = fromByteCode $ toByteCode b

count :: Main (ByteCode () Stmt)
count = sds \x=1 In {main = x =. x +. lit 1 :. pub x :. noOp }

hbtask :: Main (ByteCode () Stmt)
hbtask = sds \x=(HB 0 False) In {main = x =. getHb :. pub x :. noOp }

countlcd :: (Main (ByteCode () Stmt))
countlcd = LCD 16 2 []
	\lcd->sds \x=1 In {main =
		x =. x +. lit 1 :.
		setCursor lcd (lit 0) (lit 0) :.
		print lcd x :.
		noOp
	}
