module blinkdemo

import iTasks
import Interpret

from Data.Func import $

Start world = startEngine blink world

blink = withShared LED1 \ledShare->
	withDevice {host="192.168.0.1", port=8123} \dev->
	liftmTask dev (OnInterval 1000) (blinkTask ledShare)
	-|| updateSharedInformation "Which led to blink" [] ledShare
where
	blinkTask ls = lowerSds \led=ls In sds \x=True In {main =
		ledOff led1 :. ledOff led2 :. ledOff led3 :.
		IF x (ledOff led) (ledOn led) :.
		x =. Not x
		}
