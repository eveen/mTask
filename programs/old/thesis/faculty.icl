module faculty

from Data.Func import $

import iTasks

import TTY
import Interpret

faculty :: (Shared Int) Int -> Main (ByteCode () Stmt)
faculty si i = sds \y=i In lowerSds \x=si In {main =
	IF (y <=. lit 1) (
		pub x :. retrn
	) (
		x =. x *. y :.
		y =. y -. lit 1
	)}

Start w = startEngine [
	publish "/" $ const $ facTask,
	publish "/simulators" $ const viewSims] w

facTask :: Task ()
facTask = withShared 1 \resultShare->
	withDevice dev \device->
			viewDevice device ||-
	        (enterInformation "Factorial of what?" []
	>>= \i->liftmTask device (OnInterval 1000) (faculty resultShare i)
	||- viewSharedInformation "Result" [] resultShare)
	>>* [OnAction (Action "Stop") $ always $ return ()]
	where
//		dev = {host="localhost",port=8123}
		dev = SimSettings
			{defaultValue & stackSize = 1024, bytesMemory = 2048, resolveLabels = False}
			Manual
			1000.0
