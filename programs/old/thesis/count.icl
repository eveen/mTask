module count

from Data.Func import $

import iTasks
import Interpret

import TTY
import iTasksTTY

count :: (Shared Int) -> Main (ByteCode Int Stmt)
count i = lowerSds \counter=i In {main =
	IF (counter >. lit 20) retrn noOp :.
	counter =. counter +. lit 1 :.
	pub counter
	}

Start w =  startEngine counter w

counter = withShared 20 \counterShare->
	withDevice {zero & devicePath="/dev/ttyACM0"} \device->
//	withDeviceTask {host="localhost",port=8123} \device->
	    liftmTask device (OnInterval 1000) (count counterShare)
	-|| viewSharedInformation "Count" [] counterShare
	>^* [OnAction (Action "Reset") $ always $ set 0 counterShare]
