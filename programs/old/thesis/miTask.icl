module miTask

import Data.Maybe, Data.Func
import StdFunc => qualified return
import qualified Data.Map as DM
import iTasks
import iTasksTTY
import Interpret

Start world = startEngine [
		publish "/manage" $ const $ mTaskManager
			>>* [OnAction (Action "Shutdown") (always $ shutDown 0)],
		publish "/" $ const demo,
//		publish "/factorial" $ const factorial
		publish ("/simulators") $ const viewSims
	] world

mTaskManager :: Task ()
mTaskManager = forever $ viewmTasks ||-
		((manageShares ||- forever manageDevices) <<@ ArrangeSplit Vertical True)
		<<@ ArrangeWithSideBar 0 LeftSide 260 True
	where
		viewmTasks :: Task [MTaskDevice]
		viewmTasks = enterChoiceWithShared "Available mTasks" [ChooseFromList id] mTaskTaskStore
			>>= \task->get deviceStoreNP
			>>* [OnValue $ (ifValue isEmpty)         $ \_->
					viewInformation "No devices yet" [] [] >>= treturn
				,OnValue $ (ifValue $ not o isEmpty) $ \d->
					fromJust ('DM'.get task allmTasks)
					>>= \bc->(enterChoice "Choose Device" [ChooseFromDropdown \t->t.deviceName] d
						-&&- enterInformation "Timeout" []
					) >>* [OnAction (Action "Send") (withValue $ Just o sendTaskToDevice bc)]
					>>| treturn []
				]
