module countsteplcd

import iTasks
import Interpret
import Data.Maybe
import Data.List

from Data.Func import $

Start world = startEngine cstep world

cshare :: Shared Int
cshare = sharedStore "cshare" 0

cstep = withDevice {host="192.168.0.1",port=8123} \dev->
	    liftmTask dev (OnInterval 1000) lcdp
	-|| updateSharedInformation "Increment" [] cshare
where
	lcdp :: Main (ByteCode () Stmt)
	lcdp = LCD 16 2 [] \lcd->lowerSds \x=cshare In namedsds \y=(1 Named "incr") In
		{main = x =. x +. y :.
			setCursor lcd (lit 0) (lit 0) :. print lcd x :. noOp}
