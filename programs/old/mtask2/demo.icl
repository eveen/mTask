module demo

import StdEnv

import iTasks

import Interpret
import Show

DHTPIN  :== D2
FANPIN  :== lit D3
HEATPIN :== lit D4

Start w = doTasks main w

main =
	enterDevice >>= withDevice \dev->
	withShared 160 \low->
	withShared 220 \high->
	withShared 420 \temp->
	allTasks 
		[ updateSharedInformation "Targets" [targetUpdater] (low >*< high) @ fst
		, viewSharedInformation "Current temperature" [ViewAs \v->toReal v / 10.0] temp
		, liftmTask dev (mTask low high temp) @! 0
		]
where
	targetUpdater = UpdateUsing
		(\(l,h)->(toReal l / 10.0, toReal h / 10.0))
		(\_ (l,h)->(toInt (l * 10.0), toInt (h * 10.0)))
		(panel2
			(gEditor{|*|} <<@ labelAttr "Low")
			(gEditor{|*|} <<@ labelAttr "High")
		)

mTask :: (Shared sds Int) (Shared sds` Int) (Shared sds`` Int) -> Main (MTask v ()) | mtask, liftsds, dht v & RWShared sds & RWShared sds` & RWShared sds``
mTask lowShare highShare tempShare =
	DHT DHTPIN DHT22 \dht->
	liftsds \high = highShare In
	liftsds \temp = tempShare In
	fun \syncsds=(\oldtemp->
		temperature dht
		>>*. [IfValue ((!=.)oldtemp) (setSds temp)]
		>>=. syncsds) In
	fun \heat=(\on->getSds temp .&&. getSds high
		>>*. [IfValue (tupopen \(tem, tar)->tem <. tar &. Not on)
		         (\_->writeD HEATPIN (lit True))
		     ,IfValue (tupopen \(tem, tar)->tem >. tar &. on)
		         (\_->writeD HEATPIN (lit False))]
		>>=. \_->heat (Not on)) In
	{main
		=    syncsds (lit 0) .||. heat (lit False)
//		.||. rpeat (getSds temp .&&. getSds target
//			>>~. tupopen \(tem, tar)->
//			     writeD FANPIN  (tem <. second tar)
//		    .&&. writeD HEATPIN (tem >. first  tar)
//		)
	}
