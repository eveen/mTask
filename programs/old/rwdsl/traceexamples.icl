module traceexamples

/*
	Pieter Koopman
	Radboud University NIjmegen, The Netherlands
	pieter@cs.ru.nl
*/
import iTasks
import Simulate
import AST

Start = (mainAST mTask2).defs

// blink
mTask1 :: Main (v (TaskValue ())) | delay, ever, arith, seq v & dio DPin v
mTask1 =
	{main =
		ever (
			writeD d13 true >>|.
			delay  ms500 >>|.
			writeD d13 false >>|.
			delay ms500
		)
	}

// blink with sds
mTask2 :: Main (v (TaskValue ())) | mtask v
mTask2 =
	sds \led = True In
	{main =
		ever (
			getSds led >>=. \s.
			writeD d13 s >>|.
			setSds led (Not s) >>|.
			delay ms500
		)
	}

// blink with function
mTask3 :: Main (v (TaskValue ())) | mtask v
mTask3 =
	fun \led = (\s.
		writeD d13 s >>|.
		delay ms500 >>|.
		led (Not s)
	) In
	{main = led true
	}

mTask4 :: Main (TT (TaskValue ()))
mTask4 =
	fun \led = (\(d, s).
		writeD d13  s >>|.
		delay d >>|.
		led (d, Not s)
	) In
	{main = led (ms500, true)
	}

facDef :: Int -> Main (MTask v Int) | mtask v
facDef x =
	fun \fac = (\n. If (n <. lit 2) (lit 1) (n *. fac (n -. lit 1)))
	In {main = rtrn (fac (lit x))}

facTask :: Int -> Main (MTask v Int) | mtask v
facTask x =
  fun \fac = (\n. If (n <. lit 2) (lit 1) (n *. fac (n -. lit 1))) In
  fun \tsk = (\n. rtrn (fac n)) In
  {main = tsk (lit x)}

ms500 = lit 500
instance one  (v Int) | arith v where one  = lit 1
instance zero (v Int) | arith v where zero = lit 0

initKey = True
switchedBlink =
  sds \key = initKey In
  fun \switch = (\k. mTask () (
    readA a0 >>=. \a.
    rtrn (If (a <. lit 1000) (Not k) k) >>=. \k2.
    setSds key k2 >>|.
    delay (lit 25) >>|. 
    switch k2)) In
  fun \led = (\state. mTask () (
    getSds key >>=. \k.
    rtrn (k &. Not state) >>=. \state2.
    writeD d13 state2 >>|.
    delay (lit 1000) >>|. 
    led state2)) In
  {main = switch (lit initKey) .&&. led false } 

switchedBlink0 =
  sds \key = True In
  fun \switch = (\(). mTask () (
    readA a0 >>=. \a.
    getSds key >>=. \k.
    a <. lit 1000 ?
      setSds key (Not k) >>|.
    delay (lit 25) >>|. 
    switch ())) In
  fun \led = (\state. mTask () (
    getSds key >>=. \k.
    rtrn (k &. Not state) >>=. \state2.
    writeD d13 state2 >>|.
    delay (lit 1000) >>|. 
    led state2)) In
  {main = switch () .&&. led false } 

t8 = 
  sds \temp = 18.0 In
  sds \goal = 21.0 In
  DHT D0 DHT22 \dht.
  LCD 16 2 [] \lcd =
  fun \f = (\(). mTask Int (
  	getSds temp >>=. \t.
  	setSds temp (t +. step) >>|.
  	delay (lit 10))) In
//  fun \incr = (\x. x +. lit 1) In
  fun \newGoal = (\g.
    setSds goal g // >>|.
//    printAt lcd Zero One g
    ) In
  fun \g = (\(). mTask Int (
  	getSds goal >>=. \g.
  	setSds goal (g -. step) >>|.
  	delay (lit 15))) In
  {main = ever (f ()) .&&. ever (g ())}

t7 :: Main (v (TaskValue ((),((),())))) | mtask v & buttonPressed v & fun (v ()) v & fun (v Real) v & fun (v Int,v Bool) v
t7 = 
  sds \temp = 18.0 In
  sds \goal = 21.0 In
  DHT D0 DHT22 \dht .
  LCD 16 2 [] \lcd .
  fun \measure = (\(). mTask Int (
    temperature dht >>~. \act.
    setSds temp act >>|.
    printAt lcd Zero Zero act)) In
  fun \newGoal = (\g.
    setSds goal g >>|.
    printAt lcd Zero One g) In
  fun \switch = (\state. mTask Int (
      writeD heating state >>|.
      printAt lcd (lit 5) Zero (If state (lit "On ") (lit "Off"))
    )) In
  fun \keys = 
    (\().mTask Int (
    getSds goal >>=. \g.
    buttonPressed >>*.
	  [ IfValue ((==.) upButton)   (\_.newGoal (g +. step))
	  , IfValue ((==.) downButton) (\_.newGoal (g -. step))
	  ] )) In
  fun \control = (\(waitTime, running).mTask () (
    delay waitTime >>|.
    getSds goal >>=. \g.
    getSds temp >>*.
      [IfValue (\t.g >. t &. Not running) (\t.
      	switch true >>|.
      	control (minOnTime, true))
      ,IfValue (\t.g <. t &. running) (\t.
        switch false >>|.
        control (minOffTime, false))
      ,Always (control (lit 250, running))
      ])) In
  {main = 
  	control (lit 0, false) .&&. 
  	ever (measure () >>|. delay (lit 500)) .&&.
  	ever (keys () >>|. delay (lit 150))}

t7l :: Main (v (TaskValue ((),((),())))) | mtask v & buttonPressed v & fun (v ()) v & fun (v Real) v & fun (v Int,v Bool) v
t7l = 
  sds \temp = 18.0 In
  sds \goal = 21.0 In
  DHT D0 DHT22 \dht.
  LCD 16 2 [] \lcd.
  fun \measure = measureFun temp dht lcd  In
  fun \newGoal = goalFun goal lcd In
  fun \switch  = switchFun lcd In
  fun \keys    = keysFun goal newGoal In
  fun \control = controlFun goal temp switch control In
  {main = 
  	control (lit 0, false) .&&. 
  	everD (measure ()) (lit 500) .&&.
  	everD (keys ())    (lit 150)
  }

measureFun temp dht lcd () =
    temperature dht >>~. \act.
    setSds temp act >>|.
    printAt lcd Zero Zero act

goalFun goal lcd g = 
    setSds goal g >>|.
    printAt lcd Zero One g

switchFun lcd state = 
    writeD heating state >>|.
    printAt lcd (lit 5) Zero (If state (lit "On ") (lit "Off"))

keysFun goal newGoal () =
    getSds goal >>=. \g.
    buttonPressed >>*.
	  [ IfValue ((==.) upButton)   (\_.newGoal (g +. step))
	  , IfValue ((==.) downButton) (\_.newGoal (g -. step))
	  ]

controlFun goal temp switch control (waitTime, running) =
  mTask () (
    delay waitTime >>|.
    getSds goal >>=. \g.
    getSds temp >>*.
      [IfValue (\t.g >. t &. Not running) (\t.
      	switch true >>|.
      	control (minOnTime, true))
      ,IfValue (\t.g <. t &. running) (\t.
        switch false >>|.
        control (minOffTime, false))
      ,Always (control (lit 250, running))
      ])

step = lit 0.5
minOnTime = ms 6000 //0
minOffTime = ms 9000 //0
ms x = lit x
heating = d13
Zero = lit zero
One = lit one

/*
import StdList
from StdFunc import o


e0 = lit 42
e1 = lit 6 *. lit 7
e2 = readA a1
e3 = writeD a2 (lit True)
e4 = writeD d2 (e0 ==. e1)
e5 = e2 >>=. \x.rtrn x
e6 = readD d2 >>=. rtrn o Not
e7 = readA a1 >>*.
			[ IfValue ((<.) (lit 42)) (\_.rtrn true)
			, IfValue (\i.i <. lit 26) (\_.rtrn false)
			, IfNoValue (rtrn (lit False))
			] >>=. writeD d13
e8 = ever e7
e9 =
	ever
		((readA a1 >>=. \i.
		  rtrn (((double i *. lit 3.3 /. lit 1024.0) -. lit 0.5) *. lit 100.0)
		 ) >>*.
			[ IfValue ((<.) (lit 20.5)) (\_.rtrn false)
			, IfValue (\i.i <. lit 19.5) (\_.rtrn true)
			, IfNoValue (rtrn false)
			] >>=. writeD d13
		)
e10 = rtrn (lit 6) >>=. \x. rtrn (lit 7) >>=. \y. rtrn (x *. y)
m0 = {main = e9}

m1 =
	fun \i2T = (\i.rtrn (((double (i +. lit 0) *. lit 3.3 /. lit 1024.0) -. lit 0.5) *. lit 100.0))
	In {main =
		ever (
		 	(readA a1 >>=. \a.i2T a) >>*.
				[ IfValue ((<.) (lit 20.2)) (\_.rtrn false)
				, IfValue (\i.i <. lit 19.8) (\_.rtrn true)
				, IfNoValue (rtrn false)
				] >>=. writeD d13
		)
	}

//instance one  (v Real) | arith v where one  = lit 1.0
//instance zero (v Real) | arith v where zero = lit 0.0

facDef :: Int -> Main (MTask v Int) | arith, cond, rtrn v & fun (v Int) v & one (v Int)
facDef x =
	fun \fac = (\n. If (n <. lit 2) one (n *. fac (n -. lit 1)))
	In {main = rtrn (fac (lit x))}

fibDef =
	fun \sub = (\(x, y). x -. y) In
	fun \fib = (\n.If (n <. lit 2) (lit 1) (fib (sub (n, lit 1)) +. fib (sub (n, lit 2)))) In
	{main = rtrn (fib (lit 3))}
//where two = lit 2

f0 =
	fun \plus. (\(a,b).a +. b) In
	{main = rtrn (plus (lit 2, lit 4))}
f1 =
	fun \plus. (\(a,b).a +. b) In
	let p x y = plus (x,y) in
	{main = rtrn (p (lit 2) (lit 4))}
f2 =
	fun \inc. (\a.a +. lit 1) In
	{main = rtrn (inc (lit 6))}
f3 =
	fun \inc. (\a.rtrn (a +. lit 1)) In
	{main = inc (lit 6)}

t1 =
  DHT D0 DHT11 \dht0.
  LCD 16 2 [] \lcd =
  sds \goal = 20.0 In
  sds \temp = 0.0 In
  mtask0 \measure = (\().
    temperature dht0 >>=. \act.
    setSds temp act >>|.
    measure Zero () >>|.
    printAt lcd Zero Zero act) In
  mtask0 \keys = (\().
    getSds goal >>=. \g.
		pressed upButton >>=. \up.
    pressed downButton >>=. \down.
    rtrn (If up (g +. step) (If down (g -. step) g)) >>=. \g2.
    setSds goal g2 >>|.
    keys (ms 100) () >>|.
    printAt lcd Zero One g) In
  mtask0 \control = (\on.
    getSds goal >>=. \g.
    getSds temp >>=. \t.
    If (g >. t &. Not on) (
      writeD (lit D13) true >>|.
      control minOnTime true >>|.
      printAt lcd (lit 11) Zero true
    )(If (g <. t &. on) (
      writeD (lit D13) false >>|.
			control minOnTime false >>|.
      printAt lcd (lit 11) Zero false
    )(control (ms 500) on
    ))
  ) In
  {main =
  	print lcd (lit 42) >>|.
  	measure Zero  () >>|.
  	keys Zero () >>|.
  	control Zero false
  }

fail0 =
	fun \f = (\t. t .||. t) In
	{main = f (rtrn (lit 7))}

c0 =
	fun \r.(\a.rtrn a) In
	{main = r (lit 7)}
 
c1 =
	fun \r.(\a.rtrn a) In
	{main = (r (lit 7)) .&&. rtrn (lit True)}
 
c2 =
	fun \r.(\a.rtrn a) In
	{main = r (lit 7) >>=. \x. r (x +. x)}
 
c3 =
	fun \r.(\a.rtrn a) In
	{main = (r (lit 7) .&&. rtrn (lit True)) >>=. \_ . r (lit 42)}

c4 =
	fun \f.(\x.rtrn (x >. lit 123)) In
	{main = readA a1 >>=. \x.f x >>=. rtrn o (writeD d13)}

blink =
	{main = ever (delayInt (lit 100) (writeD d13 true) >>|. delayInt (lit 900) (writeD d13 false))}


blinkRec0 :: Main (MTask v Bool) | mtask v & fun (v Bool) v
blinkRec0 =
	fun \blink = (\state. writeD d13 state >>|. delay (lit (long 500)) >>|. blink (Not state)) In
	{main = blink true}

blinkRec :: Main (MTask v Bool) | mtask v & fun (v Bool) v
blinkRec =
	fun \blink = (\state. writeD d13 state >>|. delay (lit 500) >>|. blink (Not state)) In
	{main = blink true}

blinkEver :: Main (MTask v ()) | mtask v
blinkEver =
	{main = ever (
		delay (lit 500) >>|.
		readD d13 >>=. \state.
		writeD d13 (Not state)
	)}

blinkSds0 :: Main (MTask v ()) |  mtask v & fun (v Bool) v & fun () v
blinkSds0 =
	sds \state = True In
	fun \blink = (\(). mTask Bool (
		getSds state >>=. \val.
		writeD d13 val >>|.
		delay (lit 500) >>|.
		setSds state (Not val)
	)) In
	{main = ever (mTask Bool (blink ()))}

blinkSds :: Main (MTask v ((),Bool)) | mtask v & fun (v Bool) v 
blinkSds =
  sds \s = True In
  fun \show = (\().getSds s >>=. \val.writeD d13 val) In
  fun \switch = (\b.(delay (lit 500) >>|. setSds s b  >>|. switch (Not b))) In
  {main = ever (show ()) .&&. switch true}

switchedBlink =
  sds \key = True In
  fun \switch = (\().
    readA a0 >>=. \a.
    getSds key >>=. \k.
    setSds key (If (a <. lit 1000) (Not k) k) >>|.
    delay (lit 25) >>|. 
    switch ()) In
  fun \led = (\state.
    getSds key >>=. \k.
    rtrn (k &. Not state) >>=. \state2.
    writeD d13 state2 >>|.
    delay (lit 1000) >>|. 
    led state2) In
  {main = switch () .&&. led false}

blinkSds2 =
	sds \state = True In
	fun \show = (\().
		getSds state >>=. \val.
		writeD d13 val) In
	fun \switch = (\().
		delay (lit 500) >>|.
		getSds state >>=. \val.
		setSds state (Not val)) In
	{main = ever (show () .&&. switch ())}


checkAndWait =
  (readA a1 >>*. [IfValue (\x. x >. lit 100) (\v.rtrn true)])
  .||. (delay (lit 5000) >>*. [Always (rtrn false)])

blink1 =
	fun \t = (\state. writeD d13 state >>|. delayInt (lit 500) (t (Not state)) ) In
	{main = mTask Bool (t true)} 

blink1b =
	fun \t = (\state. mTask Bool (writeD d13 state >>|. delayInt (lit 500) (t (Not state)))) In
	{main = t true} 

blink1c =
	fun \t = (\state. writeD d13 state >>|. delayInt (lit 500) (t (Not state)) ) In
	{main = mTask Bool (t true)} 

forEver :: (MTask v b) -> MTask v Bool | >>*. v & type b
forEver t = t >>*. []

whileT :: ((v b) -> v Bool) (MTask v b) -> MTask v b | arith, rtrn, >>*. v & type b
whileT p t = t >>*. [IfValue (\v.Not (p v)) rtrn]

t2 = 
  DHT D0 DHT11 \dht0.
  LCD 16 2 [] \lcd =
  sds \goal = 20.0 In
  sds \temp = 0.0 In
  mtask0 \measure = (\().
    temperature dht0 >>=. \act.
    setSds temp act >>|.
    measure Zero () >>|.
    printAt lcd Zero Zero act) In
  mtask0 \keys = (\().
    getSds goal >>=. \g.
		pressed upButton >>=. \up.
    pressed downButton >>=. \down.
    rtrn (If up (g +. step) (If down (g -. step) g)) >>=. \g2.
    setSds goal g2 >>|.
    keys (ms 100) () >>|.
    printAt lcd Zero One g) In
  mtask0 \control = (\on.
    getSds goal >>=. \g.
    getSds temp >>=. \t.
    If (g >. t &. Not on) (
      writeD (lit D13) true >>|.
      control minOnTime true >>|.
      printAt lcd (lit 11) Zero true >>|.
      rtrn true
    )(If (g <. t &. on) (
      writeD (lit D13) false >>|.
			control minOnTime false >>|.
      printAt lcd (lit 11) Zero false >>|.
      rtrn false
    )(control (ms 500) on
    ))
  ) In
  mtask0 \cancel = (\().
    pressed leftButton >>=. \p.
    If p (
      writeD (lit D13) false >>|.
      printAt lcd (lit 11) Zero false >>|.
      rtrn false
    )(cancel (ms 25) ()
    )) In
  mtask0 \c2 = (\on.
  	control Zero on .||. cancel Zero () >>=. \s.
  	c2 Zero s >>|. rtrn false) In
  {main =
  	measure Zero () .&&.
  	keys Zero () .&&.
  	c2 Zero false
  }

t4 = 
  DHT D0 DHT22 \dht.
  LCD 16 2 [] \lcd =
  sds \goal = 20.0 In
  sds \temp = 0.0 In
  fun \measure = (\().
    temperature dht >>~. \act.
    setSds temp act >>|.
    printAt lcd Zero Zero act >>|.
    delay (lit 500)) In
  fun \keys = (\().
    getSds goal >>=. \g.
    pressed upButton >>=. \up.
    If up (setSds goal (g +. step)) (rtrn g) >>|.
    pressed downButton >>=. \down.
    If down (setSds goal (g -. step)) (rtrn g) >>|.
    printAt lcd Zero One g >>|.
    delay (lit 100)) In
  fun \control = (\on. mTask () (
    getSds goal >>=. \g.
    getSds temp >>=. \t.
    If (g >. t &. Not on) (
      writeD heating true >>|.
      delay minOnTime >>|.
      control true
    )(If (g <. t &. on) (
      writeD heating false >>|.
      delay minOffTime >>|.
      control false
    )(delay (lit 250) >>|.
      control on)))) In
  {main = control false .&&. ever (measure () .&&. keys ())}

// -- t5 --
t5 = 
  DHT D0 DHT22 \dht.
  LCD 16 2 [] \lcd =
  sds \goal = 20.0 In
  sds \temp = 0.0 In
  fun \measure = measureT dht lcd temp In
  fun \keys = keysT lcd goal temp In
  fun \control = controlT control goal temp In
  {main = control false .&&. ever (measure () .&&. keys ())}

measureT :: (v DHT) (v LCD) (v (Sds Real)) () -> v (TaskValue Int) | mtask v
measureT dht lcd temp () =
    temperature dht >>~. \act.
    setSds temp act >>|.
    printAt lcd Zero Zero act >>|.
    delay (lit 500)

keysT :: (a LCD) (a (Sds Real)) b () -> a (TaskValue Int) | mtask a
keysT lcd goal temp () =
    getSds goal >>=. \g.
    pressed upButton >>=. \up.
    If up (setSds goal (g +. step)) (rtrn g) >>|.
    pressed downButton >>=. \down.
    If down (setSds goal (g -. step)) (rtrn g) >>|.
    printAt lcd Zero One g >>|.
    delay (lit 100)
	    
controlT :: ((v Bool) -> v (TaskValue ())) (v (Sds b)) (v (Sds b)) (v Bool) -> v (TaskValue ()) | Ord b & basicType b & mtask v & fun (v ()) v
controlT control goal temp on =
	 mTask () (
    getSds goal >>=. \g.
    getSds temp >>=. \t.
    If (g >. t &. Not on) (
      writeD heating true >>|.
      delay minOnTime >>|.
      control true
    )(If (g <. t &. on) (
      writeD heating false >>|.
      delay minOffTime >>|.
      control false
    )(delay (lit 250) >>|.
      control on)))
// -- t5 --

t6 = 
  DHT D0 DHT22 \dht.
  LCD 16 2 [] \lcd =
  sds \goal = 20.0 In
  sds \temp = 0.0 In
  fun \measure = (\(). mTask Int (
    temperature dht >>~. \act.
    setSds temp act >>|.
    printAt lcd Zero Zero act >>|.
    delay (lit 500))) In
  fun \keys = 
    (\().mTask Int (
    getSds goal >>=. \g.
    buttonPressed >>=. \b.
    ((b ==. upButton) ? setSds goal (g +. step)) >>|.
    ((b ==. downButton) ? setSds goal (g -. step)) >>|.
    printAt lcd Zero One g >>|.
    delay (lit 100))) In
  fun \control = (\on.mTask () (
    getSds goal >>=. \g.
    getSds temp >>=. \t.
    If (g >. t &. Not on) (
      writeD heating true >>|.
      delay minOnTime >>|.
      control true
    )(If (g <. t &. on) (
      writeD heating false >>|.
      delay minOffTime >>|.
      control false
    )(delay (lit 250) >>|.
      control on)))) In
  {main = control false .&&. ever (measure () .&&. keys ())}


heating = d13
//step = lit 0.5

t3 =
	sds \s = 5 In
	{main =
		getSds s >>=. \v.
		setSds s (v +. lit 1)
	}

step = lit 0.5
minOnTime = ms 60000
minOffTime = ms 90000
ms x = lit x

Zero = lit zero
One = lit one

//Start w  = startEngine ttt w
//ttt = withShared 1 \sds. return sds >>= \s. set 3 s // da mag nie


//Start w = startEngine (simulate {main = ever (readD d0)}) w
//Start w = startEngine (simulate {main = readA a2 .&&. rtrn (lit 6 *. lit 7)}) w
//Start w = startEngine (simulate {main = (readA a2 >>=. rtrn) .&&. (readA a0 >>~. \x. rtrn (lit 6 *. x))}) w
s1 = {main = delayInt (lit 10) (readA a2 >>=. rtrn) .&&. (delay (lit 4) >>|. readA a0 >>~. \x. rtrn (lit 6 *. x))}
s2 = sds \s.1 In s1
s3 = sds \s.1 In {main = ever (getSds s >>=. \n. setSds s (n +. lit 1))}
s4 = fun \f = (\n.lit 1 +. n) In {main = rtrn (f (lit 2))}
s5 = facDef 3
s6 = DHT D0 DHT11 \dht.{main = temperature dht}
s7 =
	sds \s. 0.0 In
	DHT D0 DHT11 \dht.{main = temperature dht >>~. setSds s}
s8 = LCD 16 2 [] \lcd.{main = print lcd (lit "Hello World")}
s9 =
	sds \s. False In
	LCD 16 2 [] \lcd.
	{main = ever (pressed (lit DownButton) >>=. \b. setSds s b)}
s10 = 
	LCD 16 2 [] \lcd.
	{main =
		print lcd (lit "Hello ") >>|.
		print lcd (lit "World") >>|.
		setCursor lcd (lit 1) (lit 1) >>|.
		print lcd (lit "It is me again.1234")
	}
s11 = 
	LCD 16 2 [] \lcd.
	{main =
		buttonPressed >>=. \b.
		print lcd b
	}

blinkSwitch =
	sds \key = True In
	fun \switch = (\().
		ever (
			readA a0 >>=. \v.
			(v <. lit 1000) ? (
				getSds key >>=. \k.
				setSds key (Not k))
			)  >>|. 
			delay (lit 250)) In
	fun \blink = (\s.mTask Bool (
		writeD d13 s >>|.
		delay (lit 1000) >>|.
		getSds key >>=. \k.
		blink (k &. Not s))) In
	{main = switch () .&&. blink true}

voidTask :: (MTask TT ()) -> MTask TT ()
voidTask t = t

Start3 =
	(typeOf 1
	,typeOf (\x.x + 1)
	,typeOf (\x y. x + y + 0)
	)

Start2 =
	[("\ne0: ", makeAST e0)
	,("\ne1: ", makeAST e1)
	,("\ne2: ", makeAST e2)
	,("\ne3: ", makeAST e3)
	,("\ne4: ", makeAST e4)
	,("\ne5: ", makeAST e5)
	,("\ne6: ", makeAST e6)
	,("\ne7: ", makeAST e7)
	,("\ne8: ", makeAST e8)
	,("\ne9: ", makeAST e9)
	,("\ne10: ", makeAST e10)
	,("\nm0:\n", mainAST m0)
	,("\nf2:\n", mainAST f2)
	,("\nf3:\n", mainAST f3)
	,("\nm1:\n", mainAST m1)
	,("\nf0:\n", mainAST f0)
	,("\nf1:\n", mainAST f1)
	,("\nt3:\n", mainAST t3)
	,("\nt4:\n", mainAST t4)
	,("\nt5:\n", mainAST t5)
	,("\nfac 5:\n", mainAST (facDef 5))
	,("\nfib:\n", mainAST fibDef)

	,("\nblink:\n", mainAST blink)
	,("\nblink1:\n", mainAST blink1)
	,("\nblink1b:\n", mainAST blink1b)
	,("\nblink1c:\n", mainAST blink1c)
	,("\nblinkRec:\n", mainAST blinkRec0)
	,("\nblinkRec:\n", mainAST blinkRec)
	,("\nblinkSds:\n", mainAST blinkSds)
	,("\nblinkEver:\n", mainAST blinkEver)
	,("\ncheckAndWait:\n", makeAST checkAndWait)


/*	,("\nt1:\n", mainAST t1)
	,("\nt2:\n", mainAST t2)
	,("\nc0:\n", mainAST c0)
	,("\nc1:\n", mainAST c1)
	,("\nc2:\n", mainAST c2)
	,("\nc3:\n", mainAST c3)
*/	]

Start1 =
	[["e0: ": showIt e0]
	,["e1: ": showIt e1]
	,["e2: ": showIt e2]
	,["e3: ": showIt e3]
	,["e4: ": showIt e4]
	,["e5: ": showIt e5]
	,["e6: ": showIt e6]
	,["e7: ": showIt e7]
	,["e8: ": showIt e8]
	,["e9: ": showIt e9]
	,["m0:\n": showMain m0]
	,["m1:\n": showMain m1]
	,["f0:\n": showMain f0]
	,["f1:\n": showMain f1]
	,["t1:\n": showMain t1]
	,["t2:\n": showMain t2]
	,["c0:\n": showMain c0]
	,["c1:\n": showMain c1]
	,["c2:\n": showMain c2]
	,["c3:\n": showMain c3]
//	,["t4:\n": showMain t4]
	,["blink:\n": showMain blink]
	,["blink1:\n": showMain blink1]
	,["blink1b:\n": showMain blink1b]
	,["blink1c:\n": showMain blink1c]
	,["\ncheckAndWait:\n": showIt checkAndWait]
	]
*/

