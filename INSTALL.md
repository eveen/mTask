# Installation

The instructions are written for linux using bash.
However, with minor adaptations, they can be used for other shells and operating systems as well.

Go to the place where you want to install the system

    cd /home/frobnicator

Download and install the latest greatest clean-bundle-complete nightly.

    curl ftp://ftp.cs.ru.nl/pub/Clean/builds/linux-x64/clean-bundle-complete-linux-x64-latest.tgz | tar -xz

Export `CLEAN_HOME` and add the `bin` directory to `PATH`

    echo 'export CLEAN_HOME="/home/frobnicator/clean-bundle-complete"' >> .bashrc
    echo 'export PATH="$CLEAN_HOME/bin:$PATH"' >> .bashrc

Clone the git repository

    git clone --recursive https://gitlab.science.ru.nl/mlubbers/mtask

Build `CleanSerial`

    make -C dependencies/CleanSerial/src

Create the test project files and compile the framework

    make -C programs
