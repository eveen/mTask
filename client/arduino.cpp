#ifdef ARDUINO

#include <Arduino.h>
#include <stdbool.h>
#include <stdint.h>

#include "arduino.h"
#include "interface.h"
#include "symbols.h"
#include "mem.h"

//Arduino uno stuff
#if defined(ARDUINO_AVR_UNO)
	#include <WString.h>
	#define COMMUNICATOR Serial
	uint8_t apins[] = {A0, A1, A2, A3, A4, A5, A6, A7};
	uint8_t dpins[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13};
//NodeMCU stuff
#elif defined(ARDUINO_ARCH_ESP8266)
	#include <ESP8266WiFi.h>
	#include "wifi.h"
	#define PORT 8123
	WiFiServer server(PORT);
	WiFiClient client;
	#define COMMUNICATOR client
	uint8_t apins[] = {A0};
	uint8_t dpins[] = {D0, D1, D2, D3, D4, D5, D6, D7};
//Unknown arduino device
#else
	#error Unknown arduino device
#endif

#ifdef HAVE_OLEDSHIELD
	#include <Adafruit_GFX.h>
	#include <Adafruit_SSD1306.h>

	#define OLED_RESET 1
	Adafruit_SSD1306 display(OLED_RESET);
#endif

#define getdpin(i) (i < 0 || i >= sizeof(dpins) ? 0 : dpins[i])
#define getapin(i) (i < 0 || i >= sizeof(apins) ? 0 : apins[i])

uint64_t getmillis(void)
{
	return millis();
}

void msdelay(unsigned long ms)
{
	delay(ms);
}

bool input_available(void)
{
#ifdef ARDUINO_ARCH_ESP8266
	if (!client.connected()) {
#ifdef HAVE_OLEDSHIELD
		display.print("Server disconnected");
		display.display();
		delay(1000);
#endif
		reset();
	}
#endif
	return COMMUNICATOR.available();
}

uint8_t read_byte(void)
{
	while (!input_available())
		msdelay(5);
	return COMMUNICATOR.read();
}

void write_byte(uint8_t b)
{
	COMMUNICATOR.write(b);
}

uint8_t read_apin(uint8_t t)
{
	return analogRead(getapin(t));
}

void write_apin(uint8_t p, uint8_t v)
{
	analogWrite(getapin(p), v);
}

bool read_dpin(uint8_t t)
{
	return digitalRead(getdpin(t));
}

void write_dpin(uint8_t p, bool v)
{
	digitalWrite(getdpin(p), v ? HIGH : LOW);
}

#if defined(HAVE_DHT)

#if defined(ARDUINO_AVR_UNO)
#include <dht.h>
dht DHT;
#elif defined(ARDUINO_ESP8266_WEMOS_D1MINI)
#include <WEMOS_SHT3X.h>
SHT3X sht30(0x45);
#endif

#define tpin(i) ((i & 1) > 0 ? getdpin(i >> 1 ) : getapin(i >> 1))
#define dht_real_read(pin, type)\
	switch (type) {\
	case DHT11:\
		DHT.read11(tpin(pin));\
		break;\
	case DHT21:\
		DHT.read21(tpin(pin));\
		break;\
	case DHT22:\
		DHT.read22(tpin(pin));\
		break;\
	}\

void dht_init(uint8_t pin, uint8_t type)
{
	(void)pin;
	(void)type;
}

float get_dht_temp(uint8_t pin, uint8_t type)
{
#if defined(ARDUINO_AVR_UNO)
	dht_real_read(pin, type);
	//msg_log(SC("dht temp: %u\n"), (uint16_t)(DHT.temperature*10.0));
	return DHT.temperature;
#elif defined(ARDUINO_ESP8266_WEMOS_D1MINI)
	sht30.get();
	//msg_log(SC("dht temp: %u\n"), (uint16_t)(sht30.cTemp*10.0));
	return sht30.cTemp;
#endif
}

float get_dht_humidity(uint8_t pin, uint8_t type)
{
#if defined(ARDUINO_AVR_UNO)
	dht_real_read(pin, type);
	//msg_log(SC("dht humid: %u\n"), (uint16_t)(DHT.humidity*10.0));
	return DHT.humidity*10.0;
#elif defined(ARDUINO_ESP8266_WEMOS_D1MINI)
	sht30.get();
	//msg_log(SC("dht humid: %u\n"), (uint16_t)(sht30.humidity*10.0));
	return sht30.humidity*10.0;
#endif
}
#endif

#ifdef HAVE_I2CBUTTON
#include <LOLIN_I2C_BUTTON.h>
I2C_BUTTON button(DEFAULT_I2C_BUTTON_ADDRESS);
void i2c_init(uint8_t addr)
{
	(void)addr;
}

uint8_t i2c_abutton(uint8_t addr)
{
	if (button.get() == 0) {
		return button.BUTTON_A;
	} else {
		die("Button error");
	}
	(void)addr;
}

uint8_t i2c_bbutton(uint8_t addr)
{
	(void)addr;
	if (button.get() == 0) {
		return button.BUTTON_B;
	} else {
		die("Button error");
	}
	(void)addr;
}
#endif

#ifdef HAVE_LEDMATRIX
#include <WEMOS_Matrix_LED.h>

MLED mled(5);

void ledmatrix_init(uint8_t dataPin, uint8_t clockPin)
{
	(void)dataPin;
	(void)clockPin;
}
void ledmatrix_dot(uint8_t x, uint8_t y, bool state)
{
	mled.dot(x, y, state);
}
void ledmatrix_intensity(uint8_t intensity)
{
	mled.intensity = intensity;
}
void ledmatrix_clear(void)
{
	mled.clear();
}
void ledmatrix_display(void)
{
	mled.display();
}
#endif

void real_setup(void)
{
#if defined(ARDUINO_AVR_UNO)
	Serial.begin(9600);
#elif defined(ARDUINO_ARCH_ESP8266)
#ifdef HAVE_OLEDSHIELD
	display.begin(SSD1306_SWITCHCAPVCC, 0x3C);
	display.clearDisplay();
	display.setTextSize(1);
	display.setTextColor(WHITE);
#endif

	digitalWrite(D5, LOW);

	const char *ssids[] = SSIDS;
	const char *wpas[] = WPAS;

	Serial.begin(115200);
	Serial.println("Peripherals:");

#ifdef HAVE_OLEDSHIELD
	Serial.println("Have OLED");
#endif
#ifdef HAVE_LEDMATRIX
	Serial.println("Have LEDMatrix");
	mled.clear();
	mled.display();
#endif
#ifdef HAVE_DHT
	Serial.println("Have DHT");
#endif

	int ssid = -1;
	do {
		Serial.println("Scanning...");
#ifdef HAVE_OLEDSHIELD
		display.println("Scanning..");
		display.display();
#endif
		int n = WiFi.scanNetworks();
		Serial.print("Found ");
		Serial.print(n);
		Serial.println(" networks");
#ifdef HAVE_OLEDSHIELD
		display.print(n);
		display.println(" found");
		display.display();
#endif
		if (n == 0) {
#ifdef HAVE_OLEDSHIELD
			display.println("none found");
			display.display();
#endif
			Serial.println("No networks found");
			delay(1000);
			continue;
		}
		for (unsigned j = 0; j<sizeof(ssids)/sizeof(char *); j++) {
			for (int i = 0; i < n; i++) {
				if (strcmp(WiFi.SSID(i).c_str(),
						ssids[j]) == 0) {
#ifdef HAVE_OLEDSHIELD
					display.print("Try ");
					display.println(WiFi.SSID(i));
					display.display();
#endif
					Serial.print("Connect to: ");
					Serial.println(WiFi.SSID(i));
					ssid = j;
				}
			}
		}
	} while (ssid == -1);
	WiFi.scanDelete();
	WiFi.begin(ssids[ssid], wpas[ssid]);
	while (WiFi.status() != WL_CONNECTED) {
		delay(1000);
		Serial.print(".");
#ifdef HAVE_OLEDSHIELD
		display.print(".");
		display.display();
#endif
	}
	Serial.print("\n");
#ifdef HAVE_OLEDSHIELD
	display.clearDisplay();
	display.setCursor(0, 0);
	display.print("Connected to: ");
	display.print(ssids[ssid]);
	display.print(" ");
	display.print(WiFi.localIP());
	display.print(":");
	display.println(PORT);
	display.display();
#endif
	Serial.println("WiFi connected");
	Serial.println("IP address: ");
	Serial.println(WiFi.localIP());

	server.begin();
	server.setNoDelay(true);
	Serial.print("Server started on port: ");
	Serial.println(PORT);

	Serial.println("Waiting for a client to connect.");
	while (!(client = server.available())) {
		delay(10);
	}

#ifdef HAVE_OLEDSHIELD
	display.println("Client:");
	display.print(client.remoteIP());
	display.display();
#endif
	Serial.println("Client connected\n");
#endif
	pinMode(LED_BUILTIN, OUTPUT);
	digitalWrite(LED_BUILTIN, LOW);
}

void real_yield(void)
{
#ifdef ARDUINO_ARCH_ESP8266
	yield();
#endif
}

#if LOGLEVEL > 0
void msg_log(const char *fmt, ...)
{
#if defined(ARDUINO_AVR_UNO)
	char buf[128];
	va_list args;
	va_start(args, fmt);
	vsnprintf_P(buf,128,fmt,args);
	va_end(args);
	write_byte(MTFDEBUG);
	for (uint8_t i = 0; i<128; i++) {
		if (buf[i] == '\0') {
			write_byte(i);
			break;
		}
	}
	for (uint8_t i = 0; i<128; i++) {
		if (buf[i] == '\0')
			break;
		write_byte(buf[i]);
	}
#else
	char buf[128];
	va_list args;
	va_start(args, fmt);
	vsnprintf_P(buf,128,fmt,args);
	va_end(args);
	Serial.write(buf);
	Serial.write('\r');
#endif
}
#endif

void die(const char *fmt, ...)
{
#ifdef HAVE_OLEDSHIELD
	display.print("Error: ");
	display.print(fmt);
	display.display();
#endif
	Serial.println(fmt);
	while (1) {
		msdelay(1000);
		Serial.print("die");
	}
}

void pdie(char *s)
{
	die(s);
}

void reset(void)
{
#if defined(ARDUINO_AVR_UNO)
	Serial.end();
#elif defined(ARDUINO_ARCH_ESP8266)
#ifdef HAVE_LEDMATRIX
	mled.clear();
	mled.display();
#endif
	client.stop();
#endif
	mem_reset();
	real_setup();
}

#endif
