#ifndef INTERFACE_H
#define INTERFACE_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stdarg.h>
#include <stdbool.h>

#if defined(PC)
#include "pc.h"
#elif defined(ARDUINO)
#include "arduino.h"
#else
#error "No architecture could be detected"
#endif

#if APINS > 255
#error "Maximum number of analog pins is 255"
#endif

#if DPINS > 255
#error "Maximum number of digital pins is 255"
#endif

#if MEMSIZE > UINT16_MAX - 3
#error "Maximum number of memory is 2^16-3"
#endif

#define read16bit() 256*(uint8_t)read_byte() + (uint8_t)read_byte()
#define write16bit(i) {                 \
	write_byte((uint8_t)((i)/256)); \
	write_byte((uint8_t)((i)%256)); \
}

/* Communication */
bool input_available(void);
uint8_t read_byte(void);
void write_byte(uint8_t b);

/* Analog and digital pins */
#if DPINS > 0
void write_dpin(uint8_t i, bool b);
bool read_dpin(uint8_t i);
#endif

#if APINS > 0
void write_apin(uint8_t i, uint8_t a);
uint8_t read_apin(uint8_t i);
#endif

#ifdef HAVE_DHT
//TODO pins are not actually used
void dht_init(uint8_t pin, uint8_t type);
//The pin is encoded:
//bit 0 1 2 3 4 5 6 7
//   | pin number  | bit ? digital : analog
float get_dht_temp(uint8_t pin, uint8_t type);
float get_dht_humidity(uint8_t pin, uint8_t type);
#endif

#ifdef HAVE_LEDMATRIX
//TODO pins are not actually used
void ledmatrix_init(uint8_t dataPin, uint8_t clockPin);
void ledmatrix_dot(uint8_t x, uint8_t y, bool state);
void ledmatrix_intensity(uint8_t intensity);
void ledmatrix_clear(void);
void ledmatrix_display(void);
#endif

#ifdef HAVE_I2CBUTTON
//TODO pins are not actually used
void i2c_init(uint8_t addr);
uint8_t i2c_abutton(uint8_t addr);
uint8_t i2c_bbutton(uint8_t addr);
#endif

/* Delay and communication */
uint64_t getmillis(void);
void msdelay(unsigned long ms);

/* Auxilliary */
void real_setup(void);

/* Yield function to feed the dogs */
void real_yield(void);

/* Log level: debug */
#if LOGLEVEL == 2
void msg_log(const char *, ...);
#define msg_debug(...) msg_log(__VA_ARGS__)
void dump_stack(uint8_t *program, uint16_t pc, uint16_t *stack, uint16_t sp,
	uint16_t fp);

/* Log level: normal */
#elif LOGLEVEL == 1
void msg_log(const char *, ...);
#define msg_debug(...);
#define dump_stack(a, b, c, d, e);

/* Log level: quiet */
#else
#define msg_debug(...) ;
#define msg_log(...) ;
#define dump_stack(a, b, c, d, e);
#endif

void die(const char *, ...);

void pdie(char *s);
void reset(void);

#ifndef RECOVERABLE_ERROR_CODE
#define RECOVERABLE_ERROR_CODE
#endif

#ifdef __cplusplus
}

#endif
#endif
