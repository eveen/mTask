#ifndef REWRITE_H
#define REWRITE_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

uint16_t *rewrite(struct tasktree *t, uint8_t *program, uint16_t *stack);

#ifdef __cplusplus
}
#endif
#endif
