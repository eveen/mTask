#ifdef __MBED__
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>

#include "interface.h"
#include "mbed.h"
#include "symbols.h"

Serial pc(USBTX, USBRX);

DigitalOut led1(LED1);
DigitalOut led2(LED2);
DigitalOut led3(LED3);

DigitalInOut d0(D0); DigitalInOut d1(D1); DigitalInOut d2(D2);
DigitalInOut d3(D3); DigitalInOut d4(D4); DigitalInOut d5(D5);
DigitalInOut d6(D6); DigitalInOut d7(D7); DigitalInOut d8(D8);
DigitalInOut d9(D9); DigitalInOut d10(D10); DigitalInOut d11(D11);
DigitalInOut d12(D12); DigitalInOut d13(D13); DigitalInOut d14(D14);
DigitalInOut d15(D15);

#define SERIAL_BUFSIZE 100
uint8_t buffer[SERIAL_BUFSIZE] = {0};
uint8_t readp = 0, writep = 0;

void ser_cb()
{
	buffer[writep++] = pc.getc();
	if (writep == SERIAL_BUFSIZE)
		writep = 0;
}

//Globals
bool input_available(void)
{
	return writep != readp;
}

uint8_t read_byte(void)
{
	while (!input_available())
		wait_ms(5);
	uint8_t r = buffer[readp++];
	if (readp == SERIAL_BUFSIZE)
		readp = 0;
	return r;
}

void write_byte(uint8_t b)
{
	pc.putc(b);
}

DigitalInOut dpins[] =
	{d0, d1, d2, d3, d4, d5, d6, d7, d8, d9, d10, d11, d12, d13, d14, d15};
void write_dpin(uint8_t i, bool b)
{
	dpins[i].write(b ? 1 : 0);
}

bool read_dpin(uint8_t i)
{
	return dpins[i].read() == 1 ? true : false;
}

void write_apin(uint8_t i, uint8_t a)
{
	//TODO
	(void) i;
	(void) a;
}

uint8_t read_apin(uint8_t i)
{
	//TODO
	return 0;
	(void) i;
}

uint64_t getmillis(void)
{
	return us_ticker_read() / 1000;
}

void msdelay(unsigned long ms)
{
	wait_ms(ms);
}

void real_setup(void)
{
	pc.baud(19200);
	pc.attach(&ser_cb);

}

#if LOGLEVEL > 0
void msg_log(const char *fmt, ...)
{
	char buf[128];
	va_list args;
	va_start(args, fmt);
	vsnprintf(buf,128,fmt,args);
	va_end(args);
	write_byte(MTFDEBUG);
	write_byte(strlen(buf));
	pc.printf("%s", buf);
	write_byte('\r');
}
#endif

void pdie(char *s)
{
	//TODO
	(void)s;
}

void die(const char *fmt, ...)
{
	pc.printf("Dead...\n");
	char b[200] = {0};
	va_list vl;
	va_start(vl, fmt);
	vsnprintf(b, 200, fmt, vl);
	write_byte('m');
	pc.printf(b);
	write_byte('\n');
	while (true)
		wait_ms(50);
	(void)fmt;
}

void reset(void)
{

}
#endif
