#ifndef SYMBOLS_H
#define SYMBOLS_H

#define MT_NULL 65535
#define MT_REMOVE 255
#define MT_STABLE 2
#define MT_UNSTABLE 1
#define MT_NOVALUE 0

//Instructions
#define I_DISPATCH_TABLE\
	{ &&bcreturn1_0\
	, &&bcreturn1_1\
	, &&bcreturn1_2\
	, &&bcreturn1\
	, &&bcreturn2_0\
	, &&bcreturn2_1\
	, &&bcreturn2_2\
	, &&bcreturn2\
	, &&bcreturn3_0\
	, &&bcreturn3_1\
	, &&bcreturn3_2\
	, &&bcreturn3\
	, &&bcreturn_0\
	, &&bcreturn_1\
	, &&bcreturn_2\
	, &&bcreturn\
	, &&bcjumpf\
	, &&bcjump\
	, &&bclabel\
	, &&bcjumpsr\
	, &&bctailcall\
	, &&bcarg0\
	, &&bcarg1\
	, &&bcarg2\
	, &&bcarg3\
	, &&bcarg4\
	, &&bcarg\
	, &&bcarg10\
	, &&bcarg21\
	, &&bcarg32\
	, &&bcarg43\
	, &&bcargs\
	, &&bcsteparg\
	, &&bcmktask\
	, &&bcisstable\
	, &&bcisunstable\
	, &&bcisnovalue\
	, &&bcisvalue\
	, &&bcpush1\
	, &&bcpush2\
	, &&bcpush3\
	, &&bcpush4\
	, &&bcpush\
	, &&bcpushnull\
	, &&bcpop1\
	, &&bcpop2\
	, &&bcpop3\
	, &&bcpop4\
	, &&bcpop\
	, &&bcrot\
	, &&bcdup\
	, &&bcpushptrs\
	, &&bcaddi\
	, &&bcsubi\
	, &&bcmulti\
	, &&bcdivi\
	, &&bcaddl\
	, &&bcsubl\
	, &&bcmultl\
	, &&bcdivl\
	, &&bcaddr\
	, &&bcsubr\
	, &&bcmultr\
	, &&bcdivr\
	, &&bcand\
	, &&bcor\
	, &&bcnot\
	, &&bceqi\
	, &&bcneqi\
	, &&bceql\
	, &&bcneql\
	, &&bclei\
	, &&bcgei\
	, &&bcleqi\
	, &&bcgeqi\
	, &&bclel\
	, &&bcgel\
	, &&bcleql\
	, &&bcgeql\
	, &&bcler\
	, &&bcger\
	, &&bcleqr\
	, &&bcgeqr\
	}
#define BCRETURN1_0 0
#define BCRETURN1_1 1
#define BCRETURN1_2 2
#define BCRETURN1 3
#define BCRETURN2_0 4
#define BCRETURN2_1 5
#define BCRETURN2_2 6
#define BCRETURN2 7
#define BCRETURN3_0 8
#define BCRETURN3_1 9
#define BCRETURN3_2 10
#define BCRETURN3 11
#define BCRETURN_0 12
#define BCRETURN_1 13
#define BCRETURN_2 14
#define BCRETURN 15
#define BCJUMPF 16
#define BCJUMP 17
#define BCLABEL 18
#define BCJUMPSR 19
#define BCTAILCALL 20
#define BCARG0 21
#define BCARG1 22
#define BCARG2 23
#define BCARG3 24
#define BCARG4 25
#define BCARG 26
#define BCARG10 27
#define BCARG21 28
#define BCARG32 29
#define BCARG43 30
#define BCARGS 31
#define BCSTEPARG 32
#define BCMKTASK 33
#define BCISSTABLE 34
#define BCISUNSTABLE 35
#define BCISNOVALUE 36
#define BCISVALUE 37
#define BCPUSH1 38
#define BCPUSH2 39
#define BCPUSH3 40
#define BCPUSH4 41
#define BCPUSH 42
#define BCPUSHNULL 43
#define BCPOP1 44
#define BCPOP2 45
#define BCPOP3 46
#define BCPOP4 47
#define BCPOP 48
#define BCROT 49
#define BCDUP 50
#define BCPUSHPTRS 51
#define BCADDI 52
#define BCSUBI 53
#define BCMULTI 54
#define BCDIVI 55
#define BCADDL 56
#define BCSUBL 57
#define BCMULTL 58
#define BCDIVL 59
#define BCADDR 60
#define BCSUBR 61
#define BCMULTR 62
#define BCDIVR 63
#define BCAND 64
#define BCOR 65
#define BCNOT 66
#define BCEQI 67
#define BCNEQI 68
#define BCEQL 69
#define BCNEQL 70
#define BCLEI 71
#define BCGEI 72
#define BCLEQI 73
#define BCGEQI 74
#define BCLEL 75
#define BCGEL 76
#define BCLEQL 77
#define BCGEQL 78
#define BCLER 79
#define BCGER 80
#define BCLEQR 81
#define BCGEQR 82
#define T_DISPATCH_TABLE\
	{ &&bcstable0\
	, &&bcstable1\
	, &&bcstable2\
	, &&bcstable3\
	, &&bcstable4\
	, &&bcstablenode\
	, &&bcunstable0\
	, &&bcunstable1\
	, &&bcunstable2\
	, &&bcunstable3\
	, &&bcunstable4\
	, &&bcunstablenode\
	, &&bcreadd\
	, &&bcwrited\
	, &&bcreada\
	, &&bcwritea\
	, &&bcrepeat\
	, &&bcdelay\
	, &&bcdelayuntil\
	, &&bctand\
	, &&bctor\
	, &&bcstep\
	, &&bcstepstable\
	, &&bcstepunstable\
	, &&bcseqstable\
	, &&bcsequnstable\
	, &&bcsdsget\
	, &&bcsdsset\
	, &&bcdhttemp\
	, &&bcdhthumid\
	, &&bcledmatrixdisplay\
	, &&bcledmatrixintensity\
	, &&bcledmatrixdot\
	, &&bcledmatrixclear\
	, &&bcabutton\
	, &&bcbbutton\
	}
#define BCSTABLE0 0
#define BCSTABLE1 1
#define BCSTABLE2 2
#define BCSTABLE3 3
#define BCSTABLE4 4
#define BCSTABLENODE 5
#define BCUNSTABLE0 6
#define BCUNSTABLE1 7
#define BCUNSTABLE2 8
#define BCUNSTABLE3 9
#define BCUNSTABLE4 10
#define BCUNSTABLENODE 11
#define BCREADD 12
#define BCWRITED 13
#define BCREADA 14
#define BCWRITEA 15
#define BCREPEAT 16
#define BCDELAY 17
#define BCDELAYUNTIL 18
#define BCTAND 19
#define BCTOR 20
#define BCSTEP 21
#define BCSTEPSTABLE 22
#define BCSTEPUNSTABLE 23
#define BCSEQSTABLE 24
#define BCSEQUNSTABLE 25
#define BCSDSGET 26
#define BCSDSSET 27
#define BCDHTTEMP 28
#define BCDHTHUMID 29
#define BCLEDMATRIXDISPLAY 30
#define BCLEDMATRIXINTENSITY 31
#define BCLEDMATRIXDOT 32
#define BCLEDMATRIXCLEAR 33
#define BCABUTTON 34
#define BCBBUTTON 35
#define MT_DISPATCH_TABLE\
	{ &&mtttask\
	, &&mtttaskprep\
	, &&mtttaskdel\
	, &&mttspecrequest\
	, &&mttshutdown\
	, &&mttsdsupdate\
	}
#define MTTTASK 0
#define MTTTASKPREP 1
#define MTTTASKDEL 2
#define MTTSPECREQUEST 3
#define MTTSHUTDOWN 4
#define MTTSDSUPDATE 5
#define MF_DISPATCH_TABLE\
	{ &&mtftaskack\
	, &&mtftaskprepack\
	, &&mtftaskdelack\
	, &&mtftaskreturn\
	, &&mtfspec\
	, &&mtfsdsupdate\
	, &&mtfexception\
	, &&mtfdebug\
	}
#define MTFTASKACK 0
#define MTFTASKPREPACK 1
#define MTFTASKDELACK 2
#define MTFTASKRETURN 3
#define MTFSPEC 4
#define MTFSDSUPDATE 5
#define MTFEXCEPTION 6
#define MTFDEBUG 7
#define PH_DISPATCH_TABLE\
	{ &&bcdht\
	, &&bcledmatrix\
	, &&bci2cbutton\
	}
#define BCDHT 0
#define BCLEDMATRIX 1
#define BCI2CBUTTON 2
#define DHTTYPE_DISPATCH_TABLE\
	{ &&dht11\
	, &&dht21\
	, &&dht22\
	}
#define DHT11 0
#define DHT21 1
#define DHT22 2
#define BUTTONSTATUS_DISPATCH_TABLE\
	{ &&buttonnone\
	, &&buttonpress\
	, &&buttonlong\
	, &&buttondouble\
	, &&buttonhold\
	}
#define BUTTONNONE 0
#define BUTTONPRESS 1
#define BUTTONLONG 2
#define BUTTONDOUBLE 3
#define BUTTONHOLD 4
#define EXCEPTIONS_DISPATCH_TABLE\
	{ &&mteoutofmemory\
	, &&mteheapunderflow\
	, &&mtestackoverflow\
	, &&mtesdsunknown\
	, &&mtefpexception\
	, &&mtesyncexception\
	, &&mtertserror\
	, &&mteunexpecteddisconnect\
	}
#define MTEOUTOFMEMORY 0
#define MTEHEAPUNDERFLOW 1
#define MTESTACKOVERFLOW 2
#define MTESDSUNKNOWN 3
#define MTEFPEXCEPTION 4
#define MTESYNCEXCEPTION 5
#define MTERTSERROR 6
#define MTEUNEXPECTEDDISCONNECT 7
#endif