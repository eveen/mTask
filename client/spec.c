#include "interface.h"
#include "spec.h"
#include "symbols.h"
#include "mem.h"

void spec_send(void)
{
	write_byte(MTFSPEC);
	write_byte(0
#ifdef HAVE_DHT
		| (1 << 0)
#endif
		);
	write_byte(APINS);
	write_byte(DPINS);
	write16bit(MEMSIZE);
	write_byte('\n');
	msg_log(SC("Done sending spec\n"));
}
