#ifndef INTEPRET_H
#define INTEPRET_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stdbool.h>

#include "task.h"
#define INITSTACKMAIN(stack, sp) {\
	(stack)[(sp)++] = 0; /* stack pointer   */\
	(stack)[(sp)++] = 0; /* frame pointer   */\
	(stack)[(sp)++] = 0; /* program pointer */\
};
#define STACKMETA 3

union floatint {uint32_t intp; float floatp; };

#define uint32_t2float(i) ((float)((union floatint) {.intp=i}).floatp)
#define float2uint32_t(f) ((uint32_t)((union floatint) {.floatp=f}).intp)

bool interpret(uint8_t *program, uint16_t pc, uint16_t sp, uint16_t *stack);

#ifdef __cplusplus
}
#endif
#endif
