#ifndef ARDUINO_CONFIG_H
#define ARDUINO_CONFIG_H
#ifdef ARDUINO

#define SC(s) PSTR(s)

#if defined(ARDUINO_ARCH_ESP8266)
#include <pgmspace.h>

#define APINS 1
#define DPINS 8
#define MEMSIZE 16384
#define SC(s) PSTR(s)
#define LOGLEVEL 1
#define REQUIRE_ALIGNED_MEMORY_ACCESS

#elif defined(ARDUINO_AVR_UNO)

#include <Arduino.h>
#include <WString.h>

#define APINS 6
#define DPINS 14
#define MEMSIZE 1024
#define HAVE_DHT
#define LOGLEVEL 0
#ifndef BAUDRATE
#define BAUDRATE 9600
#endif

#endif

#ifdef ARDUINO_ESP8266_WEMOS_D1MINI
#define HAVE_LEDMATRIX
#define HAVE_OLEDSHIELD
#define HAVE_DHT
//#define HAVE_I2CBUTTON
#endif

#endif
#endif
