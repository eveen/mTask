#ifndef PC_CONFIG_H
#define PC_CONFIG_H

#define APINS 6
#define DPINS 14
#define MEMSIZE 1024
#define HAVE_DHT
#define HAVE_LEDMATRIX
#define HAVE_I2CBUTTON
#define SC(s) s
#ifdef CURSES_INTERFACE
#define LOGLEVEL 2
#else
#define LOGLEVEL 1
#endif

//#define USE_MALLOC

#include <setjmp.h>

extern int gargc;
extern char **gargv;
extern jmp_buf fpe;

#define RECOVERABLE_ERROR_CODE {\
	/*Restore point for SIGFPE exceptions*/\
	if (setjmp(fpe) == 1) {\
		write_byte(MTFEXCEPTION);\
		write_byte(MTEFPEXCEPTION);\
		write_byte(current_task->taskid);\
		write_byte('\n');\
		msg_log(SC("Caught SIGFPE, resetting\n"));\
		mem_reset();\
	}\
}

#endif
