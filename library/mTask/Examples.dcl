definition module mTask.Examples

import mTask.Language

pRet :: Int -> Main (MTask v Int) | mtask v
pLRet :: Long -> Main (MTask v Long) | mtask v
pRRet :: Real -> Main (MTask v Real) | mtask v
pUns :: Int -> Main (MTask v Int) | mtask v
pRetTup :: (Int, Int) -> Main (MTask v (Int, Int)) | mtask v
pTupBind :: (Int, Int) -> Main (MTask v (Int, Int)) | mtask v
pTupBindFst :: (Int, Int) -> Main (MTask v Int) | mtask v
pTupBindSnd :: (Int, Int) -> Main (MTask v Int) | mtask v
pRetTupFst :: (Int, Int) -> Main (MTask v Int) | mtask v
pRetTupSnd :: (Int, Int) -> Main (MTask v Int) | mtask v
pFun0 :: Int -> Main (MTask v Int) | mtask v
pFun1 :: Int -> Main (MTask v Int) | mtask v
pFun2 :: (Int, Int) -> Main (MTask v Int) | mtask v
pFun3a :: (Int, Int, Bool) -> Main (MTask v Int) | mtask v
pFun3b :: (Int, Int, Bool) -> Main (MTask v Int) | mtask v
pFun3c :: (Int, Int, Bool) -> Main (MTask v Bool) | mtask v
pFun4a :: Main (MTask v Int) | mtask v
pFun4b :: Main (MTask v Long) | mtask v
pFun5 :: (Int, Int) -> Main (MTask v (Int, Int)) | mtask v
pFac :: Int -> Main (MTask v Int) | mtask v
pFacL :: Long -> Main (MTask v Long) | mtask v
pFacTl :: Int -> Main (MTask v Int) | mtask v
pFacLTl :: Long -> Main (MTask v Long) | mtask v
pTail :: Int -> Main (MTask v Int) | mtask v
pArith :: Main (MTask v Bool) | mtask v
pReadA :: APin -> Main (MTask v Int) | mtask v
pDelayRet :: Int -> Main (MTask v Int) | mtask v
pReadAOnce :: APin -> Main (MTask v Int) | mtask v
pReadATwice :: APin -> Main (MTask v Int) | mtask v
pWriteD :: (DPin, Bool) -> Main (MTask v Bool) | mtask v
pAio :: Main (MTask v Int) | mtask v
pEver :: Main (MTask v Int) | mtask v
pDio :: Main (MTask v Int) | mtask v
pStep1 :: Main (MTask v Int) | mtask v
pStep2 :: Int -> Main (MTask v Int) | mtask v
pStep3 :: Main (MTask v Int) | mtask v
pRStep1 :: Real -> Main (MTask v Real) | mtask v
pTuple3 :: Main (MTask v (Int, (Int, Int))) | mtask v
pOr1 :: (Int, Int) -> Main (MTask v Int) | mtask v
pOr2 :: (Int, Int) -> Main (MTask v Int) | mtask v
pAnd :: (Int, Int) -> Main (MTask v (Int, Int)) | mtask v
pHugetup :: (Int, Int, Int, Int, Int) -> Main (MTask v (Int, (Int, (Int, (Int, Int))))) | mtask v
pHuge :: (Long, Long, Long) -> Main (MTask v (Long, Long, Long)) | mtask v
pPlus :: (a, a) -> Main (MTask v a) | mtask v
pIPlus :: ((Int, Int) -> Main (MTask v Int)) | mtask v
pLPlus :: ((Long, Long) -> Main (MTask v Long)) | mtask v
pRPlus :: ((Real, Real) -> Main (MTask v Real)) | mtask v
pEq :: (Int, Int) -> Main (MTask v Bool) | mtask v
pLEq :: (Long, Long) -> Main (MTask v Bool) | mtask v
pREq :: (Real, Real) -> Main (MTask v Bool) | mtask v
pLLe :: (Long, Long) -> Main (MTask v Bool) | mtask v
pRLe :: (Real, Real) -> Main (MTask v Bool) | mtask v
pSds1 :: Main (MTask v Int) | mtask v
pSds2 :: Main (MTask v Int) | mtask v
pSds3 :: Main (MTask v Int) | mtask v
pSds4 :: Main (MTask v (Long, (Long, Long))) | mtask v
pSds5 :: Main (MTask v (Int, Int)) | mtask v
pLiftSds :: (Shared Int) -> Main (MTask v Int) | mtask v
pLiftSds1 :: (Shared Int) -> Main (MTask v Int) | mtask v

pBlink :: Int -> Main (MTask v Bool) | mtask v
//pBlinkRec :: Int -> Main (MTask v Bool) | mtask v

pDHT :: (DPin, DHTtype) -> Main (MTask v (Int, Int)) | mtask, dht v

//Excercises
pFib :: Int -> Main (MTask v Int) | mtask v
pAPin42 :: Main (MTask v Int) | mtask v
pSum :: (APin, APin) -> Main (MTask v Int) | mtask v
pSDSBlink :: (Shared Bool) -> Main (MTask v Bool) | mtask v & liftsds v
