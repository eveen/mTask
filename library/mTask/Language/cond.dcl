definition module mTask.Language.cond

/*
	Pieter Koopman
	Radboud University NIjmegen, The Netherlands
	pieter@cs.ru.nl
*/

import mTask.Language.basic
import mTask.Language.types

class cond v
where
	If :: (v Bool) (v t) (v t) -> v t | type t
	(?) infix 1 :: (v Bool) (v t) -> MTask v () | type t & cond v
	(?) p t :== If p t (lit ())
