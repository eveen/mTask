definition module mTask.Language.arith

/*
	Pieter Koopman
	Radboud University NIjmegen, The Netherlands
	pieter@cs.ru.nl
*/

from StdOverloaded import class +, class -, class zero, class one, class *, class /, class <
from StdClass import class Ord, class Eq
import mTask.Language.basic

class arith v where
	lit :: t -> v t | type t
	(+.) infixl 6 :: (v t) (v t) -> v t | basicType, +, zero t
	(-.) infixl 6 :: (v t) (v t) -> v t | basicType, -, zero t
	(*.) infixl 7 :: (v t) (v t) -> v t | basicType, *, zero, one t
	(/.) infixl 7 :: (v t) (v t) -> v t | basicType, /, zero t
	(&.) infixr 3 :: (v Bool) (v Bool) -> v Bool
	(|.) infixr 2 :: (v Bool) (v Bool) -> v Bool
	Not           :: (v Bool) -> v Bool
	(==.) infix 4 :: (v a) (v a) -> v Bool | Eq, basicType a
	(!=.) infix 4 :: (v a) (v a) -> v Bool | Eq, basicType a
	(<.)  infix 4 :: (v a) (v a) -> v Bool | Ord, basicType a
	(>.)  infix 4 :: (v a) (v a) -> v Bool | Ord, basicType a
	(<=.) infix 4 :: (v a) (v a) -> v Bool | Ord, basicType a
	(>=.) infix 4 :: (v a) (v a) -> v Bool | Ord, basicType a
