definition module mTask.Language.i2cbutton

import Data.GenCons
import mTask.Language.def

instance toString ButtonStatus
derive class dyn ButtonStatus
derive class iTask ButtonStatus, I2CButton
derive class gCons ButtonStatus
instance == ButtonStatus
instance type2string ButtonStatus

:: ButtonStatus = ButtonNone | ButtonPress | ButtonLong | ButtonDouble | ButtonHold
:: I2CButton =: I2CButton Int

class i2cbutton v where
	i2cbutton :: !Int ((v I2CButton) -> Main (v b)) -> Main (v b) | type b
    AButton :: !(v I2CButton) -> MTask v ButtonStatus
    BButton :: !(v I2CButton) -> MTask v ButtonStatus
