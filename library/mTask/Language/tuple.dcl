definition module mTask.Language.tuple

from mTask.Language import class type2string
import mTask.Language.basic

class tupl v
where
	first  :: (v (a, b)) -> v a | type a & type b
	second :: (v (a, b)) -> v b | type a & type b
	tupl :: (v a) (v b) -> v (a, b) | type a & type b

/*
 * Transforms a function on a v of tuple to a tuple of v's
 * @param The function to transform
 * @result The transformed function
 * @type ((v a, v b) -> v c) -> ((v (a, b)) -> v c) | tupl v & type a & type b
 */
tupopen f :== \v->f (first v, second v)

instance type2string (a,b) | type2string a & type2string b
instance type2string (a,b,c) | type2string a & type2string b & type2string c
