definition module mTask.Language.lcd

/*
	Pieter Koopman
	Radboud University NIjmegen, The Netherlands
	pieter@cs.ru.nl
*/

//import def, pinIO, combinators
//import mTask.Language.mtask
import Data.GenCons
import mTask.Language.def
import mTask.Language.combinators

:: LCD =
    { lcdtxt    :: [String]
    , lcdId     :: Int
    , cursorPos :: Int
    , cursorLin :: Int
    , sizeW     :: Int
    , sizeH     :: Int
    }

instance toString LCD
instance toString Button
instance type2string Button
instance value Button
derive class dyn LCD, Button 
derive class iTask LCD, Button
derive class gCons LCD, Button
instance == Button
instance basicType Button

:: Button = RightButton | UpButton | DownButton | LeftButton | SelectButton | NoButton

rightButton     :== lit RightButton
upButton        :== lit UpButton
downButton      :== lit DownButton
leftButton      :== lit LeftButton
selectButton    :== lit SelectButton
noButton        :== lit NoButton

class lcd v where
    LCD         :: Int Int [DPin] ((v LCD)->Main (v b)) -> Main (v b) | type b
    print       :: (v LCD) (v t) -> MTask v Int  | type t       // returns bytes written
    setCursor   :: (v LCD) (v Int) (v Int) -> MTask v ()
    scrollLeft  :: (v LCD) -> MTask v ()
    scrollRight :: (v LCD) -> MTask v ()
    pressed     :: (v Button) -> MTask v Bool

class buttonPressed v :: MTask v Button

printAt lcd x y z :== setCursor lcd x y >>|. print lcd z
