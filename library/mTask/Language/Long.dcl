definition module mTask.Language.Long

/*
	Pieter Koopman
	Radboud University NIjmegen, The Netherlands
	pieter@cs.ru.nl
*/

from StdOverloaded import class +, class -, class zero, class one, class *, class /, class <, class toInt, class ~
import mTask.Language.basic

from iTasks.WF.Definition import class iTask
from iTasks.UI.Editor.Generic import generic gEditor, :: Editor
from iTasks.Internal.Generic.Visualization import generic gText, :: TextFormat
from Data.GenDefault import generic gDefault
from Data.GenEq import generic gEq
from Text.GenJSON import generic JSONEncode, generic JSONDecode, :: JSONNode
from Data.Maybe import :: Maybe

:: Long = Long Int

LONG_MAX :== Long  0x7fffffff
LONG_MIN :== Long -0x7fffffff

instance zero Long
instance one Long
instance ~ Long
instance + Long
instance - Long
instance * Long
instance / Long
instance < Long
instance == Long
instance toString Long
instance type2string Long
instance toInt Long
class long n :: n -> Long
instance long Int, Long
class number n | +, -, *, /, <, ==, toString, toInt n
derive class iTask Long
instance basicType Long
derive class dyn Long
