definition module mTask.Language.dht

/*
	Pieter Koopman
	Radboud University NIjmegen, The Netherlands
	pieter@cs.ru.nl
*/

import mTask.Language.def
import mTask.Language.pinIO
import mTask.Language.mtask
import Control.GenBimap

:: DHT = Dht Int
:: DHTtype = DHT11 | DHT21 | DHT22

instance toString DHTtype
derive class gCons DHTtype, DHT
derive class iTask DHTtype, DHT

class dht v where
  DHT         :: p DHTtype ((v DHT)->Main (v b)) -> Main (v b) | type p & pin p & type b
  temperature :: (v DHT) -> MTask v Real
  humidity    :: (v DHT) -> MTask v Real
