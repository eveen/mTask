implementation module mTask.Language.combinators

/*
	Pieter Koopman
	Radboud University NIjmegen, The Netherlands
	pieter@cs.ru.nl
*/

import StdEnv
import mTask.Language
import mTask.Interpret

//instance step v (a, b) (v a, v b) | tupl v & type a & type b & step v (a, b) (v (a, b))
//where
//	(>>*.) l r = l >>*. map patch r
//	(>>=.) ma amb = ma >>*. [IfValue (\_->lit True) \v->amb (first v, second v)]
//	(>>|.) ma mb  = ma >>*. [IfStable (\_->lit True) \_->mb]
//	(>>~.) ma amb = ma >>*. [IfValue (\_->lit True) \v->amb (first v, second v)]
//	(>>..) ma mb  = ma >>*. [IfValue (\_->lit True) \_->mb]
//
////patch :: (Step v (a, b) (v a, v b) u) -> Step v (a, b) (v (a, b)) u | tupl v
//patch (IfValue p c)    = IfValue (\v->p (first v, second v)) (\v->c (first v, second v))
//patch (IfStable p c)   = IfStable (\v->p (first v, second v)) (\v->c (first v, second v))
//patch (IfUnstable p c) = IfUnstable (\v->p (first v, second v)) (\v->c (first v, second v))
//patch (IfNoValue c)    = IfNoValue c
//patch (Always c)       = Always c
