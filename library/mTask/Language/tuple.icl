implementation module mTask.Language.tuple

import StdEnv
import mTask.Language
import Text

instance type2string (a,b) | type2string a & type2string b where
	type2string (a,b) = "(" + type2string a + "," + type2string b + ")"
instance type2string (a,b,c) | type2string a & type2string b & type2string c where
	type2string (a,b,c) = "(" + type2string a + "," + type2string b + "," + type2string c + ")"
