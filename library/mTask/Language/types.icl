implementation module mTask.Language.types

/*
	Pieter Koopman
	Radboud University NIjmegen, The Netherlands
	pieter@cs.ru.nl
*/

import StdMisc
import Control.GenBimap
import iTasks
import Data.GenCons, mTask.Generics.gdynamic
import mTask.Language.basic
import Text

derive class iTask APin, DPin
derive class dyn APin, DPin, TaskValue
derive class gCons APin, DPin, TaskValue

instance toString DPin where toString p = consName{|*|} p
instance toString APin where toString p = consName{|*|} p
instance type2string APin where type2string a = "APin"
instance type2string DPin where type2string a = "DPin"

instance toString (TaskValue a) | toString a where
	toString NoVal = "NoValue"
	toString (Val a s) = "Value " +++ toString a +++ if s " Stable" " Unstable"
instance type2string (TaskValue a) | type2string a
where
	type2string x = type2string (cast x (abort "typeToString undef stub for casting"))

cast :: (v a) a -> a
cast _ i = i

instance type2string (a->b) | type a & type b where
	type2string f =
		let
			a = value
			b = f a
		in type2string a + "->" + type2string b
