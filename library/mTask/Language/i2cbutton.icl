implementation module mTask.Language.i2cbutton

import iTasks

import mTask.Language.def
import Data.GenCons

derive class dyn ButtonStatus
derive class iTask ButtonStatus, I2CButton
derive class gCons ButtonStatus
instance toString ButtonStatus where toString a = toSingleLineText a

instance == ButtonStatus where (==) a b = a === b
instance type2string ButtonStatus where type2string _ = "ButtonStatus"
