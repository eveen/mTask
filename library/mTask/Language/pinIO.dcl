definition module mTask.Language.pinIO

/*
	Pieter Koopman
	Radboud University NIjmegen, The Netherlands
	pieter@cs.ru.nl
*/

import mTask.Language.types

class aio v where
	readA  :: (v APin) -> MTask v Int
	writeA :: (v APin) (v Int) -> MTask v Int

class dio p v | pin p where
	readD :: (v p) -> MTask v Bool
	writeD :: (v p) (v Bool) -> MTask v Bool

:: Pin = AnalogPin APin | DigitalPin DPin
class pin p :: p -> Pin | type p
instance pin APin, DPin
