implementation module mTask.Language.mtask

/*
	Pieter Koopman
	Radboud University NIjmegen, The Netherlands
	pieter@cs.ru.nl
*/

import mTask.Language.types

Bool :: Bool
Bool = True

Int :: Int
Int = 42

Real :: Real
Real = 2.1728
