implementation module mTask.Language.pinIO

/*
	Pieter Koopman
	Radboud University NIjmegen, The Netherlands
	pieter@cs.ru.nl
*/

import mTask.Language.types

instance pin APin where pin p = AnalogPin p
instance pin DPin where pin p = DigitalPin p
