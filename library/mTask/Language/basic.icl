implementation module mTask.Language.basic

/*
	Pieter Koopman
	Radboud University NIjmegen, The Netherlands
	pieter@cs.ru.nl
*/

import mTask.Language.Long

instance toString () where toString _ = "()"

instance basicType Int where basicType = 42
instance basicType Bool where basicType = False
instance basicType Real where basicType = 3.1415
instance basicType Char where basicType = 'p'
instance basicType () where basicType = ()

instance type2string ()     where type2string x = "()"
instance type2string Int    where type2string x = "Int"
instance type2string Bool   where type2string x = "Bool"
instance type2string Char   where type2string x = "Char"
instance type2string Real   where type2string x = "Real"
instance type2string String where type2string x = "String"
