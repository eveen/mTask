definition module mTask.Language.def

/*
	Pieter Koopman
	Radboud University NIjmegen, The Netherlands
	pieter@cs.ru.nl
*/

import mTask.Language.types
import mTask.Language.basic

:: In a b = In infix 0 a b
:: And a b = And infix 1 a b
:: Main m = {main :: m}
main :: a -> Main a
unmain :: (Main a) -> a
:: Sds a = Sds Int

class sds v
where
	sds    :: ((v (Sds t))->In t (Main (MTask v u))) -> Main (MTask v u) | type t & type u
	getSds :: (v (Sds t)) -> MTask v t | type t
	setSds :: (v (Sds t)) (v t) -> MTask v t | type t

class fun a v :: ((a->v s)->In (a->v s) (Main (MTask v u))) -> Main (MTask v u) | type s & type u

//funs :: ((a->v s, b->v t)->In (a->v s, b->v t) (Main (MTask v u))) -> Main (MTask v u) | type s & type t & fun a v & fun b v
