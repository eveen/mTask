definition module mTask.Language.iTasksSDS

from iTasks.SDS.Definition import class RWShared, class Registrable, class Modifiable, class Readable, class Writeable, class Identifiable, :: Shared
import mTask.Language

class liftsds v
where
	liftsds :: ((v (Sds t))->In (Shared sds t) (Main (MTask v u))) -> Main (MTask v u) | type t & type u & RWShared sds
