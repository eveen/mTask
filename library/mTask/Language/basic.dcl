definition module mTask.Language.basic

/*
	Pieter Koopman
	Radboud University NIjmegen, The Netherlands
	pieter@cs.ru.nl
*/

import mTask.Generics.gdynamic
import mTask.AST.basic
from mTask.Interpret.ByteCodeEncoding import generic toByteCode, class toByteWidth, generic fromByteCode, :: FBC

from iTasks.WF.Definition import class iTask, :: Stability, :: TaskValue(..)
from iTasks.UI.Editor.Generic import generic gEditor, :: Editor
from iTasks.Internal.Generic.Visualization import generic gText, :: TextFormat
from Data.GenDefault import generic gDefault
from Data.GenEq import generic gEq
from Text.GenJSON import generic JSONEncode, generic JSONDecode, :: JSONNode
from Data.Maybe import :: Maybe

class type t | toString, typeOf, value, iTask, dyn, toByteWidth, toByteCode{|*|}, fromByteCode{|*|}, type2string t
instance toString ()

class basicType t | type t where basicType :: t
instance basicType Int, Bool, Real, Char, ()

:: SV t = SV String
class showType2 t :: SV t
class showType t | showType2 t
class toCode a :: a -> String
class type2string t :: t -> String
class varName a :: a -> String

instance type2string (), Int, Bool, Char, Real, String
