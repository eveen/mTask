definition module mTask.Language.LEDMatrix

import mTask.Language
import Control.GenBimap

:: LEDMatrix = LEDMatrix Int

derive class gCons LEDMatrix
derive class iTask LEDMatrix

class LEDMatrix v where
	ledmatrix   :: DPin DPin ((v LEDMatrix) -> Main (v b)) -> Main (v b) | type b
	LMDot       :: (v LEDMatrix) (v Int) (v Int) (v Bool) -> MTask v ()
	LMIntensity :: (v LEDMatrix) (v Int) -> MTask v ()
	LMClear     :: (v LEDMatrix) -> MTask v ()
	LMDisplay   :: (v LEDMatrix) -> MTask v ()
