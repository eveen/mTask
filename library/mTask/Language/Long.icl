implementation module mTask.Language.Long

/*
	Pieter Koopman
	Radboud University NIjmegen, The Netherlands
	pieter@cs.ru.nl
*/

import iTasks, mTask.Language.basic

instance zero Long where zero = Long 0
instance one Long where one = Long 1
instance ~ Long where ~ (Long x) = Long (~x)
instance + Long where + (Long x) (Long y) = Long (x + y)
instance - Long where - (Long x) (Long y) = Long (x - y)
instance * Long where * (Long x) (Long y) = Long (x * y)
instance / Long where / (Long x) (Long y) = Long (x / y)
instance < Long where < (Long x) (Long y) = x < y
instance == Long where == (Long x) (Long y) = x == y
instance toString Long where toString (Long l) = "L" +++ toString l
instance type2string Long where type2string l = "Long"
instance long Int where long i = Long i
instance long Long where long l = l
instance toInt Long where toInt (Long i) = i
derive class iTask Long
instance basicType Long where basicType = zero
derive class dyn Long
