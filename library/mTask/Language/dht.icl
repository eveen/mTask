implementation module mTask.Language.dht

/*
	Pieter Koopman
	Radboud University NIjmegen, The Netherlands
	pieter@cs.ru.nl
*/

import Data.GenCons
import mTask.Language.def
import mTask.Language.types
import mTask.Language.pinIO

derive class gCons DHTtype, DHT
derive class dyn DHTtype, DHT
derive class iTask DHTtype, DHT

instance toString DHTtype where toString t = consName{|*|} t
