implementation module mTask.Language.lcd

/*
	Pieter Koopman
	Radboud University NIjmegen, The Netherlands
	pieter@cs.ru.nl
*/

import mTask.Generics.gdynamic
import Data.GenCons
import Control.GenBimap
import iTasks
import mTask.Language.types
import mTask.Language.combinators
import mTask.Interpret.ByteCodeEncoding

instance toString Button where toString b = consName{|*|} b
instance type2string Button where type2string b = "Button"
instance value Button where value = NoButton
derive class dyn Button, LCD
derive class gCons Button, LCD
derive class iTask Button, LCD
instance == Button where == x y = gEq{|*|} x y
instance basicType Button where basicType = NoButton

instance toString LCD where toString lcd = "LCD"

