definition module mTask.Language.types

/*
	Pieter Koopman
	Radboud University NIjmegen, The Netherlands
	pieter@cs.ru.nl
*/

import mTask.Language.arith
import mTask.Language.basic
import mTask.Generics.gdynamic
import Data.GenCons
from iTasks.WF.Definition import :: TaskValue(..)
from iTasks.WF.Definition import class iTask, :: Stability
from iTasks.UI.Editor.Generic import generic gEditor, :: Editor
from iTasks.Internal.Generic.Visualization import generic gText, :: TextFormat
from Data.GenDefault import generic gDefault
from Data.GenEq import generic gEq
from Text.GenJSON import generic JSONEncode, generic JSONDecode, :: JSONNode
from StdString import instance == {#Char}

:: DPin 
   = D0 | D1 | D2 | D3 | D4 | D5 | D6 | D7 | D8 | D9 | D10 | D11 | D12 | D13
:: APin = A0 | A1 | A2 | A3 | A4 | A5
:: PinMode   = INPUT | OUTPUT | INPUT_PULLUP

:: MTaskVal a :== TaskValue a
Val :== Value
NoVal :== NoValue
:: MTask v a :== v (MTaskVal a)

instance toString DPin, APin, (TaskValue a) | toString a
instance type2string APin, DPin, (TaskValue a) | type2string a, (a->b) | type a & type b
derive class iTask APin, DPin
derive class dyn APin, DPin, TaskValue
derive class gCons APin, DPin, TaskValue

a0 :== lit A0
a1 :== lit A1
a2 :== lit A2
a3 :== lit A3
a4 :== lit A4
a5 :== lit A5
d0 :== lit D0
d1 :== lit D1
d2 :== lit D2
d3 :== lit D3
d4 :== lit D4
d5 :== lit D5
d6 :== lit D6
d7 :== lit D7
d8 :== lit D8
d9 :== lit D9
d10 :== lit D10
d11 :== lit D11
d12 :== lit D12
d13 :== lit D13

true :== lit True
false :== lit False
