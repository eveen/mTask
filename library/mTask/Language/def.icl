implementation module mTask.Language.def

/*
	Pieter Koopman
	Radboud University NIjmegen, The Netherlands
	pieter@cs.ru.nl
*/

import StdMisc
import mTask.Language

main :: a -> Main a
main a = {main=a}

unmain :: (Main a) -> a
unmain a = a.main
