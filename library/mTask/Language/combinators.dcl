definition module mTask.Language.combinators

import mTask.Language.basic
import mTask.Language.mtask
import mTask.Language.types

/*
	Pieter Koopman
	Radboud University NIjmegen, The Netherlands
	pieter@cs.ru.nl
*/

class rtrn     v :: (v t) -> MTask v t | type t
class unstable v :: (v t) -> MTask v t | type t

class step v | arith v
where
	(>>*.) infixl 1 :: (MTask v t) [Step v t u] -> MTask v u | type u & type t

	(>>=.) infixl 0 :: (MTask v t) ((v t) -> MTask v u) -> MTask v u | arith v & type u & type t
	(>>=.) ma amb = ma >>*. [IfValue (\_->lit True) amb]

	(>>|.) infixl 0 :: (MTask v t) (MTask v u) -> MTask v u | arith v & type u & type t
	(>>|.) ma mb  = ma >>*. [IfStable (\_->lit True) \_->mb]

	(>>~.) infixl 0 :: (MTask v t) ((v t) -> MTask v u) -> MTask v u | arith v & type u & type t
	(>>~.) ma amb = ma >>*. [IfValue (\_->lit True) amb]

	(>>..) infixl 0 :: (MTask v t) (MTask v u) -> MTask v u | arith v & type u & type t
	(>>..) ma mb  = ma >>*. [IfValue (\_->lit True) \_->mb]

:: Step v t u
  = IfValue    ((v t) -> v Bool) ((v t) -> MTask v u)
  | IfStable   ((v t) -> v Bool) ((v t) -> MTask v u)
  | IfUnstable ((v t) -> v Bool) ((v t) -> MTask v u)
  | IfNoValue                         (MTask v u)
  | Always                            (MTask v u)

class rpeat v :: (MTask v a) -> MTask v () | type a

class (.&&.) infixr 4 v :: (MTask v a) (MTask v b) -> MTask v (a, b) | type a & type b
class (.||.) infixr 3 v :: (MTask v a) (MTask v a) -> MTask v a | type a

class delay v :: (v n) -> MTask v n | type, number n
