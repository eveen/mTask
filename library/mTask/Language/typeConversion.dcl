definition module mTask.Language.typeConversion

/*
	Pieter Koopman
	Radboud University NIjmegen, The Netherlands
	pieter@cs.ru.nl
*/

class int i v :: (v i) -> v Int
class double d v :: (v d) -> v Real
