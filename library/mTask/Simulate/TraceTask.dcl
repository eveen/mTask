definition module mTask.Simulate.TraceTask

/*
	Pieter Koopman
	Radboud University NIjmegen, The Netherlands
	pieter@cs.ru.nl
*/

import mTask.Language

from iTasks.WF.Definition import class iTask, :: Stability, :: TaskValue(..), :: Task
from iTasks.UI.Editor.Generic import generic gEditor, :: Editor
from iTasks.Internal.Generic.Visualization import generic gText, :: TextFormat
from Data.GenDefault import generic gDefault
from Data.GenEq import generic gEq
from Text.GenJSON import generic JSONEncode, generic JSONDecode, :: JSONNode
from Data.Maybe import :: Maybe

:: TraceTask a =
	{ trace :: [String]
	, tskVl :: a
	, nTask :: TT a
	}

derive class iTask TraceTask

:: TT a = TT (Task (TraceTask a))

simulate :: (Main (TT a)) -> Task (TraceTask a) | type a

instance aio TT
instance arith TT
instance buttonPressed TT
instance cond TT
instance delay TT
instance dht TT
instance dio APin TT
instance dio DPin TT
instance rpeat TT
instance lcd TT
instance liftsds TT
instance rtrn TT
instance sds TT
instance step TT
instance tupl TT
instance unstable TT
instance .&&. TT
instance .||. TT
instance fun () TT
instance fun (TT a) TT | type a
//instance fun (TT a) TT | type a & returnType{|*|} a
instance fun (TT a, TT b) TT | type a & type b
instance fun (TT a, TT b, TT c) TT | type a & type b & type c

instance int Int TT
instance double Real TT
instance double Int TT

instance type2string (TT a) | type a

//generic returnType a :: a -> String
//derive returnType UNIT, PAIR, EITHER, CONS of gcd

