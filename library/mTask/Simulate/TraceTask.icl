implementation module mTask.Simulate.TraceTask

/*
	Pieter Koopman
	Radboud University NIjmegen, The Netherlands
	pieter@cs.ru.nl
*/


import mTask.Language
import iTasks
import StdArray
import Data.Functor
import Data.Maybe
import Data.Tuple
import mTask.Interpret.ByteCodeEncoding

derive class iTask TraceTask, TT, ObjectShare, MPC, Dyn, Main
instance == (TaskValue a) | gEq{|*|} a where == x y = x === y

// === Tooling ===

instance Functor TT where
	fmap f (TT tt) = TT (fmap (\r.{trace = r.trace, tskVl = f r.tskVl, nTask = fmap f (TT tt)}) tt)
instance TApplicative TT where
	<#> (TT ft) (TT tt) =
		TT (ft >>= \f. 
			tt >>= \t.
			return
				{ trace = trace2i " " f.trace t.trace
				, tskVl = f.tskVl t.tskVl
				, nTask = (TT ft) <#> (TT tt)
				}
		   )
	return a = TT (return {trace = [], tskVl = a, nTask = return a})
instance TMonad TT where
	>>= (TT x) f = TT (x >>= \xt. unTT (f xt.tskVl) >>= \ft. return {ft & trace = trace2i ">>=" xt.trace ft.trace})
	>>| (TT x) y = TT (x >>= \xt. unTT y            >>= \ft. return {ft & trace = trace2i ">>|" xt.trace ft.trace})

(>>=|) infixl 1 :: (TT a) (a->TT b) -> TT b | iTask a & iTask b
(>>=|) (TT x) f = TT (x >>= \r. unTT (f r.tskVl))

unTask (Task f) = f

(>>||) infixl 1 :: (TT a) (TT b) -> TT b | iTask a & iTask b
(>>||) (TT x) (TT y) = TT (x >>| y)

toTT :: (Task a) -> TT a | type a
toTT t = TT (t >>= \x.return {trace = [toString x], tskVl = x, nTask = toTT t})

// === Interface ===

simulate :: (Main (TT a)) -> Task (TraceTask a) | type a
simulate {main = task} = 
	set mpc0 sharedMPC >>|
	set [] sharedObjectShare >>|
	(mpcTask ||- (
	 run task task -||
     handleObjects)
	)

handleObjects :: Task ()
handleObjects =
  enterChoiceWithShared [ChooseFromGrid id] sharedObjectShare <<@ Title "MTask objects" >>=
  handleObject >>|
  handleObjects

handleObject :: ObjectShare -> Task [ObjectShare]
handleObject obj
  # mbr = fromDyn obj.oValue
  | isJust mbr
  = handleReal (fromJust mbr) obj
  # mbi = fromDyn obj.oValue
  | isJust mbi
  = handleInt (fromJust mbi) obj
  # mbb = fromDyn obj.oValue
  | isJust mbb
  = handleBool (fromJust mbb) obj
  # mblcd = fromDyn obj.oValue
  | isJust mblcd
  = handleLCD (fromJust mblcd) obj
  # mbdht = fromDyn obj.oValue
  | isJust mbdht
  = handleDHT (fromJust mbdht) obj
  = viewInformation [] obj <<@ Title obj.oType >>|
  	get sharedObjectShare

handleDHT :: DHTObject ObjectShare -> Task [ObjectShare]
handleDHT dht obj =
  updateInformation  [] dht <<@ Title (obj.oType + " " + toString obj.oId) >>= \new.
  upd (updateAt obj.oId {obj & oValue = toDyn {new & dhtId = dht.dhtId}}) sharedObjectShare

handleLCD :: LCD ObjectShare -> Task [ObjectShare]
handleLCD lcd obj =
  viewInformation [] lcd.lcdtxt <<@ Title (obj.oType + " " + toString obj.oId) >>*
  [ OnAction (Action "Right") (always (updateAPin A0 25))
  , OnAction (Action "Up") (always (updateAPin A0 100))
  , OnAction (Action "Down") (always (updateAPin A0 250))
  , OnAction (Action "Left") (always (updateAPin A0 450))
  , OnAction (Action "Select") (always (updateAPin A0 700))
  , OnAction (Action "No button") (always (updateAPin A0 1000))
  ] >>| get sharedObjectShare

handleReal :: Real ObjectShare -> Task [ObjectShare]
handleReal real obj =
  updateInformation [] real <<@ Title (obj.oType + " " + toString obj.oId) >>= \new.
  upd (updateAt obj.oId {obj & oValue = toDyn new}) sharedObjectShare

handleInt :: Int ObjectShare -> Task [ObjectShare]
handleInt int obj =
  updateInformation [] int <<@ Title (obj.oType + " " + toString obj.oId) >>= \new.
  upd (updateAt obj.oId {obj & oValue = toDyn new}) sharedObjectShare

handleBool :: Bool ObjectShare -> Task [ObjectShare]
handleBool bool obj =
  updateInformation [] bool <<@ Title (obj.oType + " " + toString obj.oId) >>= \new.
  upd (updateAt obj.oId {obj & oValue = toDyn new}) sharedObjectShare

mpcTask :: Task MPC
mpcTask =
	updateSharedInformation [] sharedMPC <<@ Title "MPC state"
	>>* [OnAction ActionOk   (ifValue sound (\_.mpcTask))
		,OnAction ActionNext (always (upd (\mpc.{mpc & clock = mpc.clock + mpc.delay}) sharedMPC >>| mpcTask))
		]
	where
		sound {dpins, apins} =
			isUnique (map fst dpins) &&
			isUnique (map fst apins) &&
			and [0 <= i && i < 1024 \\ (p,i) <- apins]

run :: (TT a) (TT a) -> Task (TraceTask a) | type a
run (TT task) tt0 =
	task >>= \{trace, tskVl, nTask}.
		viewInformation [] trace <<@ Title "Result" <<@ Label ("Task result: " + toString tskVl)
		>>* [OnAction ActionNext (always (upd (\mpc.{mpc & clock = mpc.clock + mpc.delay}) sharedMPC >>| run nTask tt0))
			,OnAction (Action "Reset") (always (set mpc0 sharedMPC >>| set [] sharedObjectShare >>| run tt0 tt0)) 
		    ]

// === MTask DSL ===

instance arith TT where
  lit x = TT (return {trace = [toString x], tskVl = x, nTask = TT (return {trace = [toString x], tskVl = x, nTask = lit x})})
  (+.) x y = binopT "+." (+) x y
  (-.) x y = binopT "-." (-) x y
  (*.) x y = binopT "*." (*) x y
  (/.) x y = binopT "/." (/) x y
  (&.) x y = binopT "&." (&&) x y
  (|.) x y = binopT "|." (||) x y
  Not  x   = sinopT "Not" not x
  (==.) x y = binopT "==." (==) x y
  (!=.) x y = binopT "!=." (<>) x y
  (<.)  x y = binopT "<." (<) x y
  (>.)  x y = binopT ">." (>) x y
  (<=.) x y = binopT "<=." (<=) x y
  (>=.) x y = binopT ">=." (>=) x y

binopT :: String (a b -> c) (TT a) (TT b) -> TT c | iTask a & iTask b & type c 
binopT name op (TT x) (TT y) =
 TT (x >>= \x=:{tskVl = xv}. y >>= \y=:{tskVl = yv}. return 
  let v = op xv yv in
  { trace = trace2i name x.trace y.trace
  , tskVl = v
  , nTask = lit v
  } )

sinopT :: String (a -> c) (TT a) -> TT c | iTask a & type c
sinopT name op (TT x) =
 TT (x >>= \x=:{tskVl = xv}.
   let v = op xv in
     return
       { trace = trace1 name x.trace
       , tskVl = v
       , nTask = lit v
       })

undef :: a
undef = hd []

instance aio TT where
	readA (TT pt) =
		TT (pt >>= \{tskVl = p, trace = ptt}. 
		    get sharedMPC >>= \mpc. 
		     (let i = lookupPin p 0 mpc.apins in 
		     return
		     	{ trace = trace2 "readA" ptt ["%" + toString i + "%"]
		     	, tskVl = Value i True
		     	, nTask = readA (TT pt)
		     	})
		    )
	writeA (TT pt) (TT it) = 
		TT (pt >>= \{tskVl = p, trace = ptt}.
		    it >>= \{tskVl = i, trace = itt}.
		    upd (\mpc.{mpc & apins = updatePin p i mpc.apins}) sharedMPC >>|
		    return
		    	{ trace = trace2 "writeA" ptt itt
		    	, tskVl = Value i True
		    	, nTask = writeA (TT pt) (TT it)
		    	})

instance dio APin TT where
	readD (TT pt) =
		TT (pt >>= \{tskVl = p, trace = ptt}. 
		    get sharedMPC >>= \mpc. 
		     (let b = lookupPin p 0 mpc.apins <> 0 in 
		     return
		     	{ trace = trace2 "readD " ptt  ["%" + toString b + "%"]
		     	, tskVl = Value b True
		     	, nTask = readD (TT pt)
		     	})
		    )
	writeD (TT pt) (TT bt) = 
		TT (pt >>= \{tskVl = p, trace = ptt}.
		    bt >>= \{tskVl = b, trace = btt}.
		    let i = if b 1 0 in
		    upd (\mpc.{mpc & apins = updatePin p i mpc.apins}) sharedMPC >>|
		    return
		    	{ trace = trace2 "writeD " ptt btt
		    	, tskVl = Value b True
		    	, nTask = writeD (TT pt) (TT bt)
		    	})
instance dio DPin TT where
	readD (TT pt) =
		TT (pt >>= \{tskVl = p, trace = ptt}. 
		    get sharedMPC >>= \mpc. 
		     (let b = lookupPin p False mpc.dpins in 
		     return
		     	{ trace = trace2 "readD " ptt  ["%" + toString b + "%"]
		     	, tskVl = Value b True
		     	, nTask = readD (TT pt)
		     	})
		    )
	writeD (TT pt) (TT bt) = 
		TT (pt >>= \{tskVl = p, trace = ptt}.
		    bt >>= \{tskVl = b, trace = btt}.
		    upd (\mpc.{mpc & dpins = updatePin p b mpc.dpins}) sharedMPC >>|
		    return
		    	{ trace = trace2 "writeD" ptt btt
		    	, tskVl = Value b True
		    	, nTask = writeD (TT pt) (TT bt)
		    	})

instance rtrn TT where
	rtrn (TT vt) =
		TT (vt >>= \{tskVl = v, trace = tr}.
			return
				{ trace = trace1 "rtrn" tr
				, tskVl = Value v True
				, nTask = rtrn (lit v)
				}
			)

instance unstable TT where
	unstable (TT vt) =
		TT (vt >>= \{tskVl = v, trace = tr, nTask = nvt}.
			return
				{ trace = trace1 "rtrn" tr
				, tskVl = Value v False
				, nTask = unstable nvt
				}
			)

instance rpeat TT where
	//TODO
	rpeat _ = undef
//	rpeat (TT tt) =
//		TT (tt >>= \tv.
//			return
//				{ trace = trace1 "rpeat" tv.trace
//				, tskVl = NoValue
//				, nTask = tv.nTask >>|. rpeat (TT tt)
//				})

instance .&&. TT where
	.&&. (TT xt) (TT yt) =
		TT (xt >>= \x.
			yt >>= \y.
			case (x.tskVl, y.tskVl) of
				(Value xv True, Value yv True) =
					return
						{ trace = trace2i ".&&." x.trace y.trace
						, tskVl = Value (xv, yv) True
						, nTask = rtrn (lit (xv, yv))
						}
				(Value xv _, Value yv _) =
					return
						{ trace = trace2i ".&&." x.trace y.trace
						, tskVl = Value (xv, yv) False
						, nTask = x.nTask .&&. y.nTask
						}
				(_, _) =
					return
						{ trace = trace2i ".&&." x.trace y.trace
						, tskVl = NoValue
						, nTask = x.nTask .&&. y.nTask
						}
		   )

instance .||. TT where
	.||. (TT xt) (TT yt) =
		TT (xt >>= \x.
			case x.tskVl of
				Value xv True =
					return
						{ trace = trace2i ".||." x.trace [".."]
						, tskVl = x.tskVl
						, nTask = rtrn (lit xv)
						}
				_ = yt >>= \y.
					case x.tskVl of
						Value yv True =
							return
								{ trace = trace2i ".||." x.trace y.trace
								, tskVl = y.tskVl
								, nTask = rtrn (lit yv)
								}
						_ =
							return
								{ trace = trace2i ".||." x.trace y.trace
								, tskVl = NoValue
								, nTask = x.nTask .||. y.nTask
								}
		   )

instance tupl TT where
	first x = sinopT "fst" fst x
	second x = sinopT "snd" snd x
	tupl x y = binopT "(,)" tuple x y

instance step TT
where
	>>*. (TT xt) steps =
		 TT (xt >>= \x.unTT (matchSteps x.tskVl steps
				(TT (return
					{ trace = trace2i ">>*." x.trace ["[..]"]
					, tskVl = NoValue
					, nTask = x.nTask >>*. steps
					}
				)))
		   )
import Data.Func, StdDebug

matchSteps :: (TaskValue t) [Step TT t u] (MTask TT u) -> MTask TT u | type t & type u
matchSteps val=:(Value v _) [IfValue p t:r] default = trace "ifValue" $
	TT ((unTT (p (lit v))) >>= \{tskVl = b}.
		if b
			(unTT (t (lit v)))
			(unTT (matchSteps val r default))
	   )
matchSteps val=:(Value v True) [IfStable p t:r] default = trace "ifStable" $
	TT ((unTT (p (lit v))) >>= \{tskVl = b}.
		if b
			(unTT (t (lit v)))
			(unTT (matchSteps val r default))
	   )
matchSteps val=:(Value v False) [IfUnstable p t:r] default = trace "ifUnstable" $
	TT ((unTT (p (lit v))) >>= \{tskVl = b}.
		if b
			(unTT (t (lit v)))
			(unTT (matchSteps val r default))
	   )
matchSteps NoValue [IfNoValue t:r] default = trace "ifNoValue" t
matchSteps val     [Always    t:r] default = trace "always" t
matchSteps val     [step       :r] default = matchSteps val r default
matchSteps val     []              default = default

instance delay TT where
	delay (TT dt) =
		TT (dt >>= \d.
		    get sharedMPC >>= \mpc.
		    let dv = toInt d.tskVl
		    	dv2 = dv - mpc.delay in
		    if (dv > 0)
		      (return
		      	{ trace = trace1 "delay" d.trace
		      	, tskVl = NoValue
		      	, nTask = delay (lit d.tskVl)
		      	}
		      )
		      (return {trace = ["delay " + toString dv], tskVl = Value d.tskVl True, nTask = rtrn (lit d.tskVl)})
		   )

instance cond TT where
	If (TT ct) tt et =
		TT (ct >>= \bv.
			if bv.tskVl
				(unTT tt >>= \r. return {r & trace = trace3 "If" bv.trace r.trace ["(..)"]})
				(unTT et >>= \r. return {r & trace = trace3 "If" bv.trace ["(..)"] r.trace})
		   )

instance fun () TT where
	fun def =
		{main =
			(toTT freshOId >>=| \i.
			let (fun In body) = def (\a.setTraceTT ["fun"+toString i+" ()"] (fun a))
			in  toTT (get sharedObjectShare) >>=| \objects. 
				fun () >>=| \exp.
				toTT (set (objects ++ [{oId = i, oType = "Fun ()->" + type2string exp , oValue = Dyn []}]) sharedObjectShare) >>||
				body.main				
		   )
		}

toByteCode{|ObjectShare|} _ = ""
instance toByteWidth [ObjectShare] where toByteWidth _ = 0
fromByteCode{|ObjectShare|} = fail "Not implemented"

setTraceTT :: [String] (TT a) -> TT a | type a
setTraceTT trace x = TT (unTT x >>= \r. return {r & trace = trace})

setTrace :: [String] (Task (TraceTask a)) -> (Task (TraceTask a)) | type a
setTrace trace x =  x >>= \r.return {r & trace = trace}


instance value [a] where value = []
instance toString ObjectShare where toString _ = "ObjectShare"
instance toChar ObjectShare where toChar _ = 'O'
instance type2string ObjectShare where type2string _ = "ObjectShare"
instance type2string [a] | type2string a where type2string l = "[" + type2string (hd l) + "]"
derive class dyn ObjectShare, Dyn
instance fun (TT a) TT | type a where
	fun def =
		{main =
			(toTT freshOId >>=| \i.
			let (fun In body) = def (\(TT a).TT (a >>= \av.setTrace ["fun"+toString i+" "+toString av.tskVl] (unTT (fun (TT (return av))))))
				v = value
			in  toTT (get sharedObjectShare) >>=| \objects.
				fun (lit v) >>=| \exp.
				toTT (set (objects ++ [{oId = i, oType = "Fun " + type2string v + "->" + type2string exp, oValue = Dyn []}]) sharedObjectShare ) >>|| 
				body.main				
		   )
		}
instance fun (TT a, TT b) TT | type a & type b where
	fun def =
		{main =
			(toTT freshOId >>=| \i.
			let (fun In body) = 
					def (\(TT a,TT b).TT (a >>= \av. b >>- \bv.
					setTrace ["fun"+toString i+" ("+toString av.tskVl+","+toString bv.tskVl+")"] 
					(unTT (fun (TT (return av), TT (return bv))))))
				a = value
				b = value
			in toTT (get sharedObjectShare) >>=| \objects.
				fun (lit a, lit b) >>=| \exp.
				toTT (set (objects ++ [{oId = i, oType = "Fun (" + type2string a + "," + type2string b + ")->" + type2string exp, oValue = Dyn []}]) sharedObjectShare) >>||
				body.main				
		   )
		}

instance fun (TT a, TT b, TT c) TT | type a & type b & type c where
	fun def =
		{main =
			(toTT freshOId >>=| \i.
			let (fun In body) =
					def (\(TT a,TT b,TT c).
						TT (a >>= \av. b >>- \bv. c >>= \cv.
						setTrace ["fun"+toString i+" ("+toString av.tskVl+","+toString bv.tskVl+","+toString cv.tskVl+")"]
						(unTT (fun (TT (return av), TT (return bv), TT (return cv))))))
				a = value
				b = value
				c = value
			in  toTT (get sharedObjectShare) >>=| \objects.
				fun (lit a, lit b, lit c) >>=| \exp.
				toTT (set (objects ++ [{oId = i, oType = "Fun (" + type2string a + "," + type2string b  + "," + type2string c + ")->" + type2string exp, oValue = Dyn []}]) sharedObjectShare) >>||
				body.main				
		   )
		}

instance type2string (TT a) | type a where
	type2string tt = 
		let a = value
		in k3 tt (lit a) (type2string a)
instance type2string (Main a) | type2string a where type2string m = type2string m.main
instance type2string (TraceTask a) | type a where
	type2string tt =
		let a = value
		in k3 tt {trace = [], tskVl = a, nTask = return a} (type2string a)

k3 :: a a b -> b
k3 x y z = z

instance liftsds TT where
	liftsds def =
		{main =
			toTT freshOId >>=| \i.
			let
				sds = TT (return {trace = [sdsName i], tskVl = Sds i, nTask = sds}) 
				(tt In e) = def sds
			in  toTT (get tt >>= \t->upd (\l.l ++ [{oId = i, oType = "SDS " + type2string t, oValue = toDyn t}]) sharedObjectShare >>|
			          return i) >>|| // to fix type of toTT
				TT (syncDown tt i ||- syncUp tt i ||- unTT e.main)
		}
	where
		syncDown :: (Shared sds a) Int -> Task a | type a & RWShared sds
		syncDown sh ident = whileUnchanged sh \val->
			upd (applyAt ident (\v->{v & oValue=toDyn val})) sharedObjectShare
			@? const NoValue

		syncUp :: (Shared sds a) Int -> Task a | type a & RWShared sds
		syncUp sh ident = whileUnchanged
			(mapRead (\os->os!!ident) sharedObjectShare)
				\val->set (fromJust (fromDyn val.oValue)) sh @? const NoValue

instance sds TT where
	sds def =
		{main =
			toTT freshOId >>=| \i.
			let
				sds = TT (return {trace = [sdsName i], tskVl = Sds i, nTask = sds}) 
				(t In e) = def sds
			in  toTT (upd (\l.l ++ [{oId = i, oType = "SDS " + type2string t, oValue = toDyn t}]) sharedObjectShare >>|
			          return i) >>|| // to fix type of toTT
				e.main
		}
	getSds (TT sdst) =
		TT ( sdst >>= \sds=:{tskVl = Sds i}.
			 get sharedObjectShare >>= \sdss.
			 return
			 	{ trace = ["getSds " + sdsName i]
			 	, tskVl = Value (fromJust (fromDyn (sdss !! i).oValue)) True
			 	, nTask = getSds (TT sdst)
			 	} 
		   )
	setSds (TT sdst) (TT vt) =
		TT ( sdst >>= \{tskVl = Sds i}.
			 vt >>= \v.
			 upd (applyAt i (\sds.{sds & oValue = toDyn v.tskVl})) sharedObjectShare >>|
			 return
			 	{ trace = trace2 "setSds" [sdsName i] v.trace
			 	, tskVl = Value v.tskVl True
			 	, nTask = setSds (TT sdst) (TT vt)
			 	} 
		   )

instance dht TT where
	DHT p dht_t def = 
		{main =
			   toTT freshOId >>=| \i.
				let
					dht = Dht i
					dhtTask = TT (return
							{ trace = ["dht"+toString i]
							, tskVl = dht
							, nTask = dhtTask
							})
				in toTT (upd (\l.l ++ [
							{ oId = i
							, oType = toString dht_t + " (" + toString p + ")"
							, oValue = toDyn {dhtId=i,temperature=42.0,humidity=70.0}
							}]) sharedObjectShare >>|
			       		return i
			       		) >>||
			   (def dhtTask).main
		}
	temperature (TT dhtTask) = 
		TT ( dhtTask >>= \{tskVl = Dht i}.
			 get sharedObjectShare >>= \sdss.
			 return
			 	{ trace = ["temperature " + dhtName i]
			 	, tskVl = Value (fromJust (fromDyn (sdss !! i).oValue)).temperature False
			 	, nTask = temperature (TT dhtTask)
			 	} 
		   )
	humidity (TT dhtTask) =
		TT ( dhtTask >>= \{tskVl = Dht i}.
			 get sharedObjectShare >>= \sdss.
			 return
			 	{ trace = ["humidity " + dhtName i]
			 	, tskVl = Value (fromJust (fromDyn (sdss !! i).oValue)).humidity False
			 	, nTask = humidity (TT dhtTask)
			 	} 
		   )

instance lcd TT where
	LCD len lines pins def =
		{main =
			toTT freshOId >>=| \i.
			let	lcd =
				    { lcdId     = i
				    , lcdtxt    = repeatn lines (toString (repeatn len '.'))
				    , cursorPos = 0
				    , cursorLin = 0
				    , sizeH     = lines
				    , sizeW     = len
				    }
			    lcdTask =
			    	TT (return
			    		{ trace = ["lcd"+toString i]
			    		, tskVl = lcd
			    		, nTask = lcdTask
			    		})
			in toTT (upd (\l. l ++ [
							{ oId = i
							, oType = "LCD"
							, oValue = toDyn lcd
							}]) sharedObjectShare >>|
			       		return i
					) >>||
			(def lcdTask).main
		}
	print (TT lcdt) (TT xt) =
		TT (lcdt >>= \lcd.
			xt >>= \x.
			let
				str = toString x.tskVl
				len = size str		  
				newLCD dyn = 
					case fromDyn dyn of
						Just lcd 
							# nPos = min (lcd.sizeW - 1) (lcd.cursorPos + len)
							  updLine =
							  	\line.
							  		line % (0, lcd.cursorPos - 1) +
							  		str % (0, min len (nPos - lcd.cursorPos) - 1) + 
							  		line % (nPos, lcd.sizeW - 1)
							= toDyn {lcd & lcdtxt = applyAt lcd.cursorLin updLine lcd.lcdtxt, cursorPos = nPos}
						Nothing  = dyn
			in
			upd (applyAt lcd.tskVl.lcdId (\lcd.{lcd & oValue = newLCD lcd.oValue})) sharedObjectShare >>|
			return
				{ trace = trace2 "print" lcd.trace x.trace
				, tskVl = Value len True
				, nTask = rtrn (lit len)
				}
		   )
	setCursor (TT lcdt) (TT xt) (TT yt) =
		TT (lcdt >>= \lcd.
			xt >>= \x.
			yt >>= \y.
			let
				newLCD dyn = 
					case fromDyn dyn of
						Just lcd = toDyn {lcd & cursorPos = x.tskVl, cursorLin = y.tskVl}
						Nothing  = dyn
			in
			upd (applyAt lcd.tskVl.lcdId (\lcd.{lcd & oValue = newLCD lcd.oValue})) sharedObjectShare >>|
			return
				{ trace = trace3 "setCursor" lcd.trace x.trace y.trace
				, tskVl = Value () True
				, nTask = rtrn (lit ())
				}
		   )
	scrollLeft (TT lcd) = rtrn (lit ())
	scrollRight (TT lcd) = rtrn (lit ())
	pressed (TT bt) =
		readA a0 >>=| \(Value v _).
		TT (bt >>= \b.
		    return
		    	{ trace = trace1 "pressed" b.trace
		    	, tskVl = Value (
		    		case b.tskVl of 
		    			RightButton		= v < 50
		    			UpButton		= 50 <= v && v < 195
		    			DownButton		= 195 <= v && v < 380
		    			LeftButton		= 380 <= v && v < 555
		    			SelectButton	= 555 <= v && v < 790
		    			NoButton		= 790 <= v)
		    		True
		    	, nTask = pressed (TT bt)
		    	}
		   )

instance buttonPressed TT where
	buttonPressed =
		readA a0 >>=| \(Value v _).
		TT (return
		    	{ trace = ["isPressed"]
		    	, tskVl = Value (
		    		if (v < 50)
		    			RightButton
		    		(if (v < 195)
		    			UpButton
		    		(if (v < 380)
		    			DownButton
		    		(if (v < 550)
		    			LeftButton
		    		(if (v < 790)
		    			SelectButton
		    			NoButton
		    		)))))
		    		True
		    	, nTask = buttonPressed
		    	}
		   )

instance int Int TT where int i = i
instance double Real TT where double d = d
instance double Int TT where
	double it =
		it >>=| \i.
		let d = toReal i
		in TT (return {trace = ["double " + toString i], tskVl = d, nTask = return d})

// --- tools ---

derive class iTask Sds

unTT :: (TT t) -> Task (TraceTask t)
unTT (TT t) = t

trace1 :: String [String] -> [String]
trace1 n [x] = ["(" + n + " " + x + ")"]
trace1 n x   = [n:indent x]

trace2 :: String [String] [String] -> [String]
trace2 n [x] [y] = ["(" + n + " " + x + " " + y + ")"]
trace2 n x   y   = [n:indent (x ++ y)]

trace3 :: String [String] [String] [String] -> [String]
trace3 n [x] [y] [z] = ["(" + n + " " + x + " " + y + " " + z + ")"]
trace3 n x   y   z   = [n:indent (x ++ y ++ z)]

trace2i :: String [String] [String] -> [String]
trace2i n [x] [y] = ["(" + x + n + y + ")"]
trace2i n x   y   = [n:indent (x ++ y)]

lookupPin :: p v [(p,v)] -> v | == p
lookupPin p v [] = v
lookupPin p v [(q,w):r] | p == q
	= w
	= lookupPin p v r

updatePin :: p v [(p,v)] -> [(p,v)] | == p
updatePin p v [] = [(p,v)]
updatePin p v [(q,w):r] | p == q
	= [(q,v):r]
	= [(q,w):updatePin p v r]

:: MPC =
	{ dpins :: [(DPin, Bool)]
	, apins :: [(APin, Int)]
	, clock :: Int
	, delay :: Int
	}

mpc0 :: MPC
mpc0 =
	{ dpins = [] //[(D0, True), (D1, False)]
	, apins = [] //[(A0, 0), (A2, 1023)]
	, clock = 0
	, delay = 1
	}

sharedMPC :: SimpleSDSLens MPC
sharedMPC = sharedStore "mpc" mpc0

isUnique :: [a] -> Bool | Eq a
isUnique [] = True
isUnique [a:x] = not (isMember a x) && isUnique x

indent :: ([String] -> [String])
indent = map ((+)"  ")

updateAPin :: APin Int -> Task MPC
updateAPin p i = upd (\mpc.{mpc & apins = updatePin p i mpc.apins}) sharedMPC

instance == APin where == x y = x === y
instance == DPin where == x y= x === y

:: ObjectShare =
	{ oId    :: Int
	, oType  :: String
	, oValue :: Dyn
	}
:: DHTObject =
	{ dhtId       :: Int
	, temperature :: Real
	, humidity    :: Real
	}
derive class dyn DHTObject
derive class iTask DHTObject

sharedObjectShare :: SimpleSDSLens [ObjectShare]
sharedObjectShare = sharedStore "simShare" []

freshOId :: Task Int
freshOId = get sharedObjectShare >>= \list.return (length list)

sdsName :: Int -> String
sdsName i = "sds" + toString i

dhtName i = "dht" + toString i

instance + String where + s t = s +++ t

applyAt :: Int (a->a) [a] -> [a]
applyAt 0 f [a:x] = [f a:x]
applyAt n f [a:x] = [a:applyAt (n-1) f x]
applyAt n f []    = []

