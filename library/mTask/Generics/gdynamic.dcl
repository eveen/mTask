definition module mTask.Generics.gdynamic

/*
	Pieter Koopman 2015
	pieter@cs.ru.nl
	Radboud University, Nijmegen, The Netherlands
	ARDSL project
*/

import StdGeneric
from Data.Maybe import :: Maybe

//:: Dyn

:: DYNAMIC :== [String] 
:: Dyn = Dyn DYNAMIC // to derive generic functions like iTask

class dyn a | toGenDynamic{|*|}, fromGenDynamic{|*|} a

generic   toGenDynamic a :: a -> [String]
generic fromGenDynamic a :: [String] -> Maybe (a, [String])

derive   toGenDynamic Int, Real, Char, Bool, String, UNIT, PAIR, EITHER, OBJECT, CONS of gcd, FIELD, RECORD of r
derive fromGenDynamic Int, Real, Char, Bool, String, UNIT, PAIR, EITHER, OBJECT, CONS of gcd, FIELD, RECORD of r

derive class dyn (),(,),(,,),(,,,),(,,,,),(,,,,,),(,,,,,,),(,,,,,,,),(,,,,,,,,),(,,,,,,,,,),(,,,,,,,,,,),(,,,,,,,,,,,),(,,,,,,,,,,,,),(,,,,,,,,,,,,,),(,,,,,,,,,,,,,,),(,,,,,,,,,,,,,,,),(,,,,,,,,,,,,,,,,),(,,,,,,,,,,,,,,,,,),(,,,,,,,,,,,,,,,,,,),(,,,,,,,,,,,,,,,,,,,),(,,,,,,,,,,,,,,,,,,,,),(,,,,,,,,,,,,,,,,,,,,,),(,,,,,,,,,,,,,,,,,,,,,,),(,,,,,,,,,,,,,,,,,,,,,,,),(,,,,,,,,,,,,,,,,,,,,,,,,),(,,,,,,,,,,,,,,,,,,,,,,,,,),(,,,,,,,,,,,,,,,,,,,,,,,,,,),(,,,,,,,,,,,,,,,,,,,,,,,,,,,),(,,,,,,,,,,,,,,,,,,,,,,,,,,,,),(,,,,,,,,,,,,,,,,,,,,,,,,,,,,,),(,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,),[],[! ],[ !],[!!]

toDyn :: a -> Dyn | dyn a
fromDyn :: Dyn -> Maybe a | dyn a
