implementation module mTask.Generics.gdynamic

/*
	Pieter Koopman 2015
	pieter@cs.ru.nl
	Radboud University, Nijmegen, The Netherlands
	ARDSL project
*/

import StdEnv, StdGeneric
import Data.Maybe

:: DYNAMIC :== [String]
:: Dyn = Dyn DYNAMIC

class dyn a | toGenDynamic{|*|}, fromGenDynamic{|*|} a
derive class dyn (),(,),(,,),(,,,),(,,,,),(,,,,,),(,,,,,,),(,,,,,,,),(,,,,,,,,),(,,,,,,,,,),(,,,,,,,,,,),(,,,,,,,,,,,),(,,,,,,,,,,,,),(,,,,,,,,,,,,,),(,,,,,,,,,,,,,,),(,,,,,,,,,,,,,,,),(,,,,,,,,,,,,,,,,),(,,,,,,,,,,,,,,,,,),(,,,,,,,,,,,,,,,,,,),(,,,,,,,,,,,,,,,,,,,),(,,,,,,,,,,,,,,,,,,,,),(,,,,,,,,,,,,,,,,,,,,,),(,,,,,,,,,,,,,,,,,,,,,,),(,,,,,,,,,,,,,,,,,,,,,,,),(,,,,,,,,,,,,,,,,,,,,,,,,),(,,,,,,,,,,,,,,,,,,,,,,,,,),(,,,,,,,,,,,,,,,,,,,,,,,,,,),(,,,,,,,,,,,,,,,,,,,,,,,,,,,),(,,,,,,,,,,,,,,,,,,,,,,,,,,,,),(,,,,,,,,,,,,,,,,,,,,,,,,,,,,,),(,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,),[],[! ],[ !],[!!]

//derive fromGenDynamic [], (,), (,,)
//derive toGenDynamic [], (,), (,,)

generic toGenDynamic a :: a -> [String]

toGenDynamic{|Int|} x = [toString x]
toGenDynamic{|Real|} x = [toString x]
toGenDynamic{|Char|} x = ["'" +++ toString x +++ "'"]
toGenDynamic{|String|} x = ["\"" +++ toString x +++ "\""]
toGenDynamic{|Bool|} x = [toString x]
toGenDynamic{|UNIT|} x = []
toGenDynamic{|PAIR|} f g (PAIR x y) = f x ++ g y
toGenDynamic{|EITHER|} f g (LEFT x) = f x
toGenDynamic{|EITHER|} f g (RIGHT y) = g y
toGenDynamic{|OBJECT|} f (OBJECT x) = f x
toGenDynamic{|CONS of gcd|} f (CONS x) = [gcd.gcd_name:f x]
toGenDynamic{|FIELD|} f (FIELD x) = f x
toGenDynamic{|RECORD of r|} f (RECORD x) = [r.grd_name:f x]

// ----------------------------

generic fromGenDynamic a :: [String] -> Maybe (a, [String])
fromGenDynamic{|UNIT|} l = Just(UNIT,l)
fromGenDynamic{|Char|} [a:x] 
	| size a == 3 && a.[0] == '\'' && a.[2] == '\''
		= Just (toChar a.[1],x)
		= Nothing
fromGenDynamic{|String|} [a:x]
	# len = size a
	| len >= 2 && a.[0] == '"' && a.[len-1] == '"'
		= Just (a%(1,len-2),x)
		= Nothing
fromGenDynamic{|String|} [] = Nothing
fromGenDynamic{|Bool|} ["True" :l] = Just (True ,l)
fromGenDynamic{|Bool|} ["False":l] = Just (False,l)
fromGenDynamic{|Bool|} l = Nothing
fromGenDynamic{|Int|} [a:x] = if (toString i == a) (Just (i,x)) Nothing where i = toInt a
fromGenDynamic{|Int|} [] = Nothing
fromGenDynamic{|Real|} [a:x] = if (toString i == a) (Just (i,x)) Nothing where i = toReal a
fromGenDynamic{|Real|} [] = Nothing
fromGenDynamic{|PAIR|} f g l =
	case f l of
		Just (x,l) = case g l of
						Just (y,l) = Just (PAIR x y, l)
						_ = Nothing
		_ = Nothing
fromGenDynamic{|EITHER|} f g l =
	case f l of
		Just (x,l) = Just (LEFT x,l)
		_ = case g l of
		 		Just (x,l) = Just (RIGHT x,l)
		 		_ = Nothing
fromGenDynamic{|OBJECT|} f l = case f l of  Just (x,l) = Just (OBJECT x,l); _ = Nothing
fromGenDynamic{|FIELD|} f l = case f l of  Just (x,l) = Just (FIELD x,l); _ = Nothing
fromGenDynamic{|RECORD of r|} f [n:l] | n == r.grd_name
    = case f l of
    	Just (x,l) = Just (RECORD x,l)
    	_ = Nothing
    = Nothing
fromGenDynamic{|CONS of gcd|} f l
	| [gcd.gcd_name] == take 1 l
		= case f (tl l) of  Just (x,l) = Just (CONS x,l); _ = Nothing
		= Nothing

// ----------------------------

toDyn :: a -> Dyn | dyn a
toDyn a = Dyn (toGenDynamic{|*|} a)

fromDyn :: Dyn -> Maybe a | dyn a
fromDyn (Dyn a) =
	case fromGenDynamic{|*|} a of 
		Just (x,[]) = Just x
		_ = Nothing 

Start1 = toLIBC (toDyn [(1,True,'a'),(2,False,'1')])
//Start = toGenDynamic{|*->*|} (\b.if b ["I"] ["O"]) [True,False,True]

toLIBC :: Dyn -> Maybe [(Int,Bool,Char)]
toLIBC l = fromDyn l

