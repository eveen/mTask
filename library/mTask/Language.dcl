definition module mTask.Language

import mTask.Language.Long
import mTask.Language.arith
import mTask.Language.basic
import mTask.Language.combinators
import mTask.Language.cond
import mTask.Language.def
import mTask.Language.mtask
import mTask.Language.iTasksSDS
import mTask.Language.pinIO
import mTask.Language.tuple
import mTask.Language.typeConversion
import mTask.Language.types

// Peripherals
import mTask.Language.LEDMatrix
import mTask.Language.dht
import mTask.Language.i2cbutton
import mTask.Language.lcd
