definition module mTask.Interpret.ByteCodeEncoding._FBC

from Data.Either import :: Either
from Data.Maybe import :: Maybe

:: FBC a = FBC ([Char] -> (Either String (Maybe a), [Char]))
