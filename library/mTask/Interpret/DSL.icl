implementation module mTask.Interpret.DSL

import StdEnv

import qualified Data.Map as DM

import Data.Error
import Data.Either
import Data.Func
import Data.Functor
import Data.Functor.Identity
import Data.GenCons
import Data.List
import Data.Maybe
import Data.Tuple
import Control.Applicative
import Control.Monad
import Control.Monad.Writer
import Control.Monad.State
import Control.Monad.Trans
import System.Time

import mTask.Language
import mTask.Interpret.Instructions
import mTask.Interpret.ByteCodeEncoding
import mTask.Interpret.UInt
import mTask.Interpret.Peripheral

from iTasks.SDS.Combinators.Common import mapReadWriteError
from iTasks.SDS.Definition import :: SDSReducer, :: Shared
from iTasks.Internal.SDS import
	instance Identifiable SDSLens,
	instance Readable SDSLens,
	instance Writeable SDSLens,
	instance Modifiable SDSLens,
	instance Registrable SDSLens

cast1 :: (m a) -> a
cast1 _ = undef

mainBC :: (Main (BCInterpret v)) BCState -> BCState
mainBC {main} st
	# ((a, st), w) = runIdentity $ runWriterT $ runStateT main st
	= {st & bcs_mainexpr=w}

instance zero BCState
where
	zero =
		{ BCState
		| bcs_mainexpr   = []
		, bcs_context    = []
		, bcs_functions  = 'DM'.newMap
		, bcs_freshlabel = (JL zero)
		, bcs_freshsds   = zero
		, bcs_sdses      = 'DM'.newMap
		, bcs_hardware   = []
		}

tell` :: [BCInstr] -> BCInterpret a
tell` w = liftT (tell w) >>| pure undef

binop :: (v a) BCInstr BCInstr BCInstr -> BCInstr | type a
binop a i l r = if (byteWidth a == one) i
	(if ((typeOf (cast1 a)) =: RealType) r l)

freshlabel :: BCInterpret JumpLabel
freshlabel = getState >>= \s=:{bcs_freshlabel=(JL i)}->
	put {s & bcs_freshlabel=JL (i+one)} >>| pure (JL i)

freshsds :: BCInterpret UInt8
freshsds = getState >>= \s=:{bcs_freshsds=i}->
	put {s & bcs_freshsds=i+one} >>| pure i

instance aio (StateT BCState (WriterT [BCInstr] Identity))
where
	readA p = p >>| tell` [BCMkTask BCReadA]
	writeA p v = p >>| v >>| tell` [BCMkTask BCWriteA]

instance arith (StateT BCState (WriterT [BCInstr] Identity))
where
	lit   t   = tell` [BCPush $ fromString $ toByteCode{|*|} t]
	(+.)  a b = a >>| b >>| tell` [binop a BCAddI BCAddL BCAddR]
	(-.)  a b = a >>| b >>| tell` [binop a BCSubI BCSubL BCSubR]
	(*.)  a b = a >>| b >>| tell` [binop a BCMultI BCMultL BCMultR]
	(/.)  a b = a >>| b >>| tell` [binop a BCDivI BCDivL BCDivR]
	(&.)  a b = a >>| b >>| tell` [BCAnd]
	(|.)  a b = a >>| b >>| tell` [BCOr]
	Not   a   = a >>|       tell` [BCNot]
	(==.) a b = a >>| b >>| tell` [binop a BCEqI  BCEqL BCEqL]
	(!=.) a b = a >>| b >>| tell` [binop a BCNeqI BCNeqL BCEqL]
	(<.)  a b = a >>| b >>| tell` [binop a BCLeI  BCLeL BCLeR]
	(>.)  a b = a >>| b >>| tell` [binop a BCGeI  BCGeL BCGeR]
	(<=.) a b = a >>| b >>| tell` [binop a BCLeqI BCLeqL BCLeqR]
	(>=.) a b = a >>| b >>| tell` [binop a BCGeqI BCGeqL BCGeqR]

instance cond (StateT BCState (WriterT [BCInstr] Identity))
where
	If c t e = freshlabel >>= \elselabel->freshlabel >>= \endiflabel->
		c >>| tell` [BCJumpF elselabel] >>|
		t >>| tell` [BCJump endiflabel,BCLabel elselabel] >>|
		e >>| tell` [BCLabel endiflabel]

instance dio v (StateT BCState (WriterT [BCInstr] Identity))
where
	readD p = p >>| tell` [BCMkTask BCReadD]
	writeD p v = p >>| v >>| tell` [BCMkTask BCWriteD]

instance delay (StateT BCState (WriterT [BCInstr] Identity))
where
	delay m = m >>| tell` [BCMkTask BCDelay]

instance rpeat (StateT BCState (WriterT [BCInstr] Identity))
where
	rpeat m = m >>| tell` [BCMkTask BCRepeat]

instance int Int  (StateT BCState (WriterT [BCInstr] Identity))
where
	int i = i

instance rtrn (StateT BCState (WriterT [BCInstr] Identity))
where
	rtrn m = m >>| tell` (map BCMkTask (bcstable (byteWidth m)))

instance sds (StateT BCState (WriterT [BCInstr] Identity))
where
	sds def = {main = freshsds
			>>= \sdsi->
				let sds = modify (\s->{s & bcs_sdses='DM'.put sdsi
							(Left $ String255 (toByteCode{|*|} t)) s.bcs_sdses})
						>>| pure (Sds (toInt sdsi))
				    (t In e) = def sds
				in e.main
		}
	getSds f = f >>= \(Sds i)-> tell` [BCMkTask (BCSdsGet (fromInt i))]
	setSds f v = f >>= \(Sds i)->v >>| tell`
		(  map BCMkTask (bcstable (byteWidth v))
		++ [BCMkTask (BCSdsSet (fromInt i))])

instance liftsds  (StateT BCState (WriterT [BCInstr] Identity))
where
	liftsds def = {main = freshsds
			>>= \sdsi->
				let sds = modify (\s->{s & bcs_sdses='DM'.put sdsi
							(Right (lens t)) s.bcs_sdses})
						>>| pure (Sds (toInt sdsi))
				    (t In e) = def sds
				in e.main
		}
	where
		lens :: ((Shared sds a) -> MTLens) | type, iTask a & RWShared sds
		lens = mapReadWriteError
			( Ok o fromString o toByteCode{|*|}
			, \w r->Just <$> iTasksDecode (toString w)
			) Nothing

import StdDebug, Text.GenPrint
derive gPrint BCInstr, UInt8, UInt16, BCTaskType, JumpLabel, String255

addToCtx :: JumpLabel UInt8 UInt8 -> BCInterpret ()
addToCtx (JL sl) fro to = modify \s->
	{s & bcs_context=s.bcs_context ++ [BCStepArg sl i\\i<-[fro..to-one]]}

clearCtx :: BCInterpret ()
clearCtx = modify \s->{s & bcs_context=[]}

/*
 * @param The lhs
 * @param width of the lhs
 * @param width of the rhs
 * @param Task type instruction constructor
 * @param Function creating the continuation function
 */
makeStep :: (BCInterpret a) UInt8 UInt8 (UInt8 JumpLabel -> BCTaskType) (JumpLabel UInt8 -> BCInterpret b)
	-> BCInterpret c | toByteWidth b
makeStep lhs lhswidth rhswidth instr contfun
	//Fetch a fresh label
	=   freshlabel
	//Fetch the context
	>>= \funlab->gets (\s->s.bcs_context)
	//Execute lhs
	>>= \ctx->lhs
	//Possibly add the context
	>>| tell` (if (ctx =: [])
			[]
			//The context is just the arguments up till now in reverse
			(  [BCArg (UInt8 i)\\i<-reverse (indexList ctx)]
			++ map BCMkTask (bcstable (UInt8 (length ctx)))
			++ [BCMkTask BCTAnd]
			)
		)
	//Increase the context
	>>| addToCtx funlab zero lhswidth
	//Lift the step function
	>>| liftFunction
			//Fresh label
			funlab
			//Width of the arguments is the width of the lhs plus the
			//stability plus the context
			(one + lhswidth + (UInt8 (length ctx)))
			//Body     label  ctx width            continuations
			(contfun funlab (UInt8 (length ctx)))
			//(toContFun funlab (UInt8 (length ctx)) cont)
			//Return width (always 1, a task pointer)
			(Just one)
	>>| modify (\s->{s & bcs_context=ctx})
	>>| tell` [BCMkTask $ instr rhswidth funlab]

instance step (StateT BCState (WriterT [BCInstr] Identity))
where
	(>>*.) lhs cont = makeStep lhs lhswidth rhswidth BCStep toContFun
	where
		lhswidth = taskValByteWidth lhs
		rhswidth = UInt8 (toByteWidth (rhsval cont undef))

		rhsval :: [Step v a b] b -> b | toByteWidth b
		rhsval _ i = i

		toContFun steplabel contextwidth
			= foldr tcf (tell` [BCPush $ fromString $ toByteCode{|*|} MT_NULL]) cont
		where
			tcf (IfStable f t)
				= If ((stability >>| tell` [BCIsStable]) &. f val)
					(t val >>| tell` [])
			tcf (IfUnstable f t)
				= If ((stability >>| tell` [BCIsUnstable]) &. f val)
					(t val >>| tell` [])
			tcf (IfNoValue t)
				= If (stability >>| tell` [BCIsNoValue])
					(t >>| tell` [])
			tcf (IfValue f t)
				= If ((stability >>| tell` [BCIsValue]) &. f val)
					(t val >>| tell` [])
			tcf (Always f) = const f

			stability = tell` [BCArg $ lhswidth + contextwidth]
			val = retrieveArgs steplabel zero lhswidth

	(>>=.) lhs rhs = makeStep lhs lhswidth rhswidth BCStepStable toContFun
	where
		lhswidth = taskValByteWidth lhs
		rhswidth = UInt8 (toByteWidth (rhsval rhs undef))

		rhsval :: (a -> v (TaskValue b)) b -> b | toByteWidth b
		rhsval _ i = i

		toContFun funlab ctxwidth = rhs (retrieveArgs funlab zero lhswidth)

	(>>~.) lhs rhs = makeStep lhs lhswidth rhswidth BCStepUnstable toContFun
	where
		lhswidth = taskValByteWidth lhs
		rhswidth = UInt8 (toByteWidth (rhsval rhs undef))

		rhsval :: (a -> v (TaskValue b)) b -> b | toByteWidth b
		rhsval _ i = i

		toContFun funlab ctxwidth = rhs (retrieveArgs funlab zero lhswidth)

	(>>|.) lhs rhs
		= lhs >>| rhs >>| tell` [BCMkTask (BCSeqStable (byteWidth rhs))]
	(>>..) lhs rhs
		= lhs >>| rhs >>| tell` [BCMkTask (BCSeqUnstable (byteWidth rhs))]

/*
 * Function to get an argument getter
 *
 * @param label of where the abstraction took place
 * @param first argument
 * @param last argument
 */
retrieveArgs :: JumpLabel UInt8 UInt8 -> BCInterpret a
retrieveArgs (JL sl) fro to = gets (\s->s.bcs_context)
	>>= \ctx->tell` [BCArg (findarg i ctx)\\i<-reverse [fro..to-one]]
where
	findarg :: UInt8 [BCInstr] -> UInt8
	findarg ntharg [] = abort "steparg: unknown"
	findarg ntharg [BCStepArg steplabel a:xs]
		| steplabel == sl && a == ntharg = zero
		= inc (findarg ntharg xs)
	findarg ntharg [x:_] = abort ("steparg: malformed: " +++ printToString x)

instance tupl (StateT BCState (WriterT [BCInstr] Identity))
where
	first t  = censorListen t >>= \(_, is)->tell` if (onlyArg is)
		(take (toInt (byteWidthFun fst t)) is)
		(is ++ [ BCPop (byteWidthFun snd t)])
	second t = censorListen t >>= \(_, is)->tell` if (onlyArg is)
		(drop (toInt (byteWidthFun fst t)) is)
		(is ++ [ BCRot (byteWidth t) (byteWidthFun snd t)
		       , BCPop (byteWidthFun fst t)])
	tupl a b = liftM2 tuple a b

onlyArg :: [BCInstr] -> Bool
onlyArg [] = True
onlyArg [BCArg _:xs] = onlyArg xs
onlyArg _ = False

instance unstable (StateT BCState (WriterT [BCInstr] Identity))
where
	unstable m = m >>| tell` (map BCMkTask (bcunstable (byteWidth m)))

instance .&&. (StateT BCState (WriterT [BCInstr] Identity))
where
	(.&&.) lhs rhs = lhs >>| rhs >>| tell` [BCMkTask BCTAnd] 

instance .||. (StateT BCState (WriterT [BCInstr] Identity))
where
	(.||.) lhs rhs = lhs >>| rhs >>| tell` [BCMkTask BCTOr]

/*
 * Creating a function:
 *
 * - fetching a fresh label
 * - give the definition a function call argument with the label
 * - from the return value we can deduce the argument width
 * - add the arguments to the context
 * - lift the function (add it to the state)
 * - execute the body
 */
instance fun () (StateT BCState (WriterT [BCInstr] Identity))
where
	fun def = {main
			=   freshlabel >>= \funlabel->
			let (g In m) = def \()->callFunction funlabel zero []
			in  liftFunction funlabel zero (g ()) Nothing
			>>| clearCtx >>| m.main
		}

instance fun
	(StateT BCState (WriterT [BCInstr] Identity) a)
	(StateT BCState (WriterT [BCInstr] Identity)) | type a
where
	fun def = {main=freshlabel >>= \funlabel->
			let (g In m) = def \a->callFunction funlabel (byteWidth a) [a]
			    argwidth = casta g undef
			in  addToCtx funlabel zero argwidth
			>>| liftFunction funlabel argwidth
				(g (retrieveArgs funlabel zero argwidth)
				) Nothing
			>>| clearCtx >>| m.main
		}
	where
		casta :: ((m a) -> b) a -> UInt8 | toByteWidth a
		casta _ a = UInt8 (toByteWidth a)

instance fun
	( StateT BCState (WriterT [BCInstr] Identity) a
	, StateT BCState (WriterT [BCInstr] Identity) b
	) (StateT BCState (WriterT [BCInstr] Identity)) | type a & type b
where
	fun def = {main=freshlabel >>= \funlabel->
			let (g In m)  = def \(a, b)->
					callFunction funlabel
						(byteWidth a + byteWidth b)
						[a, b >>| tell` []]
			    arg1width = casta g undef
			    arg2width = casta (\(a, b)->g (b, a)) undef
			in  addToCtx funlabel zero (arg1width + arg2width)
			>>| liftFunction funlabel (arg1width + arg2width)
				(g ( retrieveArgs funlabel zero arg1width
				   , retrieveArgs funlabel arg1width (arg1width+arg2width)
				)) Nothing
			>>| clearCtx >>| m.main
		}
	where
		casta :: ((m a, b) -> c) a -> UInt8 | toByteWidth a
		casta _ a = UInt8 (toByteWidth a)

instance fun
	( StateT BCState (WriterT [BCInstr] Identity) a
	, StateT BCState (WriterT [BCInstr] Identity) b
	, StateT BCState (WriterT [BCInstr] Identity) c
	) (StateT BCState (WriterT [BCInstr] Identity)) | type a & type b & type c
where
	fun def = {main=freshlabel >>= \funlabel->
			let (g In m)  = def \(a, b, c)->
					callFunction funlabel
						(byteWidth a + byteWidth b + byteWidth c)
						[a, b >>| tell` [], c >>| tell` []]
				arg1width = casta g undef
				arg2width = casta (\(a,b,c)->g (b,a,c)) undef
				arg3width = casta (\(a,b,c)->g (c,a,b)) undef
			in  addToCtx funlabel zero (arg1width + arg2width + arg3width)
			>>| liftFunction funlabel (arg1width + arg2width + arg3width)
				(g ( retrieveArgs funlabel zero arg1width
				   , retrieveArgs funlabel arg1width (arg1width+arg2width)
				   , retrieveArgs funlabel
						(arg1width+arg2width) (arg1width+arg2width+arg3width)
				)) Nothing
			>>| clearCtx >>| m.main
		}
	where
		casta :: ((m a, b, c) -> d) a -> UInt8 | toByteWidth a
		casta _ a = UInt8 (toByteWidth a)

callFunction :: JumpLabel UInt8 [BCInterpret b] -> BCInterpret c
callFunction label arity args
	=   tell` [BCPushPtrs]
	>>| sequence (reverse args)
	>>| tell` [BCJumpSR arity label]

liftFunction :: JumpLabel UInt8 (BCInterpret a) (Maybe UInt8)
	-> BCInterpret () | toByteWidth a
liftFunction label awidth m mw
# rwidth = maybe (byteWidth m) id mw
= censorListen m                >>= \(_, funinstructions)->
	getState                    >>= \s=:{bcs_functions}->
	put {s & bcs_functions='DM'.put label
		{ bcf_instructions =  [BCLabel label:funinstructions]
		                   ++ [BCReturn rwidth awidth]
		, bcf_argwidth     = awidth
		, bcf_returnwidth  = rwidth
		} s.bcs_functions}

//Write in terms of monadtrans etc
censorListen :: ((BCInterpret a) -> BCInterpret (a, [BCInstr]))
censorListen = mapStateT
	(fmap (\((a, s), w)->((a, w), s)) o censor (const []) o listen)

bcstable :: UInt8 -> [BCTaskType]
bcstable i = bcfork (toInt i) BCStableNode
	[BCStable0, BCStable1, BCStable2, BCStable3, BCStable4]

bcunstable :: UInt8 -> [BCTaskType]
bcunstable i = bcfork (toInt i) BCUnstableNode
	[BCUnstable0, BCUnstable1, BCUnstable2, BCUnstable3, BCUnstable4]

bcfork :: Int (UInt8 -> BCTaskType) [BCTaskType] -> [BCTaskType]
bcfork n node tt
	| n <= 4 = [tt !! n]
	= bcfork (n - 2) node tt ++ [node $ fromInt $ n - 2]

//Stubs
derive class gCons Map, BCFunction, Either
