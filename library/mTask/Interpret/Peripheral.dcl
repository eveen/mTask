definition module mTask.Interpret.Peripheral

from mTask.Interpret.ByteCodeEncoding import generic fromByteCode, generic toByteCode
from mTask.Interpret.UInt import :: UInt8

import mTask.Interpret.DSL
import mTask.Language

:: BCPeripheral
	= BCDHT Pin DHTtype
	| BCLEDMatrix UInt8 UInt8
	| BCI2CButton UInt8
	//= E.p: BCPeripheral p & iTask p & fromByteCode p & toByteCode p

derive fromByteCode BCPeripheral, Pin
derive toByteCode BCPeripheral, Pin

instance dht (StateT BCState (WriterT [BCInstr] Identity))
instance LEDMatrix (StateT BCState (WriterT [BCInstr] Identity))
instance i2cbutton (StateT BCState (WriterT [BCInstr] Identity))
