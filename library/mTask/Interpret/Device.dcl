definition module mTask.Interpret.Device

from iTasks.WF.Definition import :: Task
from iTasks.WF.Definition import class iTask
from iTasks.UI.Editor.Generic import generic gEditor, :: Editor
from iTasks.Internal.Generic.Visualization import generic gText, :: TextFormat
from Data.GenDefault import generic gDefault
from Data.GenEq import generic gEq
from Text.GenJSON import generic JSONEncode, generic JSONDecode, :: JSONNode
from iTasks.SDS.Definition import class RWShared, class Readable, class Writeable, class Modifiable, class Registrable, class Identifiable
from Data.Maybe import :: Maybe

from mTask.Interpret.Message import :: MTMessageFro, :: MTMessageTo
from mTask.Interpret.Device.Serial import :: TTYSettings
from mTask.Interpret.Device.TCP import :: TCPSettings
import mTask.Interpret.DSL
import mTask.Language

:: Channels :== ([MTMessageFro], [MTMessageTo], Bool)
:: MTDevice
derive class iTask MTMessageTo, MTMessageFro, MTException

class channelSync a :: a (sds () Channels Channels) -> Task () | RWShared sds

withDevice :: a (MTDevice -> Task b) -> Task b | iTask b & channelSync, iTask a

liftmTask :: (Main (BCInterpret (TaskValue u))) MTDevice -> Task u | type u
liftmTaskWithOptions :: CompileOpts (Main (BCInterpret (TaskValue u))) MTDevice -> Task u | type u

mTaskSafe :: (Task u) -> Task (Either MTException u) | type u

viewDevice :: MTDevice -> Task ()

//Handy stuff to enter a device
:: MTD = TCP TCPSettings | Serial TTYSettings
derive class iTask MTD
instance channelSync MTD
//Editor for a device with some presets
enterDevice :: Task MTD
