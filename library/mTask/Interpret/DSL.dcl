definition module mTask.Interpret.DSL

from Gast import class Gast, generic ggen, generic genShow, generic gPrint, :: GenState, class PrintOutput, :: PrintState
from StdOverloaded import class zero
from Control.Monad.State import :: StateT
from Control.Monad.Writer import :: WriterT
from Data.Functor.Identity import :: Identity
from StdOverloaded import class ^(^), class -(-)
from Data.GenCons import class gCons, generic conses, generic consNum, generic consIndex, generic consName
from Data.Map import :: Map
import iTasks.WF.Derives

import mTask.Interpret.Peripheral
import mTask.Interpret.ByteCodeEncoding
import mTask.Interpret.Instructions
import mTask.Interpret.Message
import mTask.Interpret.UInt
import mTask.Language
from iTasks.SDS.Definition import :: SDSLens

MT_NULL     :== UInt16 ((2^16) - 1)
MT_REMOVE   :== UInt8  ((2^8)  - 1)
MT_STABLE   :== UInt8 2
MT_UNSTABLE :== UInt8 1
MT_NOVALUE  :== UInt8 0

:: BCInterpret a :== StateT BCState (WriterT [BCInstr] Identity) a

tell` :: [BCInstr] -> BCInterpret a

:: BCState =
	{ bcs_mainexpr     :: [BCInstr]
	, bcs_context      :: [BCInstr]
	, bcs_functions    :: Map JumpLabel BCFunction
	, bcs_freshlabel   :: JumpLabel
	, bcs_freshsds     :: UInt8
	, bcs_sdses        :: Map UInt8 (Either String255 MTLens)
	, bcs_hardware     :: [BCPeripheral]
	}
:: MTLens :== SDSLens () String255 String255
:: BCFunction =
	{ bcf_instructions :: [BCInstr]
	, bcf_argwidth     :: UInt8
	, bcf_returnwidth  :: UInt8
	}

instance zero BCState

mainBC :: (Main (BCInterpret v)) BCState -> BCState

instance aio      (StateT BCState (WriterT [BCInstr] Identity))
instance arith    (StateT BCState (WriterT [BCInstr] Identity))
instance cond     (StateT BCState (WriterT [BCInstr] Identity))
instance delay    (StateT BCState (WriterT [BCInstr] Identity))
instance dio p    (StateT BCState (WriterT [BCInstr] Identity))
instance rpeat    (StateT BCState (WriterT [BCInstr] Identity))
instance int Int  (StateT BCState (WriterT [BCInstr] Identity))
//TODO instance lcd      (StateT BCState (WriterT [BCInstr] Identity))
instance rtrn     (StateT BCState (WriterT [BCInstr] Identity))
instance sds      (StateT BCState (WriterT [BCInstr] Identity))
instance liftsds  (StateT BCState (WriterT [BCInstr] Identity))
instance step     (StateT BCState (WriterT [BCInstr] Identity))
instance tupl     (StateT BCState (WriterT [BCInstr] Identity))
instance unstable (StateT BCState (WriterT [BCInstr] Identity))
instance .&&.     (StateT BCState (WriterT [BCInstr] Identity))
instance .||.     (StateT BCState (WriterT [BCInstr] Identity))
instance fun ()
                  (StateT BCState (WriterT [BCInstr] Identity))
instance fun (StateT BCState (WriterT [BCInstr] Identity) a)
                  (StateT BCState (WriterT [BCInstr] Identity)) | type a
instance fun (StateT BCState (WriterT [BCInstr] Identity) a, StateT BCState (WriterT [BCInstr] Identity) b)
                  (StateT BCState (WriterT [BCInstr] Identity)) | type a & type b
instance fun (StateT BCState (WriterT [BCInstr] Identity) a, StateT BCState (WriterT [BCInstr] Identity) b, StateT BCState (WriterT [BCInstr] Identity) c)
                  (StateT BCState (WriterT [BCInstr] Identity)) | type a & type b & type c
