definition module mTask.Interpret.Message

import mTask.Interpret.Instructions
import mTask.Interpret.DSL
import mTask.Interpret.Specification
import mTask.Interpret.ByteCodeEncoding
import mTask.Interpret.Compile
import mTask.Interpret.String255

:: MTMessageTo
	//* taskid returnwidth peripherals shares instructions
	= MTTTask MTTTaskData
	//* taskid (task prep for devices that have small comm buffers)
	| MTTTaskPrep UInt8
	//* taskid
	| MTTTaskDel UInt8
	//*
	| MTTSpecRequest
	//*
	| MTTShutdown
	//* taskid sdsid sds value
	| MTTSdsUpdate UInt8 UInt8 String255

:: MTTTaskData =
	{ mtttd_taskid       :: UInt8
	, mtttd_returnwidth  :: UInt8
	, mtttd_peripherals  :: [BCPeripheral]
	, mtttd_shares       :: [BCShareSpec]
	, mtttd_instructions :: [BCInstr]
	}

:: MTMessageFro
	//* taskid
	= MTFTaskAck UInt8
	//* taskid (task prep ack for devices that have small comm buffers)
	| MTFTaskPrepAck UInt8
	//* taskid
	| MTFTaskDelAck UInt8
	//* taskid stability returnvalue
	| MTFTaskReturn UInt8 (TaskValue String255)
	//* specification
	| MTFSpec MTDeviceSpec
	//* taskid sdsid sds value
	| MTFSdsUpdate UInt8 UInt8 String255
	//* exception taskid type
	| MTFException MTException
	//* debug message
	| MTFDebug String255

:: MTException
	= MTEOutOfMemory UInt8
	| MTEHeapUnderflow UInt8
	| MTEStackOverflow UInt8
	| MTESdsUnknown UInt8 UInt8
	| MTEFPException UInt8
	| MTESyncException String255
	| MTERTSError
	| MTEUnexpectedDisconnect

derive toByteCode MTMessageFro, MTMessageTo, MTException
derive fromByteCode MTMessageFro, MTMessageTo, MTException

instance toString MTException
