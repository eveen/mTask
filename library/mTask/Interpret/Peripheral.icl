implementation module mTask.Interpret.Peripheral

import StdEnv

import Data.Func
import Data.List
import Data.Functor
import Data.Functor.Identity
import Data.Monoid
import Control.Monad
import Control.Monad.State
import Control.Monad.Writer
import Control.Applicative

import mTask.Interpret.DSL
import mTask.Interpret.UInt
import mTask.Language

derive class gCons BCPeripheral, Pin

derive toByteCode BCPeripheral, DHTtype

derive fromByteCode BCPeripheral, DHTtype
fromByteCode{|Pin|} = topin <$> fromByteCode{|*|}

topin (UInt8 i)
	| i bitand 1 > 0 = DigitalPin $ conses{|*|} !! (i >> 1)
	                 = AnalogPin $ conses{|*|} !! (i >> 1)

toByteCode{|Pin|} (DigitalPin a) = {toChar $ (consIndex{|*|} a << 1) bitor 1}
toByteCode{|Pin|} (AnalogPin a) = {toChar $ consIndex{|*|} a << 1}

instance dht (StateT BCState (WriterT [BCInstr] Identity)) where
	DHT p type def = {main
		=   gets nextDHT
		<*  modify (\s->{s & bcs_hardware=s.bcs_hardware ++ [BCDHT (pin p) type]})
		>>= unmain o def o pure
		}
	temperature dht = dht >>= \(Dht i)->tell` [BCMkTask $ BCDHTTemp $ fromInt i]
	humidity dht = dht >>= \(Dht i)->tell` [BCMkTask $ BCDHTHumid $ fromInt i]

nextDHT :: BCState -> DHT
nextDHT st=:{bcs_hardware=p} = Dht $ fromInt $ length [()\\(BCDHT _ _)<-p]

instance LEDMatrix (StateT BCState (WriterT [BCInstr] Identity)) where
	ledmatrix data clock def = {main
		=   gets nextLEDMatrix
		<*  modify (\s->{s & bcs_hardware=s.bcs_hardware ++ [BCLEDMatrix (UInt8 (consIndex{|*|} data)) (UInt8 (consIndex{|*|} clock))]})
		>>= unmain o def o pure
		}
	LMDot m x y s = m >>= \(LEDMatrix i)->x >>| y >>| s >>| tell` [BCMkTask $ BCLEDMatrixDot $ fromInt i]
	LMIntensity m x = m >>= \(LEDMatrix i)->x >>| tell` [BCMkTask $ BCLEDMatrixIntensity $ fromInt i]
	LMClear m = m >>= \(LEDMatrix i)->tell` [BCMkTask $ BCLEDMatrixClear $ fromInt i]
	LMDisplay m = m >>= \(LEDMatrix i)->tell` [BCMkTask $ BCLEDMatrixDisplay $ fromInt i]

nextLEDMatrix :: BCState -> LEDMatrix
nextLEDMatrix st=:{bcs_hardware=p} = LEDMatrix $ fromInt $ length [()\\(BCLEDMatrix _ _)<-p]

instance i2cbutton (StateT BCState (WriterT [BCInstr] Identity)) where
	i2cbutton addr def = {main
		=   gets nextI2CButton
		<*  modify (\s->{s & bcs_hardware=s.bcs_hardware ++ [BCI2CButton (UInt8 addr)]})
		>>= unmain o def o pure
		}
	AButton m = m >>= \(I2CButton i)->tell` [BCMkTask (BCAButton (fromInt i))]
	BButton m = m >>= \(I2CButton i)->tell` [BCMkTask (BCBButton (fromInt i))]

nextI2CButton :: BCState -> I2CButton
nextI2CButton st=:{bcs_hardware=p} = I2CButton (fromInt $ length [()\\(BCI2CButton _)<-p])
