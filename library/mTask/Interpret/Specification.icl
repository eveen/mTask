implementation module mTask.Interpret.Specification

import Data.Func
import Data.GenCons
import Data.Functor
import Control.Applicative
import Control.GenBimap
import Control.Monad
import StdEnv

import mTask.Interpret.ByteCodeEncoding
import mTask.Interpret.DSL

derive class iTask MTDeviceSpec
derive class gCons MTDeviceSpec

toByteCode{|MTDeviceSpec|} m = (toString $ toChar $
			(btoi m.haveLCD)
		) +++ toByteCode{|*|} m.aPins
		  +++ toByteCode{|*|} m.dPins
		  +++ toByteCode{|*|} m.memory

btoi b = if b 1 0
	
fromByteCode{|MTDeviceSpec|}
	= top
	>>= \pphs->fromByteCode{|*|}
	>>= \aPins->fromByteCode{|*|}
	>>= \dPins->fromByteCode{|*|} >>= \mem-> pure
		{ MTDeviceSpec
		| haveLCD     = (toInt pphs bitand 1) > 0
		, aPins       = aPins
		, dPins       = dPins
		, memory      = mem
		}
