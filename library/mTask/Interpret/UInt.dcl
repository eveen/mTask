definition module mTask.Interpret.UInt

from StdOverloaded import class zero, class one, class -, class +, class *, class /, class rem, class <, class toInt, class ~, class fromInt
from iTasks.WF.Definition import class iTask
from iTasks.UI.Editor.Generic import generic gEditor, :: Editor
from iTasks.Internal.Generic.Visualization import generic gText, :: TextFormat
from Data.GenDefault import generic gDefault
from Data.GenEq import generic gEq
from Text.GenJSON import generic JSONEncode, generic JSONDecode, :: JSONNode
from Data.Maybe import :: Maybe

import Data.GenCons

:: UInt16 =: UInt16 Int
UINT16_MAX :== UInt16 0xffff
UINT16_MIN :== UInt16 0
:: UInt8 =: UInt8 Int
UINT8_MAX :== UInt8 0xff
UINT8_MIN :== UInt8 0

instance ~ UInt8, UInt16
instance < UInt8, UInt16
instance + UInt8, UInt16
instance - UInt8, UInt16
instance * UInt8, UInt16
instance / UInt8, UInt16
instance rem UInt8, UInt16
instance == UInt8, UInt16
instance one UInt8, UInt16
instance zero UInt8, UInt16
instance toInt UInt8, UInt16
instance fromInt UInt8, UInt16
instance toString UInt8, UInt16

class toUInt16 a :: a -> UInt16
instance toUInt16 Char

derive class iTask UInt8, UInt16
derive class gCons UInt8, UInt16
