implementation module mTask.Interpret.UInt

import StdEnv
import Data.Func
import iTasks => qualified return
import Data.GenCons
import Control.GenBimap

from StdInt import instance toInt Char

instance ~ UInt8 where (~) (UInt8 a) = UInt8 (~a)
instance ~ UInt16 where (~) (UInt16 a) = UInt16 (~a)
instance < UInt8 where (<) a b = on (<) toInt a b
instance < UInt16 where (<) a b = on (<) toInt a b
instance + UInt8 where (+) a b = norm (UInt8 (on (+) toInt a b))
instance + UInt16 where (+) a b = norm (UInt16 (on (+) toInt a b))
instance - UInt8 where (-) a b = norm (UInt8 (on (-) toInt a b))
instance - UInt16 where (-) a b = norm (UInt16 (on (-) toInt a b))
instance * UInt8 where (*) a b = norm (UInt8 (on (*) toInt a b))
instance * UInt16 where (*) a b = norm (UInt16 (on (*) toInt a b))
instance / UInt8 where (/) a b = norm (UInt8 (on (/) toInt a b))
instance / UInt16 where (/) a b = norm (UInt16 (on (/) toInt a b))
instance rem UInt8 where (rem) a b = norm (UInt8 (on (rem) toInt a b))
instance rem UInt16 where (rem) a b = norm (UInt16 (on (rem) toInt a b))
instance == UInt8 where (==) a b = on (==) toInt a b
instance == UInt16 where (==) a b = on (==) toInt a b
instance one UInt8 where one = UInt8 one
instance one UInt16 where one = UInt16 one
instance zero UInt8 where zero = UInt8 zero
instance zero UInt16 where zero = UInt16 zero
instance toInt UInt8 where toInt (UInt8 i) = i
instance toInt UInt16 where toInt (UInt16 i) = i
instance fromInt UInt8 where fromInt i = norm (UInt8 i)
instance fromInt UInt16 where fromInt i = norm (UInt16 i)
instance toString UInt8 where toString (UInt8 i) = toString i
instance toString UInt16 where toString (UInt16 i) = toString i

class norm a :: a -> a

instance norm UInt8 where
	norm j=:(UInt8 i) = if (i > 255) zero (if (i < 0) zero j)
instance norm UInt16 where
	norm j=:(UInt16 i) = if (i > 65535) zero (if (i < 0) zero j)

class toUInt16 a :: a -> UInt16

instance toUInt16 Char where
	toUInt16 c = UInt16 (toInt c)

derive class iTask UInt8, UInt16
derive class gCons UInt8, UInt16
