implementation module mTask.Interpret.String255

import StdOverloaded, StdArray
import Control.GenBimap
import Data.Func
import Data.GenCons
import Text
import iTasks

instance +++ String255 where (+++) s t = fromString (on (+++) toString s t)
instance % String255 where (%) s r = fromString (toString s % r)
instance == String255 where (==) s t = on (==) toString s t

instance fromString String255 where fromString s = String255 (s % (0, 255))
instance toString String255 where toString (String255 s) = s

instance Text String255
where
	textSize s = textSize (toString s)
	concat a = fromString $ concat $ map toString a
	split a b = map fromString $ on split toString a b
	join a b = fromString $ join (toString a) (map toString b)
	indexOf a b = on indexOf toString a b
	lastIndexOf a b = on lastIndexOf toString a b
	indexOfAfter i a b = on (indexOfAfter i) toString a b
	startsWith a b = on startsWith toString a b
	endsWith a b = on endsWith toString a b
	subString a b c = fromString $ subString a b (toString c)
	replaceSubString a b c = fromString $ on (replaceSubString (toString a)) toString b c
	trim a = fromString $ trim $ toString a
	ltrim a = fromString $ ltrim $ toString a
	rtrim a = fromString $ rtrim $ toString a
	lpad a b c = fromString $ lpad (toString a) b c
	rpad a b c = fromString $ rpad (toString a) b c
	toLowerCase a = fromString $ toLowerCase $ toString a
	toUpperCase a = fromString $ toUpperCase $ toString a
	upperCaseFirst a = fromString $ upperCaseFirst $ toString a
	dropChars a b = fromString $ dropChars a $ toString b

derive class iTask String255
derive class gCons String255

safePrint :: String255 -> String
safePrint s = join ", " [toString $ toInt c\\c<-:toString s]
