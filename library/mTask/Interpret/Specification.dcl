definition module mTask.Interpret.Specification

from iTasks.WF.Definition import class iTask, :: Stability, :: TaskValue(..), :: Task
from iTasks.UI.Editor.Generic import generic gEditor, :: Editor
from iTasks.Internal.Generic.Visualization import generic gText, :: TextFormat
from Data.GenDefault import generic gDefault
from Data.GenEq import generic gEq
from Text.GenJSON import generic JSONEncode, generic JSONDecode, :: JSONNode
from Data.Maybe import :: Maybe
from Data.GenCons import class gCons, generic conses, generic consNum, generic consIndex, generic consName

from mTask.Interpret.ByteCodeEncoding import generic toByteCode, generic fromByteCode, :: FBC
from mTask.Interpret.DSL import :: UInt8, :: UInt16

derive class iTask MTDeviceSpec
derive class gCons MTDeviceSpec

:: MTDeviceSpec =
	{ haveLCD :: Bool
	, aPins   :: UInt8
	, dPins   :: UInt8
	, memory  :: UInt16
	}

derive toByteCode MTDeviceSpec
derive fromByteCode MTDeviceSpec
