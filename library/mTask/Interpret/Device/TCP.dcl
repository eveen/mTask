definition module mTask.Interpret.Device.TCP

from mTask.Interpret.Device import class channelSync
from iTasks.WF.Definition import class iTask
from iTasks.UI.Editor.Generic import generic gEditor, :: Editor
from iTasks.Internal.Generic.Visualization import generic gText, :: TextFormat
from Data.GenDefault import generic gDefault
from Data.GenEq import generic gEq
from Text.GenPrint import generic gPrint, class PrintOutput, :: PrintState
from Text.GenJSON import generic JSONEncode, generic JSONDecode, :: JSONNode
from Data.Maybe import :: Maybe

:: TCPSettings = {host :: String, port :: Int}
derive class iTask TCPSettings

instance channelSync TCPSettings
