implementation module mTask.Interpret.Device.TCP

import StdArray, StdMisc
import iTasks
import Text
import Data.Either
import mTask.Interpret.Device
import mTask.Interpret.ByteCodeEncoding
import mTask.Interpret.Message

import StdDebug


derive class iTask TCPSettings

getTCPSettings :: Task TCPSettings
getTCPSettings = enterInformation [] <<@ Title "Settings"

instance channelSync TCPSettings where
	channelSync {TCPSettings|host,port} channels = tcpconnect host port (Just 500) channels
		{ ConnectionHandlers
		| onConnect     = \cid _->trace_n "onConnect" (onShareChange [])
		, onData        = onData
		, onShareChange = onShareChange
		, onDisconnect  = \_ _->(Ok [], Just ([], [], True))
		, onDestroy     = \_->(Ok [], [toByteCode{|*|} MTTShutdown])
		} <<@ NoUserInterface @! ()
	where
		onConnect :: ConnectionId String Channels -> (MaybeErrorString [Char], Maybe Channels, [String], Bool)
		onConnect cid acc ch = onShareChange [] ch

		onData :: String [Char] Channels -> (MaybeErrorString [Char], Maybe Channels, [String], Bool)
		onData newdata acc (msgs,send,ss)
			# acc = acc ++ fromString newdata
			= case runFBC parseSepMessages acc of
				(Left error, _) = (Error error, Nothing, [], True)
				(Right (Just []), newacc)
					= (Ok newacc, Nothing, [], ss)
				(Right (Just newmsgs), newacc)
					= (Ok newacc, Just (msgs++newmsgs, send, ss), [], ss)
				(Right Nothing, newacc) = abort "Shouldn't happen"
		
		onShareChange :: [Char] Channels -> (MaybeErrorString [Char], Maybe Channels, [String], Bool)
		// Nothing to send
		onShareChange acc (msgs, [], ss) = (Ok acc, Nothing, [], ss)
		// Something to send
		onShareChange acc (msgs, send, ss) = (Ok acc, Just (msgs,[],ss), map toByteCode{|*|} send, ss)
