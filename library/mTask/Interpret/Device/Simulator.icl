implementation module mTask.Interpret.Device.Simulator

import iTasks

import Data.Tuple
import Data.List

from StdFunctions import flip, o
from StdTuple import uncurry

from mTask.Interpret.Compile import :: BCShareSpec
from mTask.Interpret.Instructions import :: BCInstr
from iTasks.Extensions.DateTime import waitForTimer
import mTask.Interpret
import mTask.Interpret.ByteCodeEncoding
import mTask.Interpret.Device
import mTask.Interpret.Device.Simulator.Energy
import mTask.Interpret.Device.Simulator.Interface
import mTask.Interpret.Device.Simulator.Interpret
import mTask.Interpret.Device.Simulator.Rewrite
import mTask.Interpret.Device.Simulator.Stack
import mTask.Interpret.Device.Simulator.State
import mTask.Interpret.Device.Simulator.Symbols
import mTask.Interpret.Instructions
import mTask.Interpret.Message
import mTask.Interpret.UInt
import mTask.Interpret.Device.Simulator.Mem
import StdOverloaded

import StdDebug
from StdMisc import abort

instance zero SimSettings
where
	zero = { SimSettings
		| sim_spec = { MTDeviceSpec
			| haveLCD = True
			, aPins = zero
			, dPins = zero
			, memory = UInt16 50
			}
		, humidities = []
		, temps = []
		, d_vals = []
		, a_vals = []
		, timeStep = 1000 // 1 second
		}

derive class iTask SimSettings

to16Int :: !Char !Char -> UInt16
to16Int h l = UInt16 (((toInt h) << 16) + toInt l)

to32Int :: !UInt16 !UInt16 -> Int
to32Int h l = ((toInt h) << 16) + toInt l

loop :: !SimSettings !(SimChannels sds) !(Shared sds` BCSimState) -> Task BCSimState | RWShared sds & RWShared sds`
loop settings=:{timeStep} channels stateShare = forever (
	    waitForTimer 1 <<@ NoUserInterface
	>>| get stateShare
	>>= \s -> return {s & sp = zero, stack = map (const zero) s.stack}
	>>= trace_n "Handling Messages" handleMessages settings channels
	// Garbage collect
	>>= \s -> return (memGC s)
	>>= memTaskHead
	>>= trace_n "Handling Tasks:" handleTasks stateShare channels
	// Time and Energy
	>>= \s -> return {s & time = s.time + timeStep}
	>>= \s -> 
	let ns = addCurrentEnergy timeStep s in
	    set ns stateShare
	)

memTaskHead :: !BCSimState -> Task BCSimState
memTaskHead s=:{tasks=[]} = return {s & current_task = BCMT_NULL}
memTaskHead s = return {s & current_task = zero}

memTaskNext :: !BCSimState -> Task BCSimState
memTaskNext s=:{tasks, current_task}
| trace_n "Simulator.memTaskNext" toInt (inc current_task) >= length tasks = return {s & current_task = BCMT_NULL}
| otherwise = return {s & current_task = inc current_task}

handleTasks :: !(Shared sds` BCSimState) !(SimChannels sds) !BCSimState -> Task BCSimState | RWShared sds & RWShared sds`
// TODO: Replace with BCMT_NULL
handleTasks sShare _ s=:{current_task = UInt16 65535} = return s
handleTasks sShare chShare s=:{current_task, tasks} =
	    handleTask sShare chShare (tasks!!(toInt current_task)) s
	>>= memTaskNext
where
// TODO type
//	handleTask :: !(Shared sds BCSimState) !(SimChannels sds`) !MTTask !BCSimState -> Task BCSimState | RWShared sds & RWShared sds`
	// TODO: BCMT_NULL
	handleTask sShare _ t=:{tree = UInt16 65535} s
	#! s = trace "Handling Task (NULL) " trace_n t.MTTask.id {s & sp = zero, pc = zero} >>> initStackMain
	#! t = s.tasks!!(toInt current_task)
	= interpret t.bc s >>= \s=:{stack} = updateCurrentTask {t & tree = stack!$0} s >>> return
	// TODO: BCMT_REMOVE
	handleTask sShare _ t=:{tree = UInt16 255} s = abort "Huh, this should've been garbage collected\n"
	handleTask sShare chShare t=:{tree, bc, sds} s =
		    trace "Handling Task (NotNULL) " trace_n t.MTTask.id rewrite sShare tree bc sds s
		>>= \(sds, s=:{stack}) =
		let t` = {t & sds = sds} in
		let t`` = if ((stack !$ 0) == BCMT_STABLE) {t` & tree = BCMT_REMOVE} t` in
		    taskComplete (take (t``.returnwidth + 1) stack) t`` chShare s
		>>= return o updateCurrentTask t``

	updateCurrentTask :: !MTTask !BCSimState -> BCSimState
	updateCurrentTask t s=:{current_task, tasks} = trace_n "Simulator.updateCurrentTask" {s & tasks = updateAt (toInt current_task) t tasks}

/**
 * Folds over the messages, parses and then performs actions based on the Message
 *
 * @param The Simulator Settings, includes the memory and sensors/actuators
 * @param The Shared Channels with which to communicate
 * @param The State of the Simulator
 *
 * @result And updated state
 */
handleMessages :: !SimSettings !(SimChannels sds) !BCSimState -> Task BCSimState | RWShared sds
handleMessages settings chShare state =
	    trace_n "Handling Messages" get chShare
	>>= \ch=:(outq, inq, stop) -> foldT evalToMessage (settings, ch, state) inq
	>>= \(_, ch, s) -> set ch chShare
	>>| return s
	where
		foldT :: !((SimSettings, Channels, BCSimState) -> MTMessageTo -> Task (SimSettings, Channels, BCSimState)) !(SimSettings, Channels, BCSimState) ![MTMessageTo] -> Task (SimSettings, Channels, BCSimState)
		foldT _ (set, (outq, _, stop), st) [] = return (set, (outq, [], stop), st)
		foldT f (set, (outq, _, stop), st) inq=:[x:xs] = f (set, (outq, inq, stop), st) x >>= flip (foldT f) xs

evalToMessage :: !(SimSettings, Channels, BCSimState) !MTMessageTo -> Task (SimSettings, Channels, BCSimState)
//TODO: MTTTask now also has a list of perpherals
evalToMessage (set, (outq, inq, stop), state=:{tasks}) (MTTTask d) = trace_n "Recieved Task" buildTask d.mtttd_taskid d.mtttd_returnwidth d.mtttd_shares d.mtttd_instructions >>= \t = return (set, ([MTFTaskAck d.mtttd_taskid : outq], inq, stop), {state & tasks = [t : tasks]})
evalToMessage (set, (outq, inq, stop), state) (MTTTaskPrep id) = trace_n "Handling MTTTaskPrep" return (set, ([MTFTaskPrepAck id : outq], inq, stop), state)
evalToMessage state (MTTTaskDel tid) = trace_n "Handling MTTaskDel" return state
evalToMessage (set, (outq, inq, stop), st) MTTSpecRequest = trace_n "Handling MTSpecRequest" return (set, (outq ++ [MTFSpec set.sim_spec], inq, stop), st) // Add MTFSpec message to end of queue
evalToMessage state MTTShutdown = trace_n "Handling MTShutdown" return state
//* taskid sdsid value
// MTTSdsUpdate UInt8 UInt8 String255
evalToMessage state (MTTSdsUpdate tid sid val) = trace_n "Handling MTTSdsUpdate" return state

buildTask :: !UInt8 !UInt8 ![BCShareSpec] ![BCInstr] -> Task MTTask
buildTask id width shares instr = enterMultipleChoice [ChooseFromCheckGroup snd] (zip2 [0 ..] instr) <<@ Title "Select breakpoints" >>= \bps -> return
	{MTTask |
	 id = id
	,stability = BCMT_NOVALUE
	,returnwidth = toInt width
	,returnvalue = repeatn (toInt width) zero
	,bc = toInstr 0 instr (map fst bps)
	,sds = shares
	,tree = BCMT_NULL
	}
where
	toInstr :: Int [BCInstr] [Int] -> [(Bool, BCInstr)]
	toInstr _ is [] = map (\i -> (False, i)) is
	toInstr n [i:is] [b:bs]
	| n == b = [(True, i) : toInstr (inc n) is bs]
	| otherwise = [(False, i) : toInstr (inc n) is [b:bs]]
