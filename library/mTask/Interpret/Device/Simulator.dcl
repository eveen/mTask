definition module mTask.Interpret.Device.Simulator

import iTasks

from mTask.Interpret.Compile import :: BCShareSpec
import mTask.Interpret.Device
import mTask.Interpret.Device.Simulator.Energy
import mTask.Interpret.Device.Simulator.Interpret
import mTask.Interpret.Device.Simulator.Rewrite
import mTask.Interpret.Device.Simulator.Stack
import mTask.Interpret.Device.Simulator.State
import mTask.Interpret.Instructions
import mTask.Interpret.UInt

:: SimChannels sds :== sds () Channels Channels

/**
 * Settings that are passed to the Interpreter
 */
:: SimSettings =
	{ sim_spec :: !MTDeviceSpec                //* The Device Spec
	, humidities :: ![(UInt8, UInt16)]         //* Environment humidity
	, temps :: ![(UInt8, UInt16)]              //* Environment humidity
	, d_vals :: ![(UInt8, DigitalPeripheral)]  //* List of Digital pins, with their values
	, a_vals :: ![(UInt8, AnaloguePeripheral)] //* List of Analoge pins, with their values
	, timeStep :: !Int                         //* Amount of time that elapses every loop
	}

instance zero SimSettings

derive class iTask SimSettings

/**
 * The main loop of the simulator as in client.c
 */
loop :: !SimSettings !(SimChannels sds) !(Shared sds` BCSimState) -> Task BCSimState | RWShared sds & RWShared sds`
