definition module mTask.Interpret.Device.Serial

from mTask.Interpret.Device import class channelSync
from StdOverloaded import class zero
from TTY import getTTYDevices, :: TTYSettings(..), :: Parity(..), :: BaudRate(..), :: ByteSize(..), instance zero TTYSettings
import iTasksTTY

instance channelSync TTYSettings
