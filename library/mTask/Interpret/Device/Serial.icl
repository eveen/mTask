implementation module mTask.Interpret.Device.Serial

import StdEnv
import mTask.Interpret
import iTasks
import iTasksTTY
import System.Time
import Data.Either
import Data.Maybe
import mTask.Interpret.ByteCodeEncoding
import mTask.Interpret.Message

instance channelSync TTYSettings where
	 channelSync settings channels
		= syncSerialChannel
			{tv_sec=0,tv_nsec=100*1000000}
			settings
			toByteCode{|*|}
			(\s->case runFBC parseSepMessages [c\\c<-:s] of
				(Left e, s) = (Left e, toString s)
				(Right ma, s) = (Right (maybe [] id ma), toString s)
			)
			channels
