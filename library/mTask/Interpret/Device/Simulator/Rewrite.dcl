definition module mTask.Interpret.Device.Simulator.Rewrite

from mTask.Interpret.Device.Simulator.Task import :: MTTaskTree
from mTask.Interpret.Device.Simulator.State import :: SimState, :: BCSimState
from mTask.Interpret.UInt import :: UInt16
from mTask.Interpret.Compile import :: BCShareSpec
from mTask.Interpret.Instructions import :: BCInstr

import iTasks

/**
 * @param Pointer to the TaskTree that requires rewritting
 * @param The Instructions, including the main function
 * @param The sds
 * @param The SimState
 *
 * @result The new sds and SimState
 */
rewrite :: !(Shared sds BCSimState) !UInt16 ![(Bool, BCInstr)] ![BCShareSpec] !BCSimState -> Task (![BCShareSpec], !BCSimState) | RWShared sds
