definition module mTask.Interpret.Device.Simulator.Interpret

from mTask.Interpret.Device.Simulator.State import :: SimState, :: BCSimState
from mTask.Interpret.Device.Simulator.Task import :: MTTaskTree
from mTask.Interpret.UInt import :: UInt16, :: UInt8
from mTask.Interpret.Instructions import :: BCInstr

from StdOverloaded import class toInt

import iTasks

/**
 * @param The list of Instructions, including whether they are breakpoints
 *
 * Perform the Interpretation of the ByteCode as in interpret.c
 */
interpret :: ![(Bool, BCInstr)] !BCSimState -> Task BCSimState

/**
 * 1 If True
 * 0 Otherwise
 */
instance toInt Bool

/**
 * Convert two UInt8 to a single UInt16
 */
from16 :: !UInt8 !UInt8 -> UInt16

/**
 * Convert a UInt16 to two Chars
 */
to16 :: !UInt16 -> (!Char, !Char)
