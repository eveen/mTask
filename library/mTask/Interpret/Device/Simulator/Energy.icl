implementation module mTask.Interpret.Device.Simulator.Energy

import Data.Maybe
import mTask.Interpret.UInt
import mTask.Interpret.Device.Simulator.State

from StdList import sum, map
from StdFunctions import o
from Data.Func import $
import StdInt
import StdReal
from Data.Map import toList, toAscList, foldrWithKey

import iTasks
import iTasks.UI.Editor.Common

from StdMisc import abort

/**
 * After struggling for very long with compiler bugs and overloading errors
 * I made the decision to go for this (less elegant approach).
 * Ideally a state with existential quantifier should be implemented.
 * Old idea:
 * Peripheral a = E.s: { state :: s, getEnergyOT :: s -> Energy, setVal :: s a -> (s, Energy), getVal :: s -> (a, s, Energy)} | iTask s & iTask a
 */

derive class iTask AnaloguePeripheral, DigitalPeripheral

instance Peripheral DigitalPeripheral Bool
where
	setVal :: DigitalPeripheral Bool -> (Energy, DigitalPeripheral)
	setVal (Led s) v = (if (s == v) 0.0 1.0, Led v)
	setVal (Button _) _ = abort "Cannot set value of Button"

	getVal :: DigitalPeripheral -> (Energy, Bool, DigitalPeripheral)
	getVal (Led s) = (0.0, s, Led s)
	getVal (Button s) = (0.0, s, Button s)

instance Peripheral AnaloguePeripheral UInt8
where
	setVal :: AnaloguePeripheral UInt8 -> (Energy, AnaloguePeripheral)
	setVal (Humid _) _ = abort "Cannot set value on humidity sensor"

	getVal :: AnaloguePeripheral -> (Energy, UInt8, AnaloguePeripheral)
	getVal (Humid i) = (0.5, i, Humid i)

instance EnergyOT DigitalPeripheral
where
	getEnergyOT :: DigitalPeripheral -> Energy
	getEnergyOT (Led True) = 1.0
	getEnergyOT (Led False) = 0.0
	getEnergyOT (Button _) = 0.0

instance EnergyOT AnaloguePeripheral
where
	getEnergyOT :: AnaloguePeripheral -> Energy
	getEnergyOT (Humid _) = 1.0

getTotalEnergyOT :: !BCSimState -> Energy
getTotalEnergyOT {a_pins, d_pins} = (sum o map getEnergyOT $ dp) + (sum o map getEnergyOT $ ap)
where
	ap = map snd o toList $ a_pins
	dp = map snd o toList $ d_pins

addCurrentEnergy :: !Int !BCSimState -> BCSimState
addCurrentEnergy time ss=:{consumed_energy}
#! eot = (fromInt time) * (getTotalEnergyOT ss)
= {ss & consumed_energy = consumed_energy + eot}
