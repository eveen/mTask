implementation module mTask.Interpret.Device.Simulator.Heap

from StdDebug import trace_n
from StdMisc import abort
from StdOverloaded import class zero
import mTask.Interpret.UInt
import StdList
import StdTuple
import Data.Maybe

import mTask.Interpret.Device.Simulator.Task

newHeap :: !Int -> Heap a
newHeap n = repeatn n Nothing

alloc :: !a !(Heap a) -> (!Offset, !Heap a)
alloc e h = alloc` zero e h
where
	alloc` :: !Offset !a !(Heap a) -> (!Offset, !Heap a)
	alloc` _ _ [] = abort "Heap.alloc.alloc`: Heap full"
	alloc` i e [Just x : xs]
	# (index, xs) = alloc` (i + (fromInt 1)) e xs
	= (index, [Just x : xs])
	alloc` i e [Nothing : xs] = (i, [Just e : xs])

free :: !Offset !(Heap a) -> Heap a
free (UInt16 0) h=:[Nothing : _] = trace_n "Heap.free: Double free" h
free (UInt16 0) [Just _ : xs] = [Nothing : xs]
free n [x : xs] = [x : free (n - (fromInt 1)) xs]

deref :: !Offset !(Heap a) -> a
deref (UInt16 n) h
# e = h!!n
= fromMaybe (abort "Heap.deref: Segmentation fault") e

update :: !a !Offset !(Heap a) -> Heap a
update _ (UInt16 0) [Nothing : _] = abort "Heap.update: Segmentation fault"
update e (UInt16 0) [_ : xs] = [Just e : xs]
update e n [x : xs] = [x : update e (n - (fromInt 1)) xs]
