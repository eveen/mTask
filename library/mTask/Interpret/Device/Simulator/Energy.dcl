definition module mTask.Interpret.Device.Simulator.Energy

import mTask.Interpret.UInt
import mTask.Interpret.Device.Simulator.State

import iTasks

// To allow the user to pick the type of peripherals
:: DigitalPeripheral = Led LedState | Button ButtonState
:: AnaloguePeripheral = Humid HumidState

:: Energy :== Real

derive class iTask AnaloguePeripheral, DigitalPeripheral

:: LedState :== Bool
:: ButtonState :== Bool
:: HumidState :== UInt8

class Peripheral a r where
	setVal :: a r -> (Energy, a)
	getVal :: a -> (Energy, r, a)

class EnergyOT a where
	getEnergyOT :: a -> Energy

instance Peripheral DigitalPeripheral Bool
instance Peripheral AnaloguePeripheral UInt8

instance EnergyOT DigitalPeripheral
instance EnergyOT AnaloguePeripheral

getTotalEnergyOT :: !BCSimState -> Energy

addCurrentEnergy :: !Int !BCSimState -> BCSimState
