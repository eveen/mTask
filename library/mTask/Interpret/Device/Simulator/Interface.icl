implementation module mTask.Interpret.Device.Simulator.Interface

import StdInt
import System.Time
import qualified Data.Map as M
import StdMisc
import Data.Maybe

import mTask.Interpret.Device.Simulator.Energy
import mTask.Interpret.Device.Simulator.State
import mTask.Interpret.UInt

import iTasks

getMillies :: !*World -> (!Int, !*World)
getMillies w
#! ({tv_sec, tv_nsec}, w) = nsTime w
= (tv_sec * 1000 + tv_nsec / 1000, w)

readPin :: UInt8 (Map UInt8 a) BCSimState -> (r, Map UInt8 a, BCSimState) | Peripheral a r
readPin i pins s=:{consumed_energy}
#! per = fromMaybe (abort ("Interface.readPin: Could not get pin with id: " +++ toString i)) ('M'.get i pins)
#! (e, val, per) = getVal per
#! s = {s & consumed_energy = consumed_energy + e}
#! pins = 'M'.put i per pins
= (val, pins, s)

writePin :: UInt8 (Map UInt8 a) r BCSimState -> (Map UInt8 a, BCSimState) | Peripheral a r
writePin i pins val s=:{consumed_energy}
#! per = fromMaybe (abort ("Interface.writePin: Could not get pin with id: " +++ toString i)) ('M'.get i pins)
#! (e, per) = setVal per val
#! s = {s & consumed_energy = consumed_energy + e}
#! pins = 'M'.put i per pins
= (pins, s)

readDPin :: !UInt8 !BCSimState -> (!Bool, !BCSimState)
readDPin i s=:{d_pins}
#! (val, d_pins, s) = readPin i d_pins s
#! s = {s & d_pins = d_pins}
= (val, s)

writeDPin :: !UInt8 !Bool !BCSimState -> BCSimState
writeDPin i b s=:{d_pins}
#! (d_pins, s) = writePin i d_pins b s
#! s = {s & d_pins = d_pins}
= s

readAPin :: !UInt8 !BCSimState -> (!UInt8, !BCSimState)
readAPin i s=:{a_pins}
#! (val, a_pins, s) = readPin i a_pins s
#! s = {s & a_pins = a_pins}
= (val, s)

writeAPin :: !UInt8 !UInt8 !BCSimState -> BCSimState
writeAPin i b s=:{a_pins}
#! (a_pins, s) = writePin i a_pins b s
#! s = {s & a_pins = a_pins}
= s

getDhtTemp :: !BCSimState !UInt8 -> UInt16
getDhtTemp {temp} i = fromMaybe (abort ("Interface.getDhtTemp: Could not get pin with id: " +++ toString i)) ('M'.get i temp)

getDhtHumid :: !BCSimState !UInt8 -> UInt16
getDhtHumid {humidity} i = fromMaybe (abort ("Interface.getDhtHumid: Could not get pin with id: " +++ toString i)) ('M'.get i humidity)
