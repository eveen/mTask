implementation module mTask.Interpret.Device.Simulator.Task

from mTask.Interpret.Instructions import :: BCInstr, :: BCTaskType
from StdMisc import abort
from StdOverloaded import class zero
import mTask.Interpret.Compile
import mTask.Interpret.Device.Simulator.Heap
import mTask.Interpret.Device.Simulator.State
import mTask.Interpret.Device.Simulator.Symbols
import mTask.Interpret.Message
import mTask.Interpret.UInt
import StdBool
import iTasks
import StdDebug

derive consName MTTaskTreeNode

derive class iTask MTTask, MTStability, MTTaskTree, MTTaskTreeNode

sdsGetAddr :: !UInt8 !BCSimState -> UInt16
sdsGetAddr id {current_task, tasks}
# task = (tasks !! toInt current_task)
= sdsGetAddr` (fromInt 0) id task.MTTask.sds
where
	sdsGetAddr` :: !UInt16 !UInt8 ![BCShareSpec] -> UInt16
	sdsGetAddr` _ _ [] = abort "Task.sdsGetAddr.sdsGetAddr`: Could not find share in share list"
	sdsGetAddr` offset id [s:ss]
	| id == s.BCShareSpec.bcs_ident = offset
	| otherwise = sdsGetAddr` (inc offset) id ss

taskComplete :: ![UInt16] !MTTask !(SimChannels sds) !BCSimState -> Task BCSimState | RWShared sds
taskComplete [stab:ret] t chShare s
#! same = trace_n "Task.taskComplete" stab == t.stability
#! same = if (stab <> BCMT_NOVALUE) (same && ret == t.returnvalue) same
// TODO: Not used?
#! stable = stab == BCMT_STABLE
#! ret = createString zero (fromInt (length ret)) ret
#! val = if (stab == BCMT_NOVALUE) NoValue (Value ret (toStability stab))
// MTFTaskReturn UInt8 (Maybe (Bool, String255))
| not same = get chShare >>= \(outq, inq, b) -> set ([MTFTaskReturn t.MTTask.id val : outq], inq, b) chShare >>| return s
| otherwise = return s
where
	toStability :: UInt16 -> Stability
	toStability (UInt16 2) = True
	toStability _ = False

taskTreeClone :: !UInt16 !UInt16 !BCSimState -> (!UInt16, !BCSimState)
taskTreeClone treep rptr ss=:{heap}
#! tree = deref treep heap
#! new = tree
#! (newp, heap) = alloc new heap
#! new = {new & ptr = rptr}
#! ss = {ss & heap = heap}
#! (new, ss) = taskTreeClone` tree new newp ss
#! heap = update new newp heap
= (newp, {ss & heap = heap})
where
	taskTreeClone` :: !MTTaskTree !MTTaskTree !UInt16 !BCSimState -> (!MTTaskTree, !BCSimState)
	taskTreeClone` tree=:{node = NodeBCStableNode next w s1 s2} new newp ss
	#! (next, ss) = taskTreeClone next newp ss
	#! new = {new & node = NodeBCStableNode next w s1 s2}
	= (new, ss)
	taskTreeClone` tree=:{node = NodeBCUnstableNode next w u1 u2} new newp ss
	#! (next, ss) = taskTreeClone next newp ss
	#! new = {new & node = NodeBCUnstableNode next w u1 u2}
	= (new, ss)
	taskTreeClone` {node = NodeBCRepeat oldtree tree} new newp ss
	#! (tree, ss) = taskTreeClone tree newp ss
	#! (oldtree, ss) = taskTreeClone oldtree newp ss
	#! new = {new & node = NodeBCRepeat oldtree tree}
	= (new, ss)
	taskTreeClone` tree=:{node = NodeBCTAnd left right} new newp ss
	#! (left, ss) = taskTreeClone left newp ss
	#! (right, ss) = taskTreeClone right newp ss
	#! new = {new & node = NodeBCTAnd left right}
	= (new, ss)
	taskTreeClone` tree=:{node = NodeBCTOr left right} new newp ss
	#! (left, ss) = taskTreeClone left newp ss
	#! (right, ss) = taskTreeClone right newp ss
	#! new = {new & node = NodeBCTOr left right}
	= (new, ss)
	taskTreeClone` tree=:{node = NodeBCStep left rpc lw} new newp ss
	#! (left, ss) = taskTreeClone left newp ss
	#! new = {new & node = NodeBCStep left rpc lw}
	= (new, ss)
	taskTreeClone` tree=:{node = NodeBCSdsSet sdsp sdsid data} new newp ss
	#! (data, ss) = taskTreeClone data newp ss
	#! new = {new & node = NodeBCSdsSet sdsp sdsid data}
	= (new, ss)
	taskTreeClone` tree=:{node = NodeBCDelay wait done} new newp ss
	#! new = {new & node = NodeBCDelay wait ((toInt wait) + ss.time)}
	= (new, ss)
	taskTreeClone` _ new _ ss = (new, ss)
