definition module mTask.Interpret.Device.Simulator.Stack

from Data.Maybe import :: Maybe
from StdOverloaded import class length, class toInt
import mTask.Interpret.UInt
from mTask.Interpret.Device.Simulator.State import :: SimState, :: BCSimState
from mTask.Interpret.Device.Simulator.Task import :: MTTaskTree
import mTask.Interpret.String255

/*
 * I recommend importing this module qualified
 * Erin van der Veen
 */

:: Stack a :== [a]

/**
 * Create a new stack
 *
 * @param The size of the stack
 * @result The newly created stack
 */
newStack :: !Int -> (Stack a) | zero a

/**
 * Init Stack Main as in INITSTACKMAIN (interpret.h)
 */
initStackMain :: !(SimState a e) -> SimState a e | zero a

/**
 * Reset stack, basically does a splitAt on the sp.
 * This allows the interpret function to work as if
 * working on an empty stack
 *
 * @param The SimState
 *
 * @result (The bottom part of the stack, the old SP, a new SimState with a clean stack and sp = 0)
 */
resetStack :: !(SimState a e) -> (!Stack a, !Int, !SimState a e)

/**
 * Check if the Stack is empty
 */
emptyStack :: !(SimState a e) -> Bool

/**
 * Return the current size of the stack
 */
height :: !(SimState a e) -> Int

/**
 * Push an element on the Stack
 */
push :: !a !(SimState a e) -> SimState a e

/**
 * Push a list of elements to the Stack
 * Head of list is pushed first
 */
pushn :: ![a] !(SimState a e) -> SimState a e

/**
 * Push String255
 */
pushStr :: !String255 !BCSimState -> BCSimState

/**
 * Creates a string from a position of the stack and onwards
 * Used in BCSDSSET
 *
 * @param The SP starting the String (excluding stability)
 * @param The length of the resulting string
 * @param The (entire) stack (We cannot assume a slice, since the stack could be implemented as an array)
 *
 * @result The String
 */
//createString :: UInt16 UInt16 (Stack a) -> String255 | from16 a (Ideally)
createString :: !Int !UInt16 !(Stack UInt16) -> String255

/**
 * Pop an element from the Stack (note, not actually removed, sp is just lowered)
 */
pop :: !(SimState a e) -> (!a, !SimState a e)

/**
 * Pop n elements from the Stack
 * Does not return a result
 *
 * @param The number of elements that need to be popped
 */
popn :: !Int !(SimState a e) -> (SimState a e)

/**
 * Perform a return on the stack
 *
 * @param returnWidth
 * @param argWidth
 * @param The State
 *
 * @result The new stack, and a bool to indicate if the stack is empty
 */
stackreturn :: !Int !Int !BCSimState -> BCSimState

/**
 * Perform n right-rotations on the stack
 *
 * @param n
 * @param stack
 *
 * @result The rotated stack
 */
rotate :: !Int ![a] -> [a]

/**
 * Generalized list selector (nth element of a list)
 *
 * @param The Stack
 * @param The Index
 *
 * @result The nth element of the stack
 */
(!$) infixl 9 :: !(Stack a) !b -> a | toInt b

/**
 * Generlized updateAt function
 */
updateStack :: !a !b !(Stack b) -> Stack b | toInt a

/**
 * Generalized removeAt function
 * Also adds a new element to the end of the stack to retain stack size.
 */
removeAtStack :: !a !(Stack b) -> Stack b | toInt a & zero b
