definition module mTask.Interpret.Device.Simulator.Symbols

import mTask.Interpret.UInt

BCMT_NULL :== UInt16 65535
BCMT_REMOVE :== UInt16 255
BCMT_STABLE :== UInt16 2
BCMT_UNSTABLE :== UInt16 1
BCMT_NOVALUE :== UInt16 0
