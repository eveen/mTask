implementation module mTask.Interpret.Device.Simulator.Interpret

from mTask.Interpret.Device.Simulator.Task import :: MTTaskTree
from StdOverloaded import class toInt, class toBool
import mTask.Interpret.ByteCodeEncoding
import mTask.Interpret.Device.Simulator.Heap => qualified update
import mTask.Interpret.Device.Simulator.Stack
import mTask.Interpret.Device.Simulator.State
import mTask.Interpret.Device.Simulator.Symbols
import mTask.Interpret.Instructions
import StdArray
import StdInt
import StdReal

import iTasks

from Data.Func import $
from Data.Tuple import appSnd
from StdFunctions import flip, o

from StdMisc import abort
import StdDebug

interpret :: ![(Bool, BCInstr)] !BCSimState -> Task BCSimState
interpret program ss = trace_n "mTask.Interpret.interpret" interpret` program {ss & fp = ss.sp}
where
	interpret` :: ![(Bool, BCInstr)] !BCSimState -> Task BCSimState
	interpret` program ss=:{pc, returned}
	| returned = getNodeBP {ss & returned = False}
	#! (bp, instr) = program !! pc
	= if bp (updateInformation [] ss <<@ Title "Breakpoint reached, state: ") (return ss) >>= \ss = trace_n (consName{|*|} instr) evalInstr instr ss >>> \ss -> traceState ss interpret` program ss

getNodeBP :: BCSimState -> Task BCSimState
getNodeBP ss=:{heap} =
	    enterMultipleChoice [ChooseFromCheckGroup snd] (map (appSnd fromJust) o filter (isJust o snd) $ zip2 [0 ..] heap) <<@ Title "Select rewrite breakpoints. When target gets rewritten, breakpoint gets reached."
	>>= \bps -> return
	    {ss & heap = setBPS 0 heap (map fst bps)}
where
	setBPS :: Int (Heap MTTaskTree) [Int] -> Heap MTTaskTree
	setBPS _ heap [] = heap
	setBPS i [n:ns] [b:bs]
	| i == b = [setBP n : setBPS (inc i) ns bs]
	| otherwise = [n : setBPS (inc i) ns [b:bs]]

	setBP :: (Maybe MTTaskTree) -> Maybe MTTaskTree
	setBP Nothing = Nothing
	setBP (Just n) = Just {n & break = True}

/**
 * @param The State of the Simulator
 * @param The Operator that is to be applied on the top two stack elements
 * @result The new SimState where the top of the stack is the new result
 */
binop :: !BCSimState !(UInt16 UInt16 -> UInt16) -> BCSimState
binop ss=:{stack, sp} op
# stack = updateAt (sp - 2) (op (stack!!(sp - 2)) (stack!!(sp - 1))) stack
# sp = sp - 1
= {ss & stack = stack, sp = sp}

// Takes two bools and pushes a new bool
binopb :: !BCSimState !(Bool Bool -> Bool) -> BCSimState
binopb ss=:{stack, sp} op
# stack = updateAt (sp - 2) (fromInt o toInt $ (op (toBool $ stack!!(sp - 2)) (toBool $ stack!!(sp - 1)))) stack
# sp = sp - 1
= {ss & stack = stack, sp = sp}

// Takes to UInt16 and pushes a bool
binopib :: !BCSimState !(UInt16 UInt16 -> Bool) -> BCSimState
binopib ss=:{stack, sp} op
# stack = updateAt (sp - 2) (fromInt o toInt $ (op (stack!!(sp - 2)) (stack!!(sp - 1)))) stack
# sp = sp - 1
= {ss & stack = stack, sp = sp}

instance toBool UInt16 where
	toBool (UInt16 i) = i <> 0

instance toInt Bool where
	toInt True = 1
	toInt False = 0

from32 :: !Int -> (UInt16, UInt16)
from32 i = (fromInt (i >> 16), fromInt (0xffff bitand i))

binop2 :: !BCSimState !(Int Int -> Int) -> BCSimState
binop2 ss f
# res = f (binop2lhs ss) (binop2rhs ss)
# (rh, rl) = from32 res
= popn 4 ss >>> push rh >>> push rl

binop2b :: !BCSimState !(Int Int -> Bool) -> BCSimState
binop2b ss f
# res = f (binop2lhs ss) (binop2rhs ss)
# res = fromInt o toInt $ res
= popn 4 ss >>> push res

binop2lhs :: !BCSimState -> Int
binop2lhs ss=:{stack, sp}
# h = (toInt ((stack !$ (sp - fromInt 4)))) << 16
# l = (toInt ((stack !$ (sp - fromInt 3)))) bitand 0xffff
= h + l

binop2rhs :: !BCSimState -> Int
binop2rhs ss=:{stack, sp}
# h = (toInt ((stack !$ (sp - fromInt 2)))) << 16
# l = (toInt ((stack !$ (sp - fromInt 1)))) bitand 0xffff
= h + l

binopr :: !BCSimState !(Real Real -> Real) -> BCSimState
binopr ss f
# lhs = convert_float_in_int_to_real $ binop2lhs ss
# rhs = convert_float_in_int_to_real $ binop2rhs ss
# res = convert_real_to_float_in_int $ f lhs rhs
# (rh, rl) = from32 res
= popn 4 ss >>> push rh >>> push rl

binoprb :: !BCSimState !(Real Real -> Bool) -> BCSimState
binoprb ss f
# lhs = convert_float_in_int_to_real $ binop2lhs ss
# rhs = convert_float_in_int_to_real $ binop2rhs ss
# res = fromInt o toInt $ f lhs rhs
= popn 4 ss >>> push res

bcreturn :: !UInt8 !UInt8 !BCSimState -> BCSimState
bcreturn i j ss = trace_n "mTask.Interpret.bcreturn" stackreturn (toInt i) (toInt j) ss

bcrotc :: !UInt8 !UInt8 !BCSimState -> BCSimState
bcrotc depth num ss=:{stack,sp}
#! (base, r) = splitAt sp stack
#! (no, yes) = splitAt (sp - (toInt depth)) base
#! yes = rotate (toInt num) yes
#! stack = no ++ yes ++ r
= {ss & stack = stack}

bcargc :: !UInt8 !BCSimState -> BCSimState
bcargc arg ss=:{sp, stack, fp} = push (stack !$ (dec (fromInt fp) - arg)) ss

from16 :: !UInt8 !UInt8 -> UInt16
from16 c1 c2 = fromInt $ (toInt c1) * 256 + (toInt c2)

to16 :: !UInt16 -> (!Char, !Char)
to16 i = (h, l)
where
	h = toChar ((toInt i) >> 8)
	l = toChar ((toInt i) bitand 0x00FF)

incpc :: !BCSimState -> BCSimState
incpc ss=:{pc} = {ss & pc = inc pc}

decsp :: !BCSimState -> BCSimState
decsp ss=:{sp} = {ss & sp = dec sp}

bcstab :: !(UInt16 -> Bool) !BCSimState -> BCSimState
bcstab f ss
# (v, ss) = pop ss
= push (if (f v) (fromInt 1) (fromInt 0)) ss

/**
 * @param The jumpLabel as read from the BC
 * @param The State
 *
 * @result The Adjusted jumpLabel
 */
fixJumpLabel :: !UInt16 !BCSimState -> UInt16
fixJumpLabel j ss=:{current_task, tasks} = fixJumpLabel` j zero (map snd (tasks !! toInt current_task).bc)
where
	fixJumpLabel` :: !UInt16 !UInt16 ![BCInstr] -> UInt16
	fixJumpLabel` (UInt16 0) i _ = i
	fixJumpLabel` l i [x:xs] = fixJumpLabel` (l - fromInt (pcWidth x)) (inc i) xs
	where
		pcWidth :: !BCInstr -> Int
		pcWidth (BCReturn1 _) = 2
		pcWidth (BCReturn2 _)  = 2
		pcWidth (BCReturn3 _) = 2
		pcWidth (BCReturn_0 _) = 2
		pcWidth (BCReturn_1 _) = 2
		pcWidth (BCReturn_2 _) = 2
		pcWidth (BCReturn _ _) = 3
		pcWidth (BCJumpF _) = 3 // JumpLabel has a width of 2
		pcWidth (BCJump _) = 3
		pcWidth (BCLabel _) = 3
		pcWidth (BCJumpSR _ _) = 4
		pcWidth (BCTailcall _ _ _) = 5
		pcWidth (BCArg _) = 2
		pcWidth (BCArgs _ _) = 3
		pcWidth (BCMkTask _) = 2
		pcWidth (BCPush1 _) = 2
		pcWidth (BCPush2 _ _) = 3
		pcWidth (BCPush3 _ _ _) = 4
		pcWidth (BCPush4 _ _ _ _) = 5
		pcWidth (BCPush (String255 s)) = 1 + (size s)
		pcWidth (BCPop _) = 2
		pcWidth (BCRot _ _) = 3
		pcWidth _ = 1

evalInstr :: !BCInstr !BCSimState -> BCSimState
evalInstr BCReturn1_0 ss = bcreturn (fromInt 1) (fromInt 0) ss
evalInstr BCReturn1_1 ss = bcreturn (fromInt 1) (fromInt 1) ss
evalInstr BCReturn1_2 ss = bcreturn (fromInt 1) (fromInt 2) ss
evalInstr (BCReturn1 arg) ss = bcreturn (fromInt 1) arg ss
evalInstr BCReturn2_0 ss = bcreturn (fromInt 2) (fromInt 0) ss
evalInstr BCReturn2_1 ss = bcreturn (fromInt 2) (fromInt 1) ss
evalInstr BCReturn2_2 ss = bcreturn (fromInt 2) (fromInt 2) ss
evalInstr (BCReturn2 arg) ss = bcreturn (fromInt 2) arg ss
evalInstr BCReturn3_0 ss = bcreturn (fromInt 3) (fromInt 0) ss
evalInstr BCReturn3_1 ss = bcreturn (fromInt 3) (fromInt 1) ss
evalInstr BCReturn3_2 ss = bcreturn (fromInt 3) (fromInt 2) ss
evalInstr (BCReturn3 arg) ss = bcreturn (fromInt 3) arg ss
evalInstr (BCReturn_0 ret) ss = bcreturn ret (fromInt 0) ss
evalInstr (BCReturn_1 ret) ss = bcreturn ret (fromInt 1) ss
evalInstr (BCReturn_2 ret) ss = bcreturn ret (fromInt 2) ss
evalInstr (BCReturn ret arg) ss = bcreturn ret arg ss
evalInstr (BCJumpF (JL jumpLabel)) ss=:{stack,sp}
| stack !$ (dec sp) <> fromInt 0 = decsp ss >>> incpc
| otherwise = decsp ss >>> \ss -> {ss & pc = toInt (fixJumpLabel jumpLabel ss)}
evalInstr (BCJump (JL jumpLabel)) ss = {ss & pc = toInt (fixJumpLabel jumpLabel ss)}
evalInstr (BCLabel jumpLabel) ss = abort "This should not happen"
evalInstr (BCJumpSR w (JL l)) ss=:{sp,stack,fp,pc}
#! stack = updateStack ((fromInt sp) - w - one) (inc o fromInt $ pc) stack
#! fp = sp
#! pc = toInt $ fixJumpLabel l ss
= {ss & stack = stack, fp = fp, pc = pc}
evalInstr (BCTailcall i j (JL l)) ss=:{fp}
// We distructively update i and j, so do calculations that depend om them first
#! fp = toInt $ ((fromInt fp) - i) + j
#! i = i + (fromInt 3) + j
#! ss = bcrotc i j ss
#! sp = fp
#! pc = toInt $ fixJumpLabel l ss
= {ss & fp = fp, sp = sp, pc = pc}
evalInstr BCArg0 ss = bcargc (fromInt 0) ss >>> incpc
evalInstr BCArg1 ss = bcargc (fromInt 1) ss >>> incpc
evalInstr BCArg2 ss = bcargc (fromInt 2) ss >>> incpc
evalInstr BCArg3 ss = bcargc (fromInt 3) ss >>> incpc
evalInstr BCArg4 ss = bcargc (fromInt 4) ss >>> incpc
evalInstr (BCArg argWidth) ss = bcargc argWidth ss >>> incpc
evalInstr BCArg10 ss = bcargc (fromInt 1) ss >>> bcargc (fromInt 0) >>> incpc
evalInstr BCArg21 ss = bcargc (fromInt 2) ss >>> bcargc (fromInt 1) >>> incpc
evalInstr BCArg32 ss = bcargc (fromInt 3) ss >>> bcargc (fromInt 2) >>> incpc
evalInstr BCArg43 ss = bcargc (fromInt 4) ss >>> bcargc (fromInt 3) >>> incpc
evalInstr (BCArgs i j) ss = (if (i <= j) (bcargs i j ss) (bcargs j i ss)) >>> incpc
where
	bcargs :: !UInt8 !UInt8 !BCSimState -> BCSimState
	bcargs fr to ss
	| fr > to = ss
	| otherwise = bcargc fr ss >>> bcargs (inc fr) to
evalInstr (BCMkTask tt) ss = buildNode tt ss >>> incpc// Call helper function.
evalInstr BCIsStable ss = bcstab ((==) BCMT_STABLE) ss
evalInstr BCIsUnstable ss = bcstab ((==) BCMT_UNSTABLE) ss
evalInstr BCIsNoValue ss = bcstab ((==) BCMT_NOVALUE) ss
evalInstr BCIsValue ss = bcstab ((<>) BCMT_NOVALUE) ss
evalInstr (BCPush1 c) ss = ss >>> push (from16 (fromInt 0) c) >>> incpc
evalInstr (BCPush2 h l) ss = ss >>> push (from16 h l) >>> incpc
evalInstr (BCPush4 h1 l1 h2 l2) ss = ss >>> push (from16 h1 l1) >>> push (from16 h2 l2) >>> incpc
evalInstr (BCPush (String255 str)) ss = bcpush 0 (size str) str ss >>> incpc
where
	bcpush :: !Int !Int !String !BCSimState -> BCSimState
	bcpush fr to str ss
	| fr == to = ss
	| (to - fr) rem 2 <> 0 = push (from16 (fromInt 0) (fromInt o toInt $ str.[fr])) ss >>> bcpush (inc fr) to str
	# i = from16 (fromInt o toInt $ str.[fr]) (fromInt o toInt $ str.[inc fr])
	= push i ss >>> bcpush (fr + 2) to str
evalInstr BCPushNull ss = push BCMT_NULL ss >>> incpc
evalInstr BCPop1 ss = ss >>> popn 1 >>> incpc
evalInstr BCPop2 ss = ss >>> popn 2 >>> incpc
evalInstr BCPop3 ss = ss >>> popn 3 >>> incpc
evalInstr BCPop4 ss = ss >>> popn 4 >>> incpc
evalInstr (BCPop n) ss
| n <= zero = ss >>> incpc
| otherwise = ss >>> evalInstr BCPop1 >>> evalInstr (BCPop (dec n)) >>> incpc
evalInstr (BCRot depth n) ss = bcrotc depth n ss >>> incpc
evalInstr BCDup ss
# (p, ss) = pop ss
= push p ss >>> push p >>> incpc
evalInstr BCPushPtrs ss=:{sp, fp} = push (fromInt sp) ss >>> push (fromInt fp) >>> push (fromInt 0) >>> incpc
evalInstr BCAddI ss = binop ss (+) >>> incpc
evalInstr BCSubI ss = binop ss (-) >>> incpc
evalInstr BCMultI ss = binop ss (*) >>> incpc
evalInstr BCDivI ss = binop ss (/) >>> incpc
evalInstr BCAddL ss = binop2 ss (+) >>> incpc
evalInstr BCSubL ss = binop2 ss (-) >>> incpc
evalInstr BCMultL ss = binop2 ss (*) >>> incpc
evalInstr BCDivL ss = binop2 ss (/) >>> incpc
evalInstr BCAddR ss = binopr ss (+) >>> incpc
evalInstr BCSubR ss = binopr ss (-) >>> incpc
evalInstr BCMultR ss = binopr ss (*) >>> incpc
evalInstr BCDivR ss = binopr ss (/) >>> incpc
evalInstr BCAnd ss = binopb ss (&&) >>> incpc
evalInstr BCOr ss = binopb ss (||) >>> incpc
evalInstr BCNot ss
# (x, ss) = pop ss
= push (fromInt (toInt (x == zero))) ss >>> incpc
evalInstr BCEqI ss = binopib ss (==) >>> incpc
evalInstr BCNeqI ss = binopib ss (<>) >>> incpc
evalInstr BCEqL ss = binop2b ss (==) >>> incpc
evalInstr BCNeqL ss = binop2b ss (<>) >>> incpc
evalInstr BCLeI ss = binopib ss (<) >>> incpc
evalInstr BCGeI ss = binopib ss (>) >>> incpc
evalInstr BCLeqI ss = binopib ss (<=) >>> incpc
evalInstr BCGeqI ss = binopib ss (>=) >>> incpc
evalInstr BCLeL ss = binop2b ss (<) >>> incpc
evalInstr BCGeL ss = binop2b ss (>) >>> incpc
evalInstr BCLeqL ss = binop2b ss (<=) >>> incpc
evalInstr BCGeqL ss = binop2b ss (>=) >>> incpc
evalInstr BCLeR ss = binoprb ss (<) >>> incpc
evalInstr BCGeR ss = binoprb ss (>) >>> incpc
evalInstr BCLeqR ss = binoprb ss (<=) >>> incpc
evalInstr BCGeqR ss = binoprb ss (>=) >>> incpc

buildNode :: !BCTaskType !BCSimState -> BCSimState
buildNode tt ss=:{heap}
#! (node, ss) = buildNode` tt ss
#! tree = { MTTaskTree
	| task_type = tt
	, trash = False
	, ptr = ss.current_task
	, node = node
	, break = False
	}
#! (ptr, heap) = alloc tree heap
= push ptr {ss & heap = heap}
where
	// TODO: Fix ptr for child tasks when necessary
	buildNode` :: !BCTaskType !BCSimState -> (!MTTaskTreeNode, !BCSimState)
	buildNode` BCStable0 ss = (NodeBCStable0, ss)
	buildNode` BCStable1 ss=:{stack,sp} = pop ss >>>= \(s, ss) -> (NodeBCStable1 s, ss)
	buildNode` BCStable2 ss=:{stack,sp} = pop ss >>>= \(s1, ss) -> pop ss >>>= \(s0, ss) -> (NodeBCStable2 s0 s1, ss)
	buildNode` BCStable3 ss=:{stack,sp} = pop ss >>>= \(s2, ss) -> pop ss >>>= \(s1, ss) -> pop ss >>>= \(s0, ss) -> (NodeBCStable3 s0 s1 s2, ss)
	buildNode` BCStable4 ss=:{stack,sp} = pop ss >>>= \(s3, ss) -> pop ss >>>= \(s2, ss) -> pop ss >>>= \(s1, ss) -> pop ss >>>= \(s0, ss) -> (NodeBCStable4 s0 s1 s2 s3, ss)
	buildNode` (BCStableNode w) ss = pop ss >>>= \(next, ss) -> pop ss >>>= \(s1, ss) -> pop ss >>>= \(s0, ss) -> (NodeBCStableNode next w s0 s1, ss)
	buildNode` BCUnstable0 ss = (NodeBCUnstable0, ss)
	buildNode` BCUnstable1 ss = pop ss >>>= \(s, ss) -> (NodeBCUnstable1 s, ss)
	buildNode` BCUnstable2 ss = pop ss >>>= \(s1, ss) -> pop ss >>>= \(s0, ss) -> (NodeBCUnstable2 s0 s1, ss)
	buildNode` BCUnstable3 ss = pop ss >>>= \(s2, ss) -> pop ss >>>= \(s1, ss) -> pop ss >>>= \(s0, ss) -> (NodeBCUnstable3 s0 s1 s2, ss)
	buildNode` BCUnstable4 ss = pop ss >>>= \(s3, ss) -> pop ss >>>= \(s2, ss) -> pop ss >>>= \(s1, ss) -> pop ss >>>= \(s0, ss) -> (NodeBCUnstable4 s0 s1 s2 s3, ss)
	buildNode` (BCUnstableNode w) ss = pop ss >>>= \(next, ss) -> pop ss >>>= \(s1, ss) -> pop ss >>>= \(s0, ss) -> (NodeBCUnstableNode next w s0 s1, ss)
	buildNode` BCReadD ss = pop ss >>>= \(val, ss) -> (NodeBCReadD (fromInt o toInt $ val), ss)
	buildNode` BCWriteD ss = pop ss >>>= \(val, ss) -> pop ss  >>>= \(tar, ss) -> (NodeBCWriteD (fromInt o toInt $ tar) (toBool val), ss)
	buildNode` BCReadA ss = pop ss >>>= \(val, ss) -> (NodeBCReadA (fromInt o toInt $ val), ss)
	buildNode` BCWriteA ss = pop ss >>>= \(val, ss) -> pop ss  >>>= \(tar, ss) -> (NodeBCWriteA (fromInt o toInt $ tar)  (fromInt o toInt $ tar), ss)
	buildNode` BCRepeat ss = pop ss >>>= \(old, ss) -> (NodeBCRepeat old BCMT_NULL, ss)
	buildNode` BCDelay ss = pop ss >>>= \(w, ss) -> (NodeBCDelay w (toInt w + ss.time), ss)
	buildNode` BCTAnd ss = pop ss >>>= \(rhs, ss) -> pop ss >>>= \(lhs, ss) -> (NodeBCTAnd lhs rhs , ss)
	buildNode` BCTOr ss = pop ss >>>= \(rhs, ss) -> pop ss >>>= \(lhs, ss) -> (NodeBCTOr lhs rhs, ss)
	buildNode` (BCStep lw (JL rhs)) ss = pop ss >>>= \(lhs, ss) -> (NodeBCStep lhs rhs lw, ss)
	buildNode` (BCSdsGet id) ss = let offset = sdsGetAddr id ss in (NodeBCSdsGet offset id, ss)
	buildNode` (BCSdsSet id) ss = let offset = sdsGetAddr id ss in pop ss >>>= \(data, ss) -> (NodeBCSdsSet offset id data, ss)
