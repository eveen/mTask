definition module mTask.Interpret.Device.Simulator.Heap

import StdList
from StdOverloaded import class zero
import mTask.Interpret.UInt
import Data.Maybe

:: Offset :== UInt16

// Nothing -> Unallocated
:: Heap a :== [Maybe a]

/**
 * Create a Heap
 *
 * @param Size of the Heap
 */
newHeap :: !Int -> Heap a

/**
 * Allocate and place someting on the Heap
 *
 * @param The item to be placed on the Heap
 *
 * @result The offset on the Heap
 */
alloc :: !a !(Heap a) -> (!Offset, !Heap a)

/**
 * Free a location on the Heap
 *
 * @param The Offset on the Heap
 */
free :: !Offset !(Heap a) -> Heap a

/**
 * Dereference a Offset
 *
 * @param The Offset
 *
 * @result The element of the heap
 */
deref :: !Offset !(Heap a) -> a

/**
 * Update an element on the Heap
 * Throws error on update of unallocated space
 *
 * @param The new element
 * @param The offset on the Heap
 */
update :: !a !Offset !(Heap a) -> Heap a
