definition module mTask.Interpret.Device.Simulator.State

from mTask.Interpret.Device.Simulator.Heap import :: Heap
from mTask.Interpret.Device.Simulator.Stack import :: Stack
from mTask.Interpret.Device.Simulator import :: SimSettings
from mTask.Interpret.Specification import :: MTDeviceSpec
from mTask.Interpret.UInt import :: UInt16

import mTask.Interpret.Device.Simulator.Task
from mTask.Interpret.Compile import :: BCShareSpec

/**
 * The State of the Simulator
 *
 * @param Type of the elements on the Stack
 * @param Type of the elements on the Heap
 */
:: SimState a e =
	{ stack :: !Stack a                         //* The Device Stack
	, pc :: !Int                                //* Program Counter
	, sp :: !Int                                //* Stack Pointer
	, fp :: !Int                                //* Frame Pointer
	, max_sp :: !Int                            //* The maximum size of the stack (do we need this?)
	, tasks :: ![MTTask]                        //* The List of Tasks
	, heap :: !Heap e                           //* The Heap (Used only for taskTrees
	, current_task :: !UInt16                   //* The current Task (as in client.c)
	, returned :: !Bool                         //* Indicates whether we have finished interpreting
	, time :: !Int                              //* Time since starting Simulation in Miliseconds
	, consumed_energy :: !Real                  //* Records the amount of energy that is consumed by the program
	, temp :: !Map UInt8 UInt16                 //* Temperature that should be returned by temperature sensor (Celcius)
	, humidity :: !Map UInt8 UInt16             //* Humidity that should be returned by the humidity sensor (Percentage)
	, d_pins :: !Map UInt8 (DigitalPeripheral)  //* Map of DPins
	, a_pins :: !Map UInt8 (AnaloguePeripheral) //* Map of DPins
	}

:: BCSimState :== SimState UInt16 MTTaskTree

derive class iTask SimState

/**
 * Prints state and returns the a
 * Specific instance of trace_n
 */
traceState :: !BCSimState !a -> a

/**
 * Initiate the State
 */
newState :: !SimSettings -> BCSimState

/**
 * Bind Alternative.
 * We cannot have a Monad, since we cannot implement Applicative
 */
(>>>) infixl 1 :: !(SimState a e) !((SimState a e) -> b) -> b

/**
 * Another combinator
 */
(>>>=) infixl 1 :: !(!b, !SimState a e) !((b, SimState a e) -> (c, SimState a e)) -> (!c, !SimState a e)
