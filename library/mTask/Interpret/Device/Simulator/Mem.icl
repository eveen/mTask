implementation module mTask.Interpret.Device.Simulator.Mem

import mTask.Interpret.Device.Simulator.State
import mTask.Interpret.Device.Simulator.Task
import mTask.Interpret.Device.Simulator.Heap
import mTask.Interpret.Device.Simulator.Symbols
from StdFunctions import flip
from StdList import filter
import Data.GenCons

import StdDebug
from StdMisc import abort

memGC :: !BCSimState -> BCSimState
memGC s=:{tasks, heap} = trace_n "Running Garbage Collection" {s & tasks = filter (\t -> t.tree <> BCMT_REMOVE) tasks, heap = map (gcNode) heap}
where
	gcNode :: !(Maybe MTTaskTree) -> Maybe MTTaskTree
	gcNode (Just n)
	| n.trash = Nothing
	| otherwise = Just n
	gcNode _ = Nothing

memMarkTrashDeep :: !Offset !BCSimState -> BCSimState
memMarkTrashDeep offset s=:{heap} = {s & heap = markTrashDeepNode offset heap}
where
	markTrashDeepNode :: !Offset !(Heap MTTaskTree) -> Heap MTTaskTree
	markTrashDeepNode ptr heap
	#! node = trace "Marking node: " deref ptr heap
	// Mark this node
	#! heap = trace_n (consName{|*|} node.node) update {node & trash = True} ptr heap
	// Get his children
	#! children = getChildren node.node
	// Recurse on the children
	= foldl (flip markTrashDeepNode) heap children

memMarkTrash :: !Offset !(Heap MTTaskTree) -> Heap MTTaskTree
memMarkTrash ptr heap = update {deref ptr heap & trash = True, ptr = BCMT_NULL} ptr heap

getChildren :: !MTTaskTreeNode -> [Offset]
getChildren (NodeBCStableNode n _ _ _) = [n]
getChildren (NodeBCUnstableNode n _ _ _) = [n]
getChildren (NodeBCStep n _ _) = [n]
getChildren (NodeBCTAnd l r) = [l, r]
getChildren (NodeBCTOr l r) = [l, r]
getChildren (NodeBCSdsSet _ _ n) = [n]
getChildren _ = []

memNodeMove :: !Offset !Offset !BCSimState -> BCSimState
memNodeMove to fr s=:{heap}
#! fn = trace_n "Mem.memNodeMove" deref fr heap
#! nn = {fn & trash = False}
#! heap = update nn to heap
#! children = getChildren nn.node
#! heap = foldl (flip (updateChild to)) heap children
= {s & heap = heap}
where
	updateChild :: !Offset !Offset !(Heap MTTaskTree) -> Heap MTTaskTree
	updateChild new ptr heap
	#! child = deref ptr heap
	#! heap = update {child & ptr = new} ptr heap
	= heap
