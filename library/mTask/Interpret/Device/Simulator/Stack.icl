implementation module mTask.Interpret.Device.Simulator.Stack

from Data.Func import $
from Data.Maybe import :: Maybe
from mTask.Interpret.Device.Simulator.Interpret import to16
from StdOverloaded import class length, class toInt
import Data.List
import mTask.Interpret.Device.Simulator.State
import mTask.Interpret.String255
import mTask.Interpret.UInt
import StdArray
import StdInt
import StdList

import StdMisc
import StdDebug

newStack :: !Int -> (Stack a) | zero a
newStack i = repeatn i zero

initStackMain :: !(SimState a e) -> SimState a e | zero a
initStackMain ss = trace_n "Stack.initStackMain" pushn [zero, zero, zero] ss

resetStack :: !(SimState a e) -> (!Stack a, !Int, !SimState a e)
resetStack ss=:{stack, sp}
# (old, new) = splitAt sp stack
= (old, sp, {ss & sp = zero, stack = new})

emptyStack :: !(SimState a e) -> Bool
emptyStack {sp} = sp == zero

height :: !(SimState a e) -> Int
height {sp} = sp

push :: !a !(SimState a e) -> SimState a e
push e s=:{stack, sp} = {s & stack = updateAt sp e stack, sp = sp + 1}

pushn :: ![a] !(SimState a e) -> SimState a e
pushn [] ss = ss
pushn [x:xs] ss = push x ss >>> pushn xs

pushStr :: !String255 !BCSimState -> BCSimState
pushStr s ss = pushStr` 0 (toString s) ss
where
	pushStr` :: !Int !String !BCSimState -> BCSimState
	pushStr` i str ss
	| i >= size str = ss
	| otherwise = push (fromInt (toInt str.[i])) ss >>> push (fromInt (toInt str.[i + 1])) >>> pushStr` (i + 2) str

createString :: !Int !UInt16 !(Stack UInt16) -> String255
createString sp l s = trace_n "Stack.createString" fromString $ createString` sp zero l s
where
	createString` :: !Int !UInt16 !UInt16 !(Stack UInt16) -> String
	createString` sp i len s
	| i >= len = ""
	| otherwise = {h, l} +++ createString` sp (inc i) len s
		where
			(h, l) = to16 (s!$(fromInt sp + i))

pop :: !(SimState a e) -> (!a, !SimState a e)
pop s=:{stack,sp}
| sp == 0 = abort "Stack.pop: Popping from empty stack"
# sp = sp - 1
= (stack!!sp, {s & sp = sp})

popn :: !Int !(SimState a e) -> (SimState a e)
popn n s=:{sp}
| sp < n = abort "Stack.popn: Not enough elements on the stack"
= {s & sp = sp - n}

stackreturn :: !Int !Int !BCSimState -> BCSimState
stackreturn returnwidth argwidth ss=:{stack,sp,fp}
#! sp = (toInt (stack!!(fp - argwidth - 3))) + returnwidth;
#! pc = toInt (stack!!(fp - argwidth - 1))
#! i = toInt $ stack!!(fp - argwidth - 2)
#! stack = fix returnwidth stack
#! finished = sp - returnwidth == 0
#! ss = if finished ss {ss & fp = i}
#! ss = {ss & sp = sp, pc = pc, stack = stack, returned = finished}
= ss
where
	fix 0 st = st
	fix j st
	# st = (fix (j - 1) st)
	// -4 and dec j because for loop
	= updateAt (fp - argwidth - 4 + j) (st!!(fp + (dec j))) st

rotate :: !Int ![a] -> [a]
rotate _ [] = []
rotate n xs = take lxs o drop (lxs - n) $ (cycle xs)
where
	lxs = length xs

(!$) infixl 9 :: !(Stack a) !b -> a | toInt b
(!$) s b = s !! (toInt b)

updateStack :: !a !b !(Stack b) -> Stack b | toInt a
updateStack a b c = updateAt (toInt a) b c

removeAtStack :: !a !(Stack b) -> Stack b | toInt a & zero b
removeAtStack x s = removeAt (toInt x) s ++ [zero]
