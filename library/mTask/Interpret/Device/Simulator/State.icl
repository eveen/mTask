implementation module mTask.Interpret.Device.Simulator.State

from mTask.Interpret.Compile import :: BCShareSpec
from mTask.Interpret.Device.Simulator.Heap import :: Heap, newHeap
from mTask.Interpret.Device.Simulator.Stack import :: Stack, newStack
from mTask.Interpret.Device.Simulator.Task import :: MTTask, :: MTTaskTree
import mTask.Interpret.Device.Simulator.Energy
import mTask.Interpret.Device.Simulator
import mTask.Interpret.Specification
import mTask.Interpret.UInt

from Data.Func import $
import qualified Data.Map as M
import Data.Map.GenJSON
from StdMisc import abort

from StdDebug import trace_n

derive class iTask SimState

derive genShow SimState, JumpLabel, UInt16, MTTaskTree, Maybe, MTTask, MTTaskTreeNode, BCTaskType, BCShareSpec, BCInstr, UInt8, String255, 'M'.Map, AnaloguePeripheral, DigitalPeripheral

traceState :: !BCSimState !a -> a
traceState ss x = trace_n (foldr (+++) "" (genShow{|*|} "" False ss [])) x

newState :: !SimSettings -> BCSimState
newState {sim_spec, d_vals, a_vals, temps, humidities}
# stack_size = toInt $ sim_spec.memory / (fromInt 2)
# heap_size = toInt $ sim_spec.memory / (fromInt 2)
= { SimState
	| stack = newStack stack_size
	, pc = 0
	, sp = 0
	, fp = 0
	, max_sp = stack_size
	, tasks = []
	, heap = newHeap heap_size
	, current_task = zero
	, returned = False
	, time = zero
	, consumed_energy = zero
	, temp = 'M'.fromList temps
	, humidity = 'M'.fromList humidities
	, d_pins = 'M'.fromList d_vals
	, a_pins = 'M'.fromList a_vals
	}

(>>>) infixl 1 :: !(SimState a e) !((SimState a e) -> b) -> b
(>>>) e f = f e

(>>>=) infixl 1 :: !(!b, !SimState a e) !((b, SimState a e) -> (c, SimState a e)) -> (!c, !SimState a e)
(>>>=) e f = f e
