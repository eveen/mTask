definition module mTask.Interpret.Device.Simulator.Mem

import mTask.Interpret.Device.Simulator.State
from mTask.Interpret.Device.Simulator.Heap import :: Offset
from mTask.Interpret.UInt import :: UInt16

/**
 * Performs Garbage collection on the tasks and heap
 */
memGC :: !BCSimState -> BCSimState

/**
 * Recursivelly mark an entire tree for garbage collection
 */
memMarkTrashDeep :: !Offset !BCSimState -> BCSimState

/**
 * @param Destination
 * @param Source
 * @param State
 *
 * Move task to location
 */
memNodeMove :: !Offset !Offset !BCSimState -> BCSimState
