implementation module mTask.Interpret.Device.Simulator.Rewrite

from mTask.Interpret.Device.Simulator.Task import :: MTTaskTree
from mTask.Interpret.UInt import :: UInt16
import Data.GenCons
import mTask.Interpret.Compile
import mTask.Interpret.Device.Simulator.Heap
import mTask.Interpret.Device.Simulator.Interface
import mTask.Interpret.Device.Simulator.Interpret
import mTask.Interpret.Device.Simulator.Mem
import mTask.Interpret.Device.Simulator.Stack
import mTask.Interpret.Device.Simulator.State
import mTask.Interpret.Device.Simulator.Symbols
import mTask.Interpret.Instructions
import mTask.Interpret.String255
import Text

import iTasks

from Data.Func import $

import StdDebug
from StdMisc import abort

rewrite :: !(Shared sds BCSimState) !UInt16 ![(Bool, BCInstr)] ![BCShareSpec] !BCSimState -> Task (![BCShareSpec], !BCSimState) | RWShared sds
rewrite sShare ptr bc sds ss=:{heap}
#! tree=:{break} = trace "Rewriting Task Node at " trace_n ptr traceState ss deref ptr heap
= if break (updateInformation [] ss <<@ Title "Breakpoint reached, state: ") (return ss)
  >>= \ss -> trace_n ("\t\t" +++ (consName{|*|} tree.node)) rewrite` ptr bc sds tree ss
  >>= \(sds, ss) = trace_n "Rewrite step complete" set ss sShare >>| return (sds, ss)

rewrite` :: !UInt16 ![(Bool, BCInstr)] ![BCShareSpec] !MTTaskTree !BCSimState -> Task (![BCShareSpec], !BCSimState)
rewrite` ptr _ sds {node = NodeBCStable0} ss = push BCMT_STABLE ss >>> \s -> return (sds, s)
rewrite` ptr _ sds {node = NodeBCStable1 i} ss = push BCMT_STABLE ss >>> push i >>> \s -> return (sds, s)
rewrite` ptr _ sds {node = NodeBCStable2 i1 i2} ss = pushn [BCMT_STABLE , i1, i2] ss >>> \s -> return (sds, s)
rewrite` ptr _ sds {node = NodeBCStable3 i1 i2 i3} ss = pushn [BCMT_STABLE, i1, i2, i3] ss >>> \s -> return (sds, s)
rewrite` ptr _ sds {node = NodeBCStable4 i1 i2 i3 i4} ss = pushn [BCMT_STABLE, i1, i2, i3, i4] ss >>> \s -> return (sds, s)
// TODO: Check if still correct
rewrite` ptr bc sds {node = NodeBCStableNode next w i1 i2} ss=:{heap} = pushn [BCMT_STABLE, i1, i2] ss >>> \s=:{sp} -> rewrite` next bc sds (deref next heap) s >>= \(sds, s=:{stack}) -> return (sds, {s & stack = removeAtStack sp stack})
rewrite` ptr _ sds {node = NodeBCUnstable0} ss = push BCMT_UNSTABLE ss >>> \s -> return (sds, s)
rewrite` ptr _ sds {node = NodeBCUnstable1 i1} ss = pushn [BCMT_UNSTABLE, i1] ss >>> \s -> return (sds, s)
rewrite` ptr _ sds {node = NodeBCUnstable2 i1 i2} ss = pushn [BCMT_UNSTABLE, i1, i2] ss >>> \s -> return (sds, s)
rewrite` ptr _ sds {node = NodeBCUnstable3 i1 i2 i3} ss = pushn [BCMT_UNSTABLE, i1, i2, i3] ss >>> \s -> return (sds, s)
rewrite` ptr _ sds {node = NodeBCUnstable4 i1 i2 i3 i4} ss = pushn [BCMT_UNSTABLE, i1, i2, i3, i4] ss >>> \s -> return (sds, s)
rewrite` ptr _ sds {node = NodeBCUnstableNode next w i1 i2} ss = push BCMT_UNSTABLE ss >>> \s -> return (sds, s)
rewrite` ptr _ sds {node = NodeBCReadD i} ss
#! (d_val, ss) = readDPin i ss
= pushn [BCMT_UNSTABLE, fromInt o toInt $ d_val] ss >>> \s -> return (sds, s)
rewrite` ptr _ sds tt=:{node = NodeBCWriteD i val} ss=:{heap} = ss >>> writeDPin i val >>> pushn [BCMT_STABLE, fromInt o toInt $ val] >>> \s -> return (sds, {s & heap = update {tt & task_type = BCStable1, node = NodeBCStable1 (fromInt o toInt $ val)} ptr heap})
rewrite` ptr _ sds {node = NodeBCReadA i} ss
#! (a_val, ss) = readAPin i ss
= pushn [BCMT_UNSTABLE, fromInt o toInt $ a_val] ss >>> \s -> return (sds, s)
rewrite` ptr _ sds tt=:{node = NodeBCWriteA i val} ss=:{heap} = ss >>> writeAPin i val >>> pushn [BCMT_STABLE, fromInt o toInt $ val] >>> \s -> return (sds, {s & heap = update {tt & task_type = BCStable1, node = NodeBCStable1 (fromInt o toInt $ val)} ptr heap})
rewrite` ptr bc sds tt=:{node = NodeBCRepeat oldtree tree} ss
#! tempsp = ss.sp
#! (tree, ss) = if (tree == BCMT_NULL) (taskTreeClone oldtree ptr ss) (tree, ss)
#! tt = {tt & node = NodeBCRepeat oldtree tree}
#! heap = update tt ptr ss.heap
#! ss = {ss & heap = heap}
= rewrite` tree bc sds (deref tree ss.heap) ss >>= \(sds, ss=:{stack, sp})
	#! top = (stack !$ sp)
	| top == BCMT_STABLE = memMarkTrashDeep tree ss >>> \ss=:{heap, stack}
		#! heap = update {tt & node = NodeBCRepeat oldtree BCMT_NULL} ptr heap
		#! stack = updateStack tempsp BCMT_NOVALUE stack
		#! ss = {ss & heap = heap, stack = stack}
		= return (sds, ss)
	| otherwise
		#! ss = {ss & stack = updateStack tempsp BCMT_NOVALUE stack}
		= return (sds, ss)
rewrite` ptr _ sds tt=:{node = NodeBCDelay _ d} ss=:{time} = push (if (time > d) BCMT_STABLE BCMT_UNSTABLE) ss >>> push (fromInt d) >>> \s -> return (sds, s)
// Eww, this is is ugly
rewrite` ptr bc sds tt=:{node = NodeBCTAnd lhs rhs} ss
#! (oldStack, _, ss) = resetStack ss
= rewrite` lhs bc sds (deref lhs ss.heap) ss >>= \(sds, ss=:{sp, stack, heap})
	#! width = sp
	#! [leftStab : leftVal] = take width stack
	#! (leftStack, _, ss) = resetStack ss
	= rewrite` rhs bc sds (deref rhs heap) ss >>= \(sds, ss=:{sp, stack})
		// Not really rightVal, but it does start with that.
		#! [rightStab : rightVal] = stack
		#! stab = if (leftStab < rightStab) leftStab rightStab
		#! stack = oldStack ++ [stab] ++ leftVal ++ rightVal
		= return (sds, {ss & stack = stack})
rewrite` ptr bc sds tt=:{node = NodeBCTOr lhs rhs} ss
#! (oldStack, _, ss) = resetStack ss
= rewrite` lhs bc sds (deref lhs ss.heap) ss >>= \(sds, ss=:{sp, stack, heap})
	#! leftWidth = sp
	#! [lhss : leftVal] = take leftWidth stack
	| lhss == BCMT_STABLE = memMarkTrashDeep ptr ss >>> memNodeMove ptr lhs >>> \s -> return (sds, s)
	| lhss == BCMT_NOVALUE = rewrite` rhs bc sds (deref rhs heap) {ss & sp = zero, stack = oldStack}
	#! (_, _, ss) = resetStack ss
	= rewrite` rhs bc sds (deref rhs heap) ss >>= \(sds, ss=:{stack, sp})
		// Not really rightVal, but it does start with that.
		#! [rhss : rightVal] = stack
		| rhss == BCMT_STABLE = memMarkTrashDeep lhs {ss & stack = oldStack ++ [rhss : rightVal] ++ (repeatn leftWidth zero)} >>> memNodeMove ptr rhs >>> \s -> return (sds, s)
		#! stack = oldStack ++ [lhs : leftVal] ++ [rhs : rightVal]
		| otherwise = return (sds, {ss & stack = stack})
rewrite` ptr bc sds tt=:{node = NodeBCStep lhs rhs lw} ss=:{sp}
// Save old SP
#! oldSP = ss.sp
#! ss = initStackMain ss
= rewrite` lhs bc sds (deref lhs ss.heap) ss >>= \(sds, ss=:{pc, sp})
	= interpret bc {ss & pc = toInt rhs, sp = sp + (toInt lw)} >>= \ss=:{stack}
		| stack !$ oldSP == BCMT_NULL = return $ done oldSP ss
		| otherwise = memMarkTrashDeep lhs ss >>> \s -> memNodeMove ptr (s.stack !$ oldSP) s >>> return o done oldSP
where
	done oldSP ss = {ss & sp = oldSP} >>> push BCMT_NOVALUE >>> \ss -> (sds, {ss & sp = ss.sp + (toInt lw)})
rewrite` ptr _ sds tt=:{node = NodeBCSdsGet d _} ss = push BCMT_UNSTABLE ss >>> pushStr (sds!$d).bcs_value >>> \s -> return (sds, s)
rewrite` ptr bc sds {node = NodeBCSdsSet offset _ data} ss=:{heap}
= rewrite` data bc sds (deref data heap) ss >>= \(sds, ss) -> return (sds, {ss & sp = inc ss.sp}) >>= \(sds, ss=:{stack,sp})
	#! tsds = sds!!(toInt offset)
	#! l = fromInt $ textSize tsds.bcs_value
	#! str = createString sp l stack
	#! sds = updateAt (toInt offset) {tsds & bcs_value = str} sds
	// TODO: Fix, task is not used
	#! task = ss.tasks!!ptr
	#! sp = sp + (toInt l)
	#! ss = {ss & sp = sp}
	#! ss=:{heap} = push data ss
	#! newNode = deref data heap
	#! heap = update newNode ptr heap
	#! heap = update {newNode & trash = True} data heap
	= return (sds, {ss & heap = heap})
rewrite` ptr _ sds {node = NodeBCDHTTemp i} ss = ss >>> pushn [BCMT_UNSTABLE, temp] >>> \s -> return (sds, s)
where
	temp = getDhtTemp ss i
rewrite` ptr _ sds {node = NodeBCDHTHumid i} ss = ss >>> pushn [BCMT_UNSTABLE, humid] >>> \s -> return (sds, s)
where
	humid = getDhtHumid ss i
rewrite` _ _ _ _ _ = abort "Rewrite.rewrite.rewrite`: TaskType and Node don't match"
