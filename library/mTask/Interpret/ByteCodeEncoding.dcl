definition module mTask.Interpret.ByteCodeEncoding

from StdMisc import undef
from Control.Applicative import class Applicative, class pure, class <*>, class <*, class *>, class Alternative
from Control.Monad import class Monad
from Data.Either import :: Either
from Data.Error import :: MaybeError
from Data.Functor import class Functor
from Data.GenCons import generic conses, generic consIndex
from Data.Maybe import :: Maybe
from iTasks.WF.Definition import :: TaskException

import mTask.Language
from mTask.Interpret.UInt import :: UInt8, :: UInt16
from mTask.Interpret.String255 import :: String255
from mTask.Interpret.ByteCodeEncoding._FBC import :: FBC

generic toByteCode a :: a -> String

derive toByteCode OBJECT, CONS of {gcd_index}, PAIR, EITHER, UNIT, RECORD, FIELD
derive toByteCode [], (), (,), (,,), TaskValue, Int, Bool, Char, Real, Long, Button, ButtonStatus, APin, DPin, String, UInt8, UInt16, String255

class toByteWidth a :: a -> Int
generic fromByteCode a :: FBC a

// Successful: (Right (Just a), String)
// Not enough bytes: (Right Nothing, String)
// Error: (Left String, String) (E.g. not existing constructor number)
runFBC :: (FBC a) -> ([Char] -> (Either String (Maybe a), [Char]))
instance Functor FBC
instance pure FBC
instance <*> FBC
instance *> FBC
instance <* FBC
instance Alternative FBC
instance Monad FBC

pure2 :: (Either String (Maybe a), [Char]) -> FBC a

instance toByteWidth (a,b) | toByteWidth a & toByteWidth b
instance toByteWidth (a,b,c) | toByteWidth a & toByteWidth b & toByteWidth c
instance toByteWidth (TaskValue a) | toByteWidth a
instance toByteWidth Int, Bool, Char, (), Real, Long, Button, ButtonStatus, APin, DPin, UInt8, UInt16

derive fromByteCode OBJECT, CONS of {gcd_index,gcd_name}, PAIR, EITHER, UNIT, RECORD, FIELD
derive fromByteCode [], (), (,), (,,), TaskValue, Int, Bool, Char, Real, Long, Button, ButtonStatus, APin, DPin, String, UInt8, UInt16, String255

convert_real_to_float_in_int :: !Real -> Int
convert_float_in_int_to_real :: !Int -> Real

fromBCADT :: FBC a | conses{|*|} a
fail :: String -> (FBC a)
peek :: FBC (Maybe Char)
skipNL :: FBC ()
top :: FBC Char
ntop :: Int -> FBC String

taskValByteWidth :: (b (TaskValue v)) -> UInt8 | toByteWidth v
byteWidthFun :: (a -> b) (v a) -> UInt8 | toByteWidth b
byteWidth :== byteWidthFun \x->x

parseSepMessages :: FBC [a] | fromByteCode{|*|} a

iTasksDecode :: String -> MaybeError TaskException a | type a
