implementation module mTask.Interpret.Message

import StdEnv
import iTasks => qualified >>=,forever,sequence,>>|,return
import Data.GenCons
import Data.Func
import Data.Functor
import Control.Applicative
import Control.Monad
import Control.GenBimap
import Data.Maybe
import Data.Either
import Data.Tuple
import mTask.Interpret.DSL
import mTask.Interpret.Device
import mTask.Interpret.ByteCodeEncoding
import mTask.Interpret.Specification
import Text => qualified join

derive toByteCode MTMessageFro, MTException

// MTTTask UInt8 UInt8 [BCPeripheral] [BCShareSpec] [BCInstr]
derive toByteCode MTMessageTo
toByteCode{|MTTTaskData|} d
	# allbc = concat (map toByteCode{|*|} d.mtttd_instructions)
	# allhw = concat (map toByteCode{|*|} d.mtttd_peripherals)
	=   toByteCode{|*|} d.mtttd_taskid
	+++ toByteCode{|*|} d.mtttd_returnwidth
	+++ toByteCode{|*|} (UInt16 (sharelength d.mtttd_shares))
	+++ toByteCode{|*|} (UInt8  (size allhw))
	+++ toByteCode{|*|} (UInt16 (size allbc))
	+++ toByteCode{|*|} d.mtttd_shares +++ allhw +++ allbc

sharelength = foldr (\s->(+) (3 + textSize s.bcs_value)) 0

derive fromByteCode MTMessageFro, MTMessageTo, MTException
fromByteCode{|MTTTaskData|}
	= fromByteCode{|*|} >>= \taskid->
		fromByteCode{|*|} >>= \returnwidth->
		fromByteCode{|*|} >>= \(UInt16 sdslen)->
		fromByteCode{|*|} >>= \(UInt8  hwlen)->
		fromByteCode{|*|} >>= \(UInt16 bclen)->
		sequence (repeatn sdslen top) >>= \sharebc->pure2 (runFBC fromByteCode{|*|} sharebc) >>= \shares->
		sequence (repeatn hwlen top)  >>= \hwbc->   pure2 (runFBC fromByteCode{|*|} hwbc)    >>= \hw->
		sequence (repeatn bclen top)  >>= \instrbc->pure2 (runFBC fromByteCode{|*|} instrbc) >>= \instr->
		pure
			{mtttd_taskid=taskid
			,mtttd_returnwidth=returnwidth
			,mtttd_peripherals=hw
			,mtttd_shares=shares
			,mtttd_instructions=instr}

derive class gCons MTMessageFro, MTMessageTo, BCPeripheral, Maybe, Pin, MTTTaskData, MTException

instance toString MTException where toString e = toSingleLineText e
