definition module mTask.Interpret.Compile

from StdOverloaded import class zero
import mTask.Interpret.DSL
import mTask.Language

:: CompileOpts =
	{ tailcallopt  :: Bool
	, labelresolve :: Bool
	, shorthands   :: Bool
	}

instance zero CompileOpts

compileOpts :: CompileOpts (Main (BCInterpret (TaskValue v))) -> (UInt8, [(Maybe MTLens, Task BCShareSpec)], [BCPeripheral], [BCInstr]) | type v

:: BCShareSpec =
	{ bcs_ident    :: UInt8
	//1 is yes, 0 is no
	, bcs_itasks   :: UInt8
	, bcs_value    :: String255
	}

derive toByteCode BCShareSpec
derive fromByteCode BCShareSpec
derive class gCons BCShareSpec
derive class iTask BCShareSpec
