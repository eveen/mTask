definition module mTask.Interpret.String255

from StdOverloaded import class +++, class %, class fromString, class ==
from iTasks.WF.Definition import class iTask
from iTasks.UI.Editor.Generic import generic gEditor, :: Editor
from iTasks.Internal.Generic.Visualization import generic gText, :: TextFormat
from Data.GenDefault import generic gDefault
from Data.GenEq import generic gEq
from Text.GenJSON import generic JSONEncode, generic JSONDecode, :: JSONNode
from Data.Maybe import :: Maybe
from Text import class Text

import Data.GenCons

:: String255 =: String255 String

instance +++ String255
instance % String255
instance fromString String255
instance toString String255
instance == String255
instance Text String255

derive class iTask String255
derive class gCons String255

safePrint :: String255 -> String
