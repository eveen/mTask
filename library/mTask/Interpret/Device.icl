implementation module mTask.Interpret.Device

import StdEnv

import Control.Applicative
import Data.Either
import Data.Func
import Data.Functor
import Data.List
import Data.Map => qualified union, difference, find, updateAt, get
import Data.Map.GenJSON
import Data.Tuple
import iTasks

import mTask.Interpret.ByteCodeEncoding
import mTask.Interpret.Message
import mTask.Interpret.DSL
import mTask.Interpret.Compile
import mTask.Interpret.Device.Serial
import mTask.Interpret.Device.TCP
import mTask.Interpret

derive class iTask MTDeviceData, MTaskStatus, MTException

:: Channels   :== ([MTMessageFro], [MTMessageTo], Bool)
:: MTDevice     = MTDevice
	//* device data
	(SimpleSDSLens MTDeviceData)
	//* sds update queue
	(SimpleSDSLens (Map UInt8 [(UInt8, String255)]))
	//* channels
	(SimpleSDSLens Channels)
:: MTDeviceData =
	{ deviceTasks :: Map UInt8 MTaskStatus
	, deviceSpec  :: Maybe MTDeviceSpec
	, deviceIds   :: [UInt8]
	}
:: MTaskStatus  = MTSInit | MTSPrepack | MTSAcked | MTSValue (TaskValue String255) | MTSException MTException

instance zero MTDeviceData where
	zero =
		{ deviceTasks = newMap
		, deviceSpec  = Nothing
		, deviceIds   = [zero..UInt8 255]
		}

sendMessage :: (MTMessageTo (SimpleSDSLens Channels) -> Task Channels)
sendMessage = upd o appSnd3 o flip (++) o pure

withDevice :: a (MTDevice -> Task b) -> Task b | iTask b & channelSync, iTask a
withDevice device devfun =
	withShared newMap \sdsupdates->
	withShared ([], [MTTSpecRequest], False) \channels->
	withShared zero \dev->
		parallel
			[(Embedded, \_->watch channels
				>>* [OnValue $ ifValue thd3 \_->throw MTEUnexpectedDisconnect])
			,(Embedded
				,   \stl->  appendTask Embedded (\_->catchAll
						(channelSync device channels <<@ NoUserInterface)
						(\e->throw (MTESyncException (String255 (e % (0, 255))))) @! Nothing) stl
				>>= \cstid->appendTask Embedded (\_->processChannels dev sdsupdates channels <<@ NoUserInterface @! Nothing) stl
				>>= \pctid->watch dev
				>>* [OnValue $ ifValue (\s->isJust s.deviceSpec)
						\_->appendTask Embedded (\_->Just <$> devfun (MTDevice dev sdsupdates channels)) stl]
				>>= \dftid->watch (sdsFocus (Right dftid) $ taskListItemValue stl)
				>>* [OnValue $ ifValue (\tv->tv =: (Value _ True))
						\_->upd (\(m, s, ss)->(m, s++[MTTShutdown], True)) channels
						>-| watch channels
						>>* [OnValue $ ifValue (\s->s=:(_, [], _)) \_->return Nothing]
			])] []
		@? \tv->case tv of
			NoValue = NoValue
			//At least one task was evaluated
			Value vs s = case [v\\(_, v=:(Value (Just _) _))<-vs] of
				[] = NoValue
				//The device function has a value
				[Value (Just a) s:_]
					//Its stability has to be postponed until after cleanup
					= Value a (s && length [()\\(_, Value _ True)<-vs] >= 2)
where
	processChannels :: (SimpleSDSLens MTDeviceData) (SimpleSDSLens (Map UInt8 [(UInt8, String255)])) (SimpleSDSLens Channels) -> Task [MTDeviceData]
	processChannels dev sdsupdates channels = forever
		$   watch channels
		>>* [OnValue $ ifValue (not o isEmpty o fst3)
		    $   \(r,s,ss)->upd (\(rr,s,ss)->(drop (length r) rr,s,ss)) channels
		    >>- \_->sequence (map process r)
		    ]
	where
		process :: MTMessageFro -> Task MTDeviceData
		process (MTFSpec c)
			= traceValue ("Received spec: " +++ toSingleLineText c)
			>>| upd (\s->{s & deviceSpec=Just c}) dev
		process (MTFTaskDelAck i)
			= traceValue ("Task " +++ toString i +++ " deleted")
			>>| upd (\s->{s & deviceIds=s.deviceIds ++ [i], deviceTasks=del i s.deviceTasks}) dev
		process (MTFTaskAck i)
			= traceValue ("Task acked: " +++ toSingleLineText i)
			>>| upd (\s->{s & deviceTasks=put i MTSAcked s.deviceTasks}) dev
		process (MTFTaskPrepAck i)
			= traceValue ("Taskprep acked: " +++ toSingleLineText i)
			>>| upd (\s->{s & deviceTasks=put i MTSPrepack s.deviceTasks}) dev
		process (MTFTaskReturn taskid mv)
			= traceValue ("Task " +++ toSingleLineText taskid +++ " returned: " +++ toSingleLineText (safePrint <$> mv))
				>>| upd (\s->{s & deviceTasks=put taskid (MTSValue mv) s.deviceTasks}) dev
		process (MTFSdsUpdate taskid sdsid value)
			= traceValue ("Received an update for sds " +++ toString sdsid +++ " from task " +++ toString taskid +++ " with value: " +++ safePrint value)
			>>- \_->upd (alter (fmap \l->l ++ [(sdsid, value)]) taskid) sdsupdates
			>>- \_->get dev
		process (MTFException exc)
			= traceValue ("Received an exception: ", toSingleLineText exc)
			>>= \_->case exc of
				//General error, just rethrow
				MTERTSError = throw MTERTSError
				//Task specific errors
				e = upd (\s->{s & deviceTasks=MTSException e <$ s.deviceTasks}) dev
		process (MTFDebug s)
			= traceValue ("MSG: " +++ toString s +++ "\n") >>- \_->get dev
		process x = traceValue ("Not implemented: " +++ toSingleLineText x)
			>>- \_->get dev

mTaskSafe :: (Task u) -> Task (Either MTException u) | type u
mTaskSafe t = try (Right <$> t) \e->case e of
	//General errors
	e=:MTERTSError = throw e
	//Task specific errors
	e = return (Left e)

liftmTask :: (Main (BCInterpret (TaskValue u))) MTDevice -> Task u | type u
liftmTask a b = liftmTaskWithOptions zero a b

liftmTaskWithOptions :: CompileOpts (Main (BCInterpret (TaskValue u))) MTDevice -> Task u | type u
liftmTaskWithOptions opts task (MTDevice dev sdsupdates channels)
	# (returnwidth, shares, hardware, instructions) = compileOpts opts task
	# (mayberefs, shares) = unzip shares
	# taskshare = mapReadWrite
		(\  s->s.deviceTasks
		,\t s->Just {s & deviceTasks=t}
		) Nothing dev
	=   get dev
	//Compile
	>>- \st=:{deviceIds=[sid:rest]}->upd (\s->{s & deviceTasks=put sid MTSInit s.deviceTasks, deviceIds=rest}) dev
	//Make sure the task is deleted even if we are destroyed (e.g. from a step)
	>>- \_->let taskView = sdsFocus sid $ mapLens "taskView" taskshare Nothing
		in  withCleanupHook (sendMessage (MTTTaskDel sid) channels)
		//Resolve shares
		$   sequence shares
		//Add task to sdsupdates map
		>>- \shares->upd (put sid []) sdsupdates
		//Send the prep
		>>- \_->sendMessage (MTTTaskPrep sid) channels
		//Wait for the prepack
		>>- \_->watch taskView
		>>* [OnValue $ ifValue (\t->t=:MTSPrepack || t =: MTSAcked || t =: (MTSValue _) || t =: (MTSException _))
			\_->sendMessage (MTTTask {mtttd_taskid=sid,mtttd_returnwidth=returnwidth,mtttd_peripherals=hardware,mtttd_shares=shares,mtttd_instructions=instructions}) channels]
		//Wait for task ack
		>>- \_->watch taskView
		>>* [OnValue $ ifValue (\t->t =: MTSAcked || t =: (MTSValue _) || t =: (MTSException _))
			\_->    waitForReturn taskView
				-|| allTasks [watchShareUpstream sid ish sh\\(Just ish)<-mayberefs & sh<-shares]
				-|| watchSharesDownstream mayberefs shares sid
		] @? \tv->case tv of
			NoValue = NoValue
			Value v _ = v
where
	//waitForReturn :: UInt8 -> Task u^
	waitForReturn taskView  = watch $ flip mapReadError taskView \s->case s of
			MTSException e = Error (exception e)
			MTSValue NoValue = Ok NoValue
			MTSValue (Value a s) = case iTasksDecode (toString a) of
				Error e = Error e
				Ok v = Ok (Value v s)
			_ = Ok NoValue

	//If a share changes from the server
	watchShareUpstream :: UInt8 MTLens BCShareSpec -> Task ()
	watchShareUpstream taskid ref share = watch ref
		>>* [OnValue $ ifValue ((<>)share.bcs_value) $ const
			$ traceValue "Watching the upstream share" >>| whileUnchanged ref
			\v->sendMessage (MTTSdsUpdate taskid share.bcs_ident v) channels
				@? const NoValue
			]

	//If a share changes from the client
	watchSharesDownstream :: [Maybe MTLens] [BCShareSpec] UInt8 -> Task ()
	watchSharesDownstream mayberefs shares taskid
		# myupdates = sdsFocus taskid $ mapLens "sdsupdates" sdsupdates (Just [])
		= forever
		$   watch myupdates
		>>* [OnValue $ ifValue (not o isEmpty)
			$   \shup->upd (drop $ length shup) myupdates
			>>- \_   ->sequence (map (uncurry applyDownstreamUpdate) shup)
			@! ()]
	where
		applyDownstreamUpdate :: UInt8 String255 -> Task ()
		applyDownstreamUpdate sdsid value = case find (\(_, sh)->sh.bcs_ident==sdsid) (zip2 mayberefs shares) of
			Nothing = traceValue "Huh? I got a share update for an unknown share" @! ()
			Just (Nothing, _) = traceValue "Huh? I got a share update for a non iTasks share" @! ()
			Just (Just ref, _) = traceValue "Found the sds, updating" >>- \_->upd (const value) ref @! ()

viewDevice :: MTDevice -> Task ()
viewDevice (MTDevice dev sdsupdates channels) = allTasks
	[ viewSharedInformation [ViewAs $ toList] sdsupdates
		<<@ Title "Sdsupdates" @! ()
	, viewSharedInformation [] channels
		<<@ Title "Communication Channels" @! ()
	, viewSharedInformation [ViewAs $ toData`] dev
		<<@ Title "Device Data" @! ()
	] <<@ ArrangeWithTabs False @? const NoValue
where
	toData` d = {deviceTasks`=toList d.deviceTasks, deviceSpec`=d.deviceSpec}//, deviceIds`=d.deviceIds}

:: MTDeviceData` =
	{ deviceTasks` :: [(UInt8, MTaskStatus)]
	, deviceSpec`  :: Maybe MTDeviceSpec
//	, deviceIds`   :: [UInt8]
	}

derive class iTask MTMessageTo, MTMessageFro, BCPeripheral, Pin, MTD, MTTTaskData, MTDeviceData`

:: MTD = TCP TCPSettings | Serial TTYSettings
instance channelSync MTD where
	channelSync (TCP t) ch = channelSync t ch
	channelSync (Serial t) ch = channelSync t ch
instance zero MTD where zero = TCP {TCPSettings|port=8123,host="localhost"}

enterDevice :: Task MTD
enterDevice
	=   tune (Title "Enter Device")
	$   tune ArrangeHorizontal
	$   updateChoice [ChooseFromList fst] presets (hd presets) <<@ Title "Select a preset" 
	>&> flip whileUnchanged (tune (Title "Update the preset") o updateInformation []) o mapRead (maybe zero snd)
where
	presets =
		[("Demo device Windows Bluetooth (HC-06)", Serial {zero & devicePath="COM5", baudrate=B9600})
		,("Demo device Linux Bluetooth (HC-06)", Serial {zero & devicePath="/dev/rfcomm0", baudrate=B9600})
		,("Arduino UNO", Serial {zero & devicePath="/dev/ttyACM0", baudrate=B38400})
		,("Chinese UNO", Serial {zero & devicePath="/dev/ttyUSB0", baudrate=B38400})
		,("Arduino UNO with Bluetooth (Itead)", Serial {zero & devicePath="/dev/rfcomm", baudrate=B38400})
		,("WEMOS LOLIN D1 Mini", TCP {TCPSettings | host="192.168.1.1xx", port=8123})
		,("Local client", TCP {TCPSettings | host="localhost", port=8123})
		,("Generic TCP client", TCP {TCPSettings | host="", port=8123})
		]
