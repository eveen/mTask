implementation module mTask.Interpret.Instructions

import Data.GenCons
import Data.Functor
import Control.Applicative
import Control.Monad => qualified join
import Control.GenBimap
import mTask.Interpret.UInt
import iTasks => qualified return, >>=, >>|, forever, sequence
import StdEnv
import Text
import Text.HTML

import Data.Func
import mTask.Interpret.ByteCodeEncoding
import mTask.Interpret.String255

derive class gCons BCInstr, BCTaskType, JumpLabel
derive class iTask BCInstr, BCTaskType, JumpLabel
derive toByteCode BCTaskType, BCInstr, JumpLabel

instance < JumpLabel where < (JL i) (JL j) = i < j

derive fromByteCode JumpLabel, BCTaskType, BCInstr

bytewidth :: BCInstr -> Int
bytewidth i = size $ toByteCode{|*|} i

debugInstructions :: [BCInstr] -> [(String, String, [String])]
debugInstructions d = pprint 0 d
where
	pprint :: Int [BCInstr] -> [(String, String, [String])]
	pprint i [] = []
	pprint i [BCLabel l:is] = [("","",[]),("  ","BCLabel", [toString l]):pprint i is]
	pprint i [ins:is] =
		[let (c,a) = printInstr ins in (toString i +++ ": ", c,a)
		:pprint (i+bytewidth ins) is]

formatDebugInstructions :: [(String, String, [String])] -> HtmlTag
formatDebugInstructions is
	= TableTag []
		[ TbodyTag []
			[TrTag []
				[ TdTag  [] [Text n]
				, TdTag  [] [PreTag [] [Text i]]
				: [TdTag [] [PreTag [] [Text a]]\\a<-args]
				]
			\\(n, i, args)<-is
			]
		]

instance toString JumpLabel where toString (JL i) = toString i
generic gPrintInstr a :: a [String] -> [String]
derive gPrintInstr BCInstr, UInt8, UInt16, JumpLabel, BCTaskType
gPrintInstr{|OBJECT|} f (OBJECT a) c = f a c
gPrintInstr{|EITHER|} f _ (LEFT a) c = f a c
gPrintInstr{|EITHER|} _ f (RIGHT a) c = f a c
gPrintInstr{|FIELD|} f (FIELD a) c = f a c
gPrintInstr{|RECORD|} f (RECORD a) c = f a c
gPrintInstr{|UNIT|} _ c = c
gPrintInstr{|CONS of {gcd_name}|} f (CONS a) c = [gcd_name:f a c]
gPrintInstr{|PAIR|} l r (PAIR x y) c = l x (r y c)
gPrintInstr{|Int|} i c = [toString i:c]
gPrintInstr{|Bool|} i c = [toString i:c]
gPrintInstr{|String|} i c = [i:c]
gPrintInstr{|String255|} (String255 i) c = [join " " [toString (toInt i)\\i<-:i]:c]

printInstr :: BCInstr -> (String, [String])
printInstr c
	# [c:as] = gPrintInstr{|*|} c []
	= (c, as)
