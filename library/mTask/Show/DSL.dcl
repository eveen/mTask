definition module mTask.Show.DSL

/*
	Pieter Koopman
	Radboud University NIjmegen, The Netherlands
	pieter@cs.ru.nl
*/

import mTask.Show.basic
import mTask.Language

instance arith Show
instance aio Show
instance dio APin Show
instance dio DPin Show
instance step Show
instance rtrn Show
instance unstable Show
instance rpeat Show
instance delay Show
instance dht Show
instance LEDMatrix Show
instance lcd Show
instance i2cbutton Show
instance sds Show
instance liftsds Show
instance cond Show
instance tupl Show
instance int Int Show
instance int Real Show
instance double Int Show
instance double Real Show
instance fun () Show
instance fun (Show a) Show | type a
instance fun (Show a, Show b) Show | type a & type b
instance fun (Show a, Show b, Show c) Show | type a & type b & type c
instance .||. Show
instance .&&. Show
