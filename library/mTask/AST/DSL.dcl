definition module mTask.AST.DSL

/*
	Pieter Koopman
	Radboud University NIjmegen, The Netherlands
	pieter@cs.ru.nl
*/

import mTask.Language

instance aio AST
instance arith AST
instance cond AST
instance delay AST
instance dht AST
instance dio p AST
instance double Int AST
instance double Real AST
instance rpeat AST
instance fun () AST
instance fun (AST a) AST | basicType a
instance fun (AST a, AST b) AST | basicType a & basicType b
instance fun (AST a, AST b, AST c) AST | basicType a & basicType b & basicType c
instance lcd AST
instance rtrn AST
instance sds AST
instance step AST
instance unstable AST
instance .&&. AST
instance .||. AST

instance typeOf Button
instance typeOf DHT
instance value DHT
instance typeOf DHTtype
instance value DHTtype
instance typeOf LCD
instance value LCD
instance typeOf ButtonStatus
instance value ButtonStatus

