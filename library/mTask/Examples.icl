implementation module mTask.Examples

import StdEnv, Data.Func
import mTask.Interpret
import mTask.Language

derive class dyn Long

pRet :: Int -> Main (MTask v Int) | mtask v
pRet i = {main=rtrn (lit i)}

pLRet :: Long -> Main (MTask v Long) | mtask v
pLRet i = {main=rtrn (lit i)}

pRRet :: Real -> Main (MTask v Real) | mtask v
pRRet i = {main=rtrn (lit i)}

pUns :: Int -> Main (MTask v Int) | mtask v
pUns i = {main=unstable (lit i)}

pRetTup :: (Int, Int) -> Main (MTask v (Int, Int)) | mtask v
pRetTup (i, j) = {main=rtrn (lit (i, j))}

pTupBind :: (Int, Int) -> Main (MTask v (Int, Int)) | mtask v
pTupBind (i, j) = {main=rtrn (lit (i, j)) >>=. rtrn}

pTupBindFst :: (Int, Int) -> Main (MTask v Int) | mtask v
pTupBindFst t = {main=rtrn (lit t) >>=. rtrn o first}

pTupBindSnd :: (Int, Int) -> Main (MTask v Int) | mtask v
pTupBindSnd t = {main=rtrn (lit t) >>=. tupopen (rtrn o snd)}

pRetTupFst :: (Int, Int) -> Main (MTask v Int) | mtask v
pRetTupFst (i, j) = {main=rtrn (first (lit (i, j)))}

pRetTupSnd :: (Int, Int) -> Main (MTask v Int) | mtask v
pRetTupSnd (i, j) = {main=rtrn (second (lit (i, j)))}

pFun0 :: Int -> Main (MTask v Int) | mtask v
pFun0 i = fun \fourtytwo=(\()->lit i) In
	{main=rtrn (fourtytwo ())}

pFun1 :: Int -> Main (MTask v Int) | mtask v
pFun1 i = fun \inc=(\i->i +. lit 1) In
	{main=rtrn (inc (lit i))}

pFun2 :: (Int, Int) -> Main (MTask v Int) | mtask v
pFun2 (i, j) = fun \fst=f In
	fun \snd=uncurry (flip (curry f)) In
	{main=rtrn (fst (lit i, lit j))}
where
	f :: (v Int, v Int) -> v Int
	f (a, b) = a

pFun3a :: (Int, Int, Bool) -> Main (MTask v Int) | mtask v
pFun3a (a, b, c) = fun \fst=f In
	{main=rtrn (fst (lit a, lit b, lit c))}
where
	f :: (v Int, v Int, v Bool) -> v Int
	f (a, b, c) = a

pFun3b :: (Int, Int, Bool) -> Main (MTask v Int) | mtask v
pFun3b (a, b, c) = fun \fst=f In
	{main=rtrn (fst (lit a, lit b, lit c))}
where
	f :: (v Int, v Int, v Bool) -> v Int
	f (a, b, c) = b

pFun3c :: (Int, Int, Bool) -> Main (MTask v Bool) | mtask v
pFun3c (a, b, c) = fun \fst=f In
	{main=rtrn (fst (lit a, lit b, lit c))}
where
	f :: (v Int, v Int, v Bool) -> v Bool
	f (a, b, c) = c

pFun4a :: Main (MTask v Int) | mtask v
pFun4a = fun \fst=f In
	{main=rtrn (fst (lit (Long 123456), lit 44))}
where
	f :: (v Long, v Int) -> v Int
	f (_, c) = c

pFun4b :: Main (MTask v Long) | mtask v
pFun4b = fun \f=fn In
	{main=rtrn (f (lit (Long 123456), lit 44))}
where
	fn :: ((v Long, v Int) -> v Long) | mtask v
	fn = fst

pFun5 :: (Int, Int) -> Main (MTask v (Int, Int)) | mtask v
pFun5 (a,b) = fun \swap=f In
	{main=rtrn (swap (lit (a,b)))}
where
	f :: (v (Int, Int)) -> v (Int, Int) | mtask v
	f v = tupl (second v) (first v)

//
//pFun4c :: Main (MTask v Int) | mtask v
//pFun4c = fun \fst=f In
//	{main=rtrn (fst (lit 42, (lit 42, lit 43)))}
//where
//	f :: (v Int, (v Int, v Int)) -> v Int | mtask v
//	f (a, (b, c)) = a
//
//pFun4d :: Main (MTask v (Int, Int)) | mtask v
//pFun4d = fun \fst=f In
//	{main=rtrn (fst (lit 42, (lit 42, lit 43)))}
//where
//	f :: (v Int, (v Int, v Int)) -> v (Int, Int) | mtask v
//	f (a, (b, c)) = tupl b c

pFac :: Int -> Main (MTask v Int) | mtask v
pFac i = fun \fac=(\i=If (i ==. lit zero) (lit one) (i *. fac (i -. lit one))) In
	{main=rtrn (fac (lit i))}

pFacL :: Long -> Main (MTask v Long) | mtask v
pFacL i = fun \fac=(\i=If (i ==. lit zero) (lit one) (i *. fac (i -. lit one))) In
	{main=rtrn (fac (lit i))}

pFacTl :: Int -> Main (MTask v Int) | mtask v
pFacTl i =
	fun \facacc=(\(n,a)->If (n ==. lit zero) a (facacc (n -. lit one, n*.a))) In
	fun \fac=(\i=facacc (i, lit one)) In
	{main=rtrn (fac (lit i))}

pFacLTl :: Long -> Main (MTask v Long) | mtask v
pFacLTl i =
	fun \facacc=(\(n,a)->If (n ==. lit one) a (facacc (n -. lit one, n*.a))) In
	fun \fac=(\i=facacc (i, lit one)) In
	{main=rtrn (fac (lit i))}

pTail :: Int -> Main (MTask v Int) | mtask v
pTail i =
	fun \countdown=(\n->If (n ==. lit zero) n (countdown (n -. lit one))) In
	{main=rtrn (countdown (lit i))}

//Impossible
//pMut :: Main (MTask v Int) | mtask v
//pMut =
//	fun \flip=(\()->flop ()) In
//	fun \flop=(\()->flip ()) In
//	{main=rtrn (flop ())}
//Not implemented
pMut :: Main (MTask v ()) | mtask v
pMut = funs \(flip,flop)->
		(\()->flop ()
		,\()->flip ()
		) In {main=rtrn (flip ())}

pArith :: Main (MTask v Bool) | mtask v
pArith = {main=
		rtrn $ ((lit 5 +. lit 3 /. lit 2) ==. (lit 6 -. lit 7 *. lit 8)) |. lit True
	}

pReadA :: APin -> Main (MTask v Int) | mtask v
pReadA p = {main=readA (lit p)}

pDelayRet :: Int -> Main (MTask v Int) | mtask v
pDelayRet i = {main=delay (lit 5000) >>|. rtrn (lit i)}

pReadAOnce :: APin -> Main (MTask v Int) | mtask v
pReadAOnce p = {main=readA (lit p) >>~. rtrn}

pReadATwice :: APin -> Main (MTask v Int) | mtask v
pReadATwice p = {main=readA (lit p) >>.. readA (lit p) >>~. rtrn}

pWriteD :: (DPin, Bool) -> Main (MTask v Bool) | mtask v
pWriteD (p, v) = {main=writeD (lit p) (lit v)}

pAio :: Main (MTask v Int) | mtask v
pAio = {main=readA a0 >>~. \v->writeA a0 (v +. lit 1)}

pEver :: Main (MTask v Int) | mtask v
pEver = {main=rpeat (rtrn (lit 42))}

pDio :: Main (MTask v Int) | mtask v
pDio = {main=readA a0 >>~. \v->writeA a0 (v +. lit 1)}

pStep1 :: Main (MTask v Int) | mtask v
pStep1 = {main=
	rtrn (lit 42) >>=. rtrn
	}

pStep2 :: Int -> Main (MTask v Int) | mtask v
pStep2 i = {main=
	rtrn (lit i) >>*.
		[IfValue (\i->i ==. lit 42) rtrn
		,IfValue (\_->lit True) (\i->rtrn (i +. lit 5))]
	}

pStep3 :: Main (MTask v Int) | mtask v
pStep3 = {main=
	rtrn (lit 42) >>*.
		[IfUnstable (\_->lit True) rtrn]
	}

pRStep1 :: Real -> Main (MTask v Real) | mtask v
pRStep1 r = {main=
	rtrn (lit r) >>=. rtrn
	}

pTuple3 :: Main (MTask v (Int, (Int, Int))) | mtask v
pTuple3 = {main=rtrn (lit 1) .&&. rtrn (lit 2) .&&. rtrn (lit 3)}

pOr1 :: (Int, Int) -> Main (MTask v Int) | mtask v
pOr1 (a, b) = {main=rtrn (lit a) .||. unstable (lit b)}

pOr2 :: (Int, Int) -> Main (MTask v Int) | mtask v
pOr2 (a, b) = {main=unstable (lit a) .||. rtrn (lit b)}

pAnd :: (Int, Int) -> Main (MTask v (Int, Int)) | mtask v
pAnd (a, b) = {main=rtrn (lit a) .&&. rtrn (lit b)}

pHugetup :: (Int, Int, Int, Int, Int) -> Main (MTask v (Int, (Int, (Int, (Int, Int))))) | mtask v
pHugetup (a, b, c, d, e)= {main=rtrn (lit a) .&&. rtrn (lit b) .&&. rtrn (lit c) .&&. rtrn (lit d) .&&. rtrn (lit e)}

pHuge :: (Long, Long, Long) -> Main (MTask v (Long, Long, Long)) | mtask v
pHuge (a, b, c) = {main=rtrn (lit (a, b, c))}

pPlus :: (a, a) -> Main (MTask v a) | mtask v
pPlus (i, j) = {main=rtrn (lit i +. lit j)}

pIPlus :: ((Int, Int) -> Main (MTask v Int)) | mtask v
pIPlus = pPlus

pLPlus :: ((Long, Long) -> Main (MTask v Long)) | mtask v
pLPlus = pPlus

pRPlus :: ((Real, Real) -> Main (MTask v Real)) | mtask v
pRPlus = pPlus

pEq :: (Int, Int) -> Main (MTask v Bool) | mtask v
pEq (a, b) = {main=rtrn (lit a ==. lit b)}

pLEq :: (Long, Long) -> Main (MTask v Bool) | mtask v
pLEq (a, b) = {main=rtrn (lit a ==. lit b)}

pREq :: (Real, Real) -> Main (MTask v Bool) | mtask v
pREq (a, b) = {main=rtrn (lit a ==. lit b)}

pLLe :: (Long, Long) -> Main (MTask v Bool) | mtask v
pLLe (a, b) = {main=rtrn (lit a <. lit b)}

pRLe :: (Real, Real) -> Main (MTask v Bool) | mtask v
pRLe (a, b) = {main=rtrn (lit a <. lit b)}

pSds1 :: Main (MTask v Int) | mtask v
pSds1 = sds \l=42 In {main=rtrn (lit 42)}

pSds2 :: Main (MTask v Int) | mtask v
pSds2 = sds \l=42 In {main=getSds l >>~. rtrn}

pSds3 :: Main (MTask v Int) | mtask v
pSds3 = sds \l=42 In {main=setSds l (lit 43)}

pSds4 :: Main (MTask v (Long, (Long, Long))) | mtask v
pSds4 = sds \l=(Long 0, (Long 1, Long 2)) In {main=setSds l (lit (Long 10, (Long 11, Long 12)))}

pSds5 :: Main (MTask v (Int, Int)) | mtask v
pSds5 = sds \l=42 In {main=setSds l (lit 43) .&&. delay (lit 5000)}

pLiftSds :: (Shared Int) -> Main (MTask v Int) | mtask v
pLiftSds sh = liftsds \l=sh In {main=getSds l}

pLiftSds1 :: (Shared Int) -> Main (MTask v Int) | mtask v
pLiftSds1 sh = liftsds \l=sh In {main=setSds l (lit 43) >>|. getSds l}

pBlink :: Int -> Main (MTask v Bool) | mtask v
pBlink i = {main =
		rpeat (
			delay (lit i) >>|.
			readD d13 >>~. \state->
			writeD d13 (Not state)
		)
	}

//pBlinkRec :: Int -> Main (MTask v Bool) | mtask v
//pBlinkRec i =
//	fun \blink=(\st->writeD d13 st >>|. delay (lit i) >>|. blink (Not st))
//	In {main = blink true}

pDHT :: (DPin, DHTtype) -> Main (MTask v (Int, Int)) | mtask, dht v
pDHT (p, t) = DHT p t \dht->{main=temperature dht .&&. humidity dht}

//Excercises
pFib :: Int -> Main (MTask v Int) | mtask v
pFib i = fun \fib=(\i->
		If (i ==. lit 0)
			(lit 0)
			(If (i ==. lit 1)
				(lit 1)
				(fib (i -. lit 2) +. fib (i -. lit 1))
			)
	)
	In {main = rtrn (fib (lit i))}

pAPin42 :: Main (MTask v Int) | mtask v
pAPin42 = {main=readA (lit A0) >>*. [IfValue (\i->i >. lit 42) (\v->rtrn v)]}

pSum :: (APin, APin) -> Main (MTask v Int) | mtask v
pSum (a1,a2) = {main=readA (lit a1) .&&. readA (lit a2) >>~. \t->rtrn (first t +. second t)}

pSDSBlink :: (Shared Bool) -> Main (MTask v Bool) | mtask v & liftsds v
pSDSBlink sh = liftsds \bs=sh
	In {main=rpeat (getSds bs >>~. \v->writeD (lit D13) v)}
