#!/bin/sh
rm -rf arduino
mkdir -p arduino
curl -SsL https://downloads.arduino.cc/arduino-1.8.9-linux64.tar.xz \
	| tar -xJC arduino --strip-components=1
for f in dependencies/*.zip
do
	unzip -d arduino/libraries "$f"
done
mkdir -p arduino/hardware/esp8266
git clone --recursive https://github.com/esp8266/Arduino.git \
	arduino/hardware/esp8266/esp8266
( cd arduino/hardware/esp8266/esp8266/tools/; python get.py; )
