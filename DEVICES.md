# Device Notes

## linux/windows/macos client

### Prebuilt release
[windows](https://gitlab.science.ru.nl/mlubbers/mTask/builds/artifacts/master/file/client/client.exe?job=windows-client)
[linux](https://gitlab.science.ru.nl/mlubbers/mTask/builds/artifacts/master/file/client/client?job=linux-client)

### Manual compilation

To compile the client for regular operating systems run

    make -C client

To cross compile the windows client from linux install mingw and run

    make DETECTED_OS=Windows CC=x86_64-w64-mingw32-gcc CFLAGS=-m64 -BC dependencies/CleanSerial/src

The makefile either creates a `client` or a `client.exe` that can be executed.
Run `client --help` for information on the command line arguments.

## Arduino compatibles

Currently supported:

- Arduino UNO
- LOLIN D1 mini
- NodeMCUv2

### Automatic

Download and prepare the arduino IDE

    bash install_arduino.sh

Run the IDE and open the sketch

    ./arduino/arduino client/client.ino

After setting the correct board details, you can either upload or verify.

### Manual

- Download and install the latest Arduino IDE and set it up to work with your device (e.g. by adding the board to the board manager).

- Install the required dependencies as ZIP libraries from `dependencies`

### Wifi

If you want to use wifi, make sure to copy `client/wifi.def.h` to `client.wifi.h` and add the required wifi connection details.

## Customize the firmware

The firmware can be customized using preprocessor macros.

`pc.h` contains the linux/windows/macos specific configuration

`arduino.h` contains the Arduino compatible specific configuration

### Customization options

#### General options

- MEMSIZE

  The number of bytes to reserve for the mTask memory
- APINS

  The number of available analog pins
- DPINS

  The number of available digital pins
- LOGLEVEL

  The loglevel (0=silent, 1=info, 2=debug)
- SC(s)

  The method for storing strings (e.g. to store them in progmem)
- REQUIRE\_ALIGNED\_MEMORY\_ACCESS

  When defined, all memory will be aligned (some architectures crash when accessing misaligned data)

- `HAVE_LEDMATRIX`

  16x16 LED Matrix is connected
- `HAVE_OLEDSHIELD`

  OLED shield is connected (_TODO_, currently only used for runtime information)
- `HAVE_DHT`

  Have a digital humidity and temperature sensor.

#### PC

- CURSES\_INTERFACE

  Use the experimental curses frontend (requires libncurses)

#### Arduino

- BAUDRATE

  Use a different baudrate for the serial connection

### Notes on Arduino compatibles

#### UNO compatibles
It is advised to increase the serial buffer a bit since the loop can take relatively long.
This is done by increasing the `SERIAL_RX_BUFFER_SIZE` in `hardware/arduino/avr/cores/arduino/HardwareSerial.cpp`.
Default baudrate is 9600.

#### ESP8266 boards
The ESP8266 requires aligned LOAD/STORE instructions and will crash otherwise.
To achieve this, make sure to define `REQUIRE_ALIGNED_MEMORY_ACCESS`

#### Bluetooth

##### Bluetooth shield v2.2 (Itead studio)
The Bluetooth Shield v2.2 (Itead studio) operates on a baudrate of 38400 so the sketch also requires this.
For operation the tx of the shield needs to be connected to the rx of the arduino and vice versa.

If you want to program the arduino, remove the shield or the tx/rx jumpers.

##### Connect bluetooth on linux
Connect and pair:

```
$ bluetoothctl
[bluetooth]# scan on
...
[bluetooth]# pair 00:00:00:00:00:00
...
[bluetooth]# info 00:00:00:00:00:00
Name: ...
Alias: ...
...
UUID: Serial Port (...)
```

Setup the rfcomm tty

```
$ rfcomm release all # optional
$ rfcomm bind hci0 00:12:12:12:08:30 1
$ ls -l /dev/rfcomm*
crw-rw---- 1 root dialout 216, 0 Oct 31 09:44 /dev/rfcomm0
```

And you are ready to go by selecting /dev/rfcomm0 as a device.
The initial message often takes a little longer than expected.

##### Connect bluetooth on windows

- Click on the bluetooth icon
- Click on add new bluetooth or other device
- Click on bluetooth
- Select the arduino device and enter the pin
- In the bluetooth options click more bluetooth options
- Click on the COM ports tab and there you see an outgoing connection for the device.
  If this is not the case, add it. That outgoing connection is the address you need to enter in the programs.
  e.g. COM4

### Libraries

All library zip files are in `dependencies`.

- `Adafruit-GFX-Library-master.zip`

  Addition graphics routines for the OLED shield
- `Adafruit_SSD1306-esp8266-64x48.zip`

  Patched SDD library for the LOLIN D1 mini
- `dhtlib.zip`

  Required for DHT
- `WEMOS_Matrix_LED_Shield_Arduino_Library-master.zip`

  Required for the LED shield
- `WEMOS_SHT3x_Arduino_Library-master.zip`

  Required for the SHT shield of the LOLIN D1 mini
