# mTask

## Introduction
The *mTask* framework is an ecosystem for programming entire IoT systems from a single source.
The framework is powered by a multi-backend tagless embedded DSL embedded in the pure functional programming language [Clean][clean].
With the bytecode generation backend and the iTasks integration mTask tasks can be embedded in [iTasks][itasks] programs and shared data sources can be synchronized.
The client runtime system runs on embedded devices such as Arduino/ESPxxx/Nucleo arm boards but also on full fledged linux, windows and mac machines.

## Server

### Prebuilt (server) release

This includes a clean distribution

[win32](https://gitlab.science.ru.nl/mlubbers/mTask/builds/artifacts/master/file/mtask-windows-x86.zip?job=windows-x86-server)
[win64](https://gitlab.science.ru.nl/mlubbers/mTask/builds/artifacts/master/file/mtask-windows-x64.zip?job=windows-x64-server)
[linux](https://gitlab.science.ru.nl/mlubbers/mTask/builds/artifacts/master/file/mtask-linux-x64.tar.gz?job=linux-x64-server)

### Installation

It is easier to fetch a prebuilt release.
However, if you want to set things up by hand, to for example use the git
version directly, see [INSTALL.md](INSTALL.md).

## Client
The prebuilt releases contain the linux/windows machine client.
To create an embedded client for yourself, see [DEVICES.md](DEVICES.md).

## Project ideas

- [ ] Exceptions
- [ ] Rewrite only when there are events
- [x] Fixed length updateable data types (e.g. arrays) (in progress)
- [ ] Interrupts
- [ ] SDS Lenses
- [ ] Multiple task instances for the same task
- [ ] Buttons in step
- [ ] Device to device communication
- [ ] Peripherals
    - [ ] Generalized interface for peripherals
    - [ ] Screen support
    - [ ] PWM support
    - [ ] ...
- [ ] ...

## Publications

- M. Lubbers, P. Koopman, and R. Plasmeijer, “Interpreting Task Oriented Programs on Tiny Computers,” in Proceedings of the 31th Symposium on the Implementation and Application of Functional Programming Languages, Singapore, 2019, in-press.
- M. Lubbers, P. Koopman, and R. Plasmeijer, “Multitasking on Microcontrollers using Task Oriented Programming,” in 2019 42nd International Convention on Information and Communication Technology, Electronics and Microelectronics (MIPRO), Opatija, Croatia, 2019, pp. 1587–1592.
- M. Lubbers, P. Koopman, and R. Plasmeijer, “Task Oriented Programming and the Internet of Things,” in Proceedings of the 30th Symposium on the Implementation and Application of Functional Programming Languages, Lowell, MA, 2018, pp. 83–94.
- M. Amazonas Cabral De Andrade, “Developing Real Life, Task Oriented Applications for the Internet of Things,” Master’s Thesis, Radboud University, Nijmegen, 2018.
- R. Plasmeijer and P. Koopman, “A Shallow Embedded Type Safe Extendable DSL for the Arduino,” in Trends in Functional Programming, vol. 9547, Cham: Springer International Publishing, 2016.
- M. Lubbers, “Task Oriented Programming and the Internet of Things,” Master’s Thesis, Radboud University, Nijmegen, 2017.
- P. Koopman, M. Lubbers, and R. Plasmeijer, “A Task-Based DSL for Microcomputers,” 2018, pp. 1–11.

[clean]: clean.cs.ru.nl
[itasks]: clean.cs.ru.nl/ITasks
