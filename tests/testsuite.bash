#!/bin/bash
set -e
startdate=$(date)
../client/client &
./TestSuite
kill %1
echo "started at $startdate"
echo "ended at $(date)"
