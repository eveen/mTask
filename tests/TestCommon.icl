implementation module TestCommon

import Gast.Testable
import StdEnv
import Data.Either
import mTask.Interpret.Message
import mTask.Interpret.UInt
import mTask.Language
//
import Gast => qualified <., <=., >., >=.
//
//import Text

derive class Gast Either, Maybe, Button, MTDeviceSpec, MTMessageFro, MTMessageTo, BCInstr, BCShareSpec, BCTaskType, Pin, BCPeripheral, APin, DPin, DHTtype, TaskValue, JumpLabel, MTTTaskData, MTException
derive gPrint Long, UInt8, UInt16, String255
derive genShow Long, UInt8, UInt16, String255

ggen{|Long|}   s = [LONG_MAX, LONG_MIN, zero, one, ~one:[zero..dec LONG_MAX]]
ggen{|UInt8|}  s = [UINT8_MAX, UINT8_MIN:[zero..dec UINT8_MAX]]
ggen{|UInt16|} s = [UINT16_MAX, UINT16_MIN:[zero..dec UINT16_MAX]]
ggen{|String255|} s = map fromString (ggenString 255 1.0 0 255 aStream)

int16s :: [Int]
int16s = [-1*0x7fff,0x7fff,0,1,-1:[-1*0x7fff+1..0x7fff-1]]

realeq :: Real Real -> Bool
realeq a b
	| a == b = True
	= abs (a-b) <= max (abs a) (abs b) * eps

realprop :: Real Real -> Property
realprop x y = check realeq x y
