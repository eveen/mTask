module TestByteCodeEncoding

import StdEnv

import Data.GenDefault

import Data.Func
import Data.Either
import Data.Maybe
import Control.GenBimap

import mTask.Interpret.Device
import mTask.Interpret.Specification
import mTask.Interpret.DSL
import mTask.Interpret.ByteCodeEncoding
import mTask.Interpret.Message
import mTask.Language

import Gast => qualified <., >., <=., >=.
import Gast.CommandLine

import TestCommon
import Text

//derive class Gast MTMessageTo, MTMessageFro, BCPeripheral, Pin
//derive gEq MTMessageTo, MTMessageFro, BCPeripheral, Pin
derive gEq BCPeripheral, Pin

instance == MTDeviceSpec where (==) x y = x === y
instance == BCShareSpec where (==) x y = x === y
instance == Pin where (==) x y = x === y
instance == MTMessageTo where (==) x y = x === y
instance == MTMessageFro where (==) x y = x === y
instance == (TaskValue a) | gEq{|*|} a where (==) x y = x === y
instance == BCTaskType where (==) x y = x === y
instance == BCPeripheral where (==) x y = x === y

testEncoding` :: (a a -> Property) a -> Property | Gast, fromByteCode{|*|}, toByteCode{|*|}, Eq a
testEncoding` pred a = q (runFBC fromByteCode{|*|} (fromString (toByteCode{|*|} a))) (Right (Just a), [])
	/\ q (runFBC fromByteCode{|*|} (fromString (toByteCode{|*|} a) ++ ['abc'])) (Right (Just a), ['abc'])
where
	q (Right (Just e1), c1) (Right (Just e2), c2) = pred e1 e2 /\ c1 =.= c2
	q e1 e2 = e1 =.= e2

testEncoding :== testEncoding` (=.=)

Start w = flip (exposeProperties [Concise 99999999] [Tests 100000, Bent]) w $
		[ name "test()"          $ cast () testEncoding
		, name "testInt"         $ (testEncoding For int16s)
		, name "testLong"        $ cast (Long 0) testEncoding
		, name "testBool"        $ cast True testEncoding
		, name "testChar"        $ (testEncoding For map toChar [0..255])
		, name "testReal"        $ (testEncoding` realprop For reals)
		, name "testButton"      $ cast NoButton testEncoding
		, name "testUInt8"       $ cast (UInt8 0) testEncoding
		, name "testUInt16"      $ cast (UInt16 0) testEncoding
		, name "testString"      $ (testEncoding For ggenString 256 1.0 0 255 aStream)
		, name "testString255"   $ cast (String255 "") testEncoding
		, name "test2TuplesInt"  $ (testEncoding For tl [(0,0):ggen{|*->*->*|} (\_->int16s) (\_->int16s) genState])
		, name "test2TuplesLng"  $ cast (Long 0, Long 0) testEncoding
		, name "test3TuplesInt"  $ (testEncoding For tl [(0,0,0):ggen{|*->*->*->*|} (\_->int16s) (\_->int16s) (\_->int16s) genState])
		, name "test3TuplesLng"  $ cast (Long 0, Long 0, Long 0) testEncoding
		, name "testSpec"        $ cast {gDefault{|*|} & aPins=UInt8 0} testEncoding
		, name "testMessageTo"   $ cast MTTShutdown testEncoding
		, name "testMessageFro"  $ cast (MTFTaskAck (UInt8 0)) testEncoding
		, name "testBCShareSpec" $ cast {bcs_value=fromString "",bcs_ident=zero,bcs_itasks=zero} testEncoding
		, name "testPin"         $ cast (AnalogPin A0) testEncoding
		, name "testBCPeriph"    $ cast (BCDHT (AnalogPin A0) DHT22) testEncoding
		, name "testTaskVal()"   $ cast (Value () False) testEncoding
		, name "testTaskValInt"  $ cast (Value (UInt16 0) False) testEncoding
		, name "testTaskValLng"  $ cast (Value (Long 0) False) testEncoding
		, name "testTaskValTup"  $ cast (Value (UInt16 0, UInt16 0) False) testEncoding
		, name "testInstr"       $ cast BCStable0 testEncoding
		, name "testMessageList"
			\ml->runFBC pSepMessages (
				fromString (join "\n" (map toByteCode{|*|} ml)))
			=.= (Right (Just ml), [])
		]
where
	cast :: a -> (a -> Property) -> (a -> Property)
	cast _ = id

	pSepMessages :: FBC [MTMessageFro]
	pSepMessages = parseSepMessages
derive gDefault MTDeviceSpec, UInt8, UInt16
