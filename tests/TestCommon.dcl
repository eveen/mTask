definition module TestCommon

from Gast.Testable import :: Property
//import Gast => qualified <., <=., >., >=.
import mTask.Interpret.Message
from StdOverloaded import class fromInt(fromInt)
from StdList import map
from StdOverloaded import class <(<)

derive class Gast Either, Maybe, Button, MTDeviceSpec, MTMessageFro, MTMessageTo, BCInstr, BCShareSpec, BCTaskType, Pin, BCPeripheral, TaskValue, MTTTaskData
derive gPrint Long, UInt8, UInt16, String255
derive genShow Long, UInt8, UInt16, String255
derive ggen Long, UInt8, UInt16, String255

int16s :: [Int]
reals :== [0.001, -0.001, 1.23E10, -1.23E10, 1.23E20, -1.23E20:map fromInt ints]
ints  :== [0,1,-1,42,-42,100,-100]
longs :== map Long [20000, -20000:ints]

eps :== 1.19E-07

realeq :: Real Real -> Bool
realprop :: Real Real -> Property
