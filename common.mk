SHELL:=bash

.SECONDARY: $(addsuffix .prj,$(BINARIES))
.PHONY: clean

ifeq ($(OS), Windows_NT)
DETECTED_OS?=Windows
else
DETECTED_OS?=POSIX
endif

ifeq ($(DETECTED_OS), Windows)
LIBDIR=Libraries
else
LIBDIR=lib
endif

.PHONY: clean clobber

all: $(BINARIES)

%: %.prj %.icl
	cpm $< |& grep -v Analyzing

%.prj: mtask-$(DETECTED_OS).prt
	cpm project $* create $<

%.prj: %.prj.default
	cp $< $@

%.prj:
	cpm project $(basename $@) create
	cpm project $@ target iTasks
	cpm project $@ root ..
	cpm project $@ exec "$$PWD"/$(basename $@)
	cpm project $@ path add "$$PWD/../library/"
	cpm project $@ path add "$$PWD/../library/CleanSerial/src"
	cpm project $@ path add "$$PWD/../library/CleanSerial/src/$(DETECTED_OS)"
	cpm project $@ path add "$$PWD"
	cpm project $@ path add "$$CLEAN_HOME/$(LIBDIR)/Gast"
	cpm project $@ set -dynamics -h 500M -s 20M -b

clean:
	$(RM) -r $(BINARIES) $(foreach suf,.bc .pbc .prj .tar.gz -sapl -data -www,$(addsuffix $(suf),$(BINARIES)))
